#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "i8080.h"
#include "i8080_asm.h"



/* Test case environment */

#define TEST_BEGIN(X) do {                  \
    printf("Testing %-10s", X);             \
    fflush(stdout);                         \
    cpu.reset = true;                       \
    cpu_step(&cpu);                         \
    memset(mem, 0, MEM_SIZE);

#define TEST_END \
    printf("\033[32mOK\033[0m\n"); \
} while(0)


#define TEST_FAIL(REASON, ...) \
    printf("\033[31mFAIL\033[0m: " REASON " at line %d\n", __VA_ARGS__, __LINE__); \
    status = EXIT_FAILURE; \
    break

#define TEST_ASSERT_EQ(REG, VAL) \
    if ((REG) != (VAL)) { TEST_FAIL(#REG " is %d instead of %d", REG, VAL); }

#define TEST_ASSERT_EQ8(REG, VAL) \
    if ((REG) != (VAL)) { TEST_FAIL(#REG " is $%02x instead of $%02x", REG, VAL); }

#define TEST_ASSERT_EQ16(REG, VAL) \
    if ((REG) != (VAL)) { TEST_FAIL(#REG " is $%04x instead of $%04x", REG, VAL); }



/* Emulator */

#define TTY_PORT 0
#define MEM_SIZE 0x10000

#define MEM_LOAD_ARR(OFF, ARR) do {    \
    memcpy(mem+OFF, ARR, sizeof(ARR));      \
} while (0)

#define MEM_LOAD(OFF, ...) do {        \
    const data_t prog[] = { __VA_ARGS__ };  \
    MEM_LOAD_ARR(OFF, prog);           \
} while (0)

#define MEM_LOAD_FILE(OFF, FP) \
    fread(mem+OFF, 1, MEM_SIZE-OFF, FP);


cpu_t cpu = {0};
data_t mem[MEM_SIZE] = {0};

data_t cpu_user_load(cpu_t *cpu, addr_t addr) {
    (void)cpu;
    return mem[addr];
}

void cpu_user_store(cpu_t *cpu, addr_t addr, data_t data) {
    (void)cpu;
    mem[addr] = data;
}

data_t cpu_user_input(cpu_t *cpu, port_t port) {
    (void)cpu;
    return mem[port];
}

void cpu_user_output(cpu_t *cpu, port_t port, data_t data) {
    (void)cpu;
    mem[port] = data;
}


int main(void) {
    int status = EXIT_SUCCESS;

    TEST_BEGIN("MOV");
        MEM_LOAD(0x0000,
            MOV_A_M,
            MOV_B_A,
            MOV_M_B,
        );

        mem[0x0400] = 0xaa;
        cpu.HL = 0x0400;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xaa);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.B, 0xaa);

        cpu.HL = 0x0500;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x0500], 0xaa);

    TEST_END;


    TEST_BEGIN("STAX");
        MEM_LOAD(0x0000,
            STAX_B,
            STAX_D,
        );

        cpu.A = 0xaa;
        cpu.BC = 0x0400;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x0400], 0xaa);

        cpu.A = 0xbb;
        cpu.DE = 0x0500;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x0500], 0xbb);

    TEST_END;


    TEST_BEGIN("LDAX");
        MEM_LOAD(0x0000,
            0x0a, // LDAX B
            0x1a, // LDAX D
        );

        mem[0x0400] = 0xaa;
        cpu.BC = 0x0400;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xaa);

        mem[0x0500] = 0xbb;
        cpu.DE = 0x0500;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xbb);
    TEST_END;


    TEST_BEGIN("STA");
        MEM_LOAD(0x0000,
            STA, 0x00, 0x04 // STA $4000
        );

        cpu.A = 0xaa;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x0400], 0xaa);
    TEST_END;


    TEST_BEGIN("LDA");
        MEM_LOAD(0x0000,
            LDA, 0x00, 0x04, // LDA $4000
        );

        mem[0x0400] = 0xaa;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xaa);
    TEST_END;


    TEST_BEGIN("SHLD");
        MEM_LOAD(0x0000,
            SHLD, 0x00, 0x04, // SHLD $4000
        );

        cpu.HL = 0xaabb;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x0400], 0xbb);
        TEST_ASSERT_EQ8(mem[0x0401], 0xaa);
    TEST_END;


    TEST_BEGIN("LHLD");
        MEM_LOAD(0x0000,
            LHLD, 0x00, 0x04, // LHLD $4000
        );

        mem[0x0400] = 0xaa;
        mem[0x0401] = 0xbb;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0xbbaa);
    TEST_END;


    TEST_BEGIN("MVI");
        MEM_LOAD(0x0000,
            MVI_B, 0xbb,
            MVI_C, 0xcc,
            MVI_D, 0xdd,
            MVI_E, 0xee,
            MVI_H, 0xff,
            MVI_L, 0x11,
            MVI_M, 0x99,
            MVI_A, 0xaa,
        );

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.B, 0xbb);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.C, 0xcc);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.D, 0xdd);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.E, 0xee);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.H, 0xff);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.L, 0x11);

        cpu.HL = 0x0400;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x0400], 0x99);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xaa);

    TEST_END;


    TEST_BEGIN("LXI");
        MEM_LOAD(0x0000,
            LXI_B,  0xcc, 0xbb, // LXI B, $bbcc
            LXI_D,  0xee, 0xdd, // LXI D, $ddee
            LXI_H,  0x11, 0xff, // LXI H, $ff11
            LXI_SP, 0x44, 0x33, // LXI SP,$3344
        );

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.BC, 0xbbcc);

        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.DE, 0xddee);

        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0xff11);

        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.SP, 0x3344);

    TEST_END;


    TEST_BEGIN("SPHL");
        MEM_LOAD(0x0000,
            SPHL
        );

        cpu.HL = 0xaabb;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.SP, 0xaabb);

    TEST_END;


    TEST_BEGIN("PCHL");
        MEM_LOAD(0x0000,
            PCHL
        );

        cpu.HL = 0xaabb;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.PC, 0xaabb);

    TEST_END;


    TEST_BEGIN("XCHG");
        MEM_LOAD(0x0000,
            XCHG
        );

        cpu.HL = 0xaabb;
        cpu.DE = 0xddee;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0xddee);
        TEST_ASSERT_EQ16(cpu.DE, 0xaabb);

    TEST_END;


    TEST_BEGIN("PUSH"); // TODO: test with other registers
        MEM_LOAD(0x0000,
            PUSH_B
        );

        cpu.SP = 0x13a6;
        cpu.BC = 0x6a30;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x13a5], 0x6a);
        TEST_ASSERT_EQ8(mem[0x13a4], 0x30);
        TEST_ASSERT_EQ16(cpu.SP, 0x13a4);

    TEST_END;


    TEST_BEGIN("POP"); // TODO: test with other registers
        MEM_LOAD(0x0000,
            POP_H
        );

        mem[0x1238] = 0xff;
        mem[0x1239] = 0x3d;
        mem[0x123a] = 0x93;
        mem[0x123b] = 0xff;
        cpu.HL = 0x0ff0;
        cpu.SP = 0x1239;

        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0x933d);
        TEST_ASSERT_EQ16(cpu.SP, 0x123b);
    TEST_END;


    TEST_BEGIN("XTHL");
        MEM_LOAD(0x0000,
            XTHL
        );

        cpu.SP = 0x10ad;
        cpu.HL = 0x0b3c;
        mem[0x10ad] = 0xf0;
        mem[0x10ae] = 0x0d;

        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0x0df0);
        TEST_ASSERT_EQ8(mem[0x10ae], 0x0b);
        TEST_ASSERT_EQ8(mem[0x10ad], 0x3c);
        TEST_ASSERT_EQ16(cpu.SP, 0x10ad);

    TEST_END;


    TEST_BEGIN("EI-DI");
        MEM_LOAD(0x0000,
            EI,
            DI,
        );

        TEST_ASSERT_EQ(cpu.inte, false);

        cpu_step(&cpu);
        TEST_ASSERT_EQ(cpu.inte, true);

        cpu_step(&cpu);
        TEST_ASSERT_EQ(cpu.inte, false);

    TEST_END;


    TEST_BEGIN("STC-CMC");
        MEM_LOAD(0x0000,
            STC, // STC
            CMC, // CMC
        );

        TEST_ASSERT_EQ(cpu.F & FLAG_C, 0);

        cpu_step(&cpu);
        TEST_ASSERT_EQ(cpu.F & FLAG_C, FLAG_C);

        cpu_step(&cpu);
        TEST_ASSERT_EQ(cpu.F & FLAG_C, 0);

    TEST_END;


    TEST_BEGIN("CMA");
        MEM_LOAD(0x0000,
            CMA, // CMA
        );

        cpu.A = 0x55;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xaa);

    TEST_END;


    TEST_BEGIN("ADD"); // TODO: test flags
        MEM_LOAD(0x0000,
            ADD_B,
            ADD_C,
            ADD_D,
            ADD_E,
            ADD_H,
            ADD_L,
            ADD_M,
            ADD_A,
            ADI, 0x5a,
        );

        cpu.A = 0x00; cpu.B = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0x10; cpu.C = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x10);

        cpu.A = 0x55; cpu.D = 0xaa;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xff);

        cpu.A = 0xff; cpu.E = 0x0f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x0e);

        cpu.A = 0xf0; cpu.H = 0xf1;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xe1);

        cpu.A = 0x33; cpu.L = 0xf2;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x25);

        cpu.A = 0xaa;
        cpu.HL = 0x0400;
        mem[0x0400] = 0xdd;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x87);

        cpu.A = 0xcc;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x98);

        cpu.A = 0x41;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x9b);

    TEST_END;


    TEST_BEGIN("ADC"); // TODO: test flags
        MEM_LOAD(0x0000,
            ADC_B,
            ADC_C,
            ADC_D,
            ADC_E,
            ADC_H,
            ADC_L,
            ADC_M,
            ADC_A,
            ACI, 0x5a,
        );

        cpu.A = 0x00; cpu.B = 0x00;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x01);

        cpu.A = 0x10; cpu.C = 0x00;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x10);

        cpu.A = 0x55; cpu.D = 0xaa;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0xff; cpu.E = 0x0f;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x0e);

        cpu.A = 0xf0; cpu.H = 0xf1;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xe2);

        cpu.A = 0x33; cpu.L = 0xf2;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x25);

        cpu.A = 0xaa;
        cpu.HL = 0x0400;
        mem[0x0400] = 0xdd;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x88);

        cpu.A = 0xcc;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x98);

        cpu.A = 0x41;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x9c);

    TEST_END;


    TEST_BEGIN("DAA");
        MEM_LOAD(0x0000,
            MVI_A, 0x08,
            ADI, 0x93,
            DAA,
        );

        cpu_step(&cpu);
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x9b);
        TEST_ASSERT_EQ8(cpu.F, 0x82);
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x01);
        TEST_ASSERT_EQ8(cpu.F, 0x13);

    TEST_END;


    TEST_BEGIN("SUB"); // TODO: test flags
        MEM_LOAD(0x0000,
            SUB_B,
            SUB_C,
            SUB_D,
            SUB_E,
            SUB_H,
            SUB_L,
            SUB_M,
            SUB_A,
            SUI, 0x5a,
        );

        cpu.A = 0x00; cpu.B = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0x10; cpu.C = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x10);

        cpu.A = 0x55; cpu.D = 0xaa;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xab);

        cpu.A = 0xff; cpu.E = 0x0f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xf0);

        cpu.A = 0xf0; cpu.H = 0xf1;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xff);

        cpu.A = 0x33; cpu.L = 0xf2;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x41);

        cpu.A = 0xaa;
        cpu.HL = 0x0400;
        mem[0x0400] = 0xdd;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xcd);

        cpu.A = 0xcc;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0x41;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xe7);

    TEST_END;


    TEST_BEGIN("SBB"); // TODO: test flags
        MEM_LOAD(0x0000,
            SBB_B,
            SBB_C,
            SBB_D,
            SBB_E,
            SBB_H,
            SBB_L,
            SBB_M,
            SBB_A,
            SBI, 0x5a,
        );

        cpu.A = 0x00; cpu.B = 0x00;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0x10; cpu.C = 0x00;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x0f);

        cpu.A = 0x55; cpu.D = 0xaa;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xab);

        cpu.A = 0xff; cpu.E = 0x0f;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xef);

        cpu.A = 0xf0; cpu.H = 0xf1;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xff);

        cpu.A = 0x33; cpu.L = 0xf2;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x40);

        cpu.A = 0xaa;
        cpu.HL = 0x0400;
        cpu.F = 0x03;
        mem[0x0400] = 0xdd;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xcc);

        cpu.A = 0xcc;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xff);

        cpu.A = 0x41;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xe6);

    TEST_END;


    TEST_BEGIN("CMP");
        MEM_LOAD(0x0000,
            CMP_B,
            CMP_C,
            CMP_D,
            CMP_E,
            CMP_H,
            CMP_L,
            CMP_M,
            CMP_A,
            CPI, 0x5a,
        );

        cpu.A = 0x00; cpu.B = 0x00;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);
        TEST_ASSERT_EQ8(cpu.F, 0x56);

        cpu.A = 0x10; cpu.C = 0x00;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x10);
        TEST_ASSERT_EQ8(cpu.F, 0x12);

        cpu.A = 0x55; cpu.D = 0xaa;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x55);
        TEST_ASSERT_EQ8(cpu.F, 0x83);

        cpu.A = 0xff; cpu.E = 0x0f;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xff);
        TEST_ASSERT_EQ8(cpu.F, 0x96);

        cpu.A = 0xf0; cpu.H = 0xf1;
        cpu.F = 0x02;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xf0);
        TEST_ASSERT_EQ8(cpu.F, 0x87);

        cpu.A = 0x33; cpu.L = 0xf2;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x33);
        TEST_ASSERT_EQ8(cpu.F, 0x17);

        cpu.A = 0xaa;
        cpu.HL = 0x0400;
        mem[0x0400] = 0xdd;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xaa);
        TEST_ASSERT_EQ8(cpu.F, 0x83);

        cpu.A = 0xcc;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xcc);
        TEST_ASSERT_EQ8(cpu.F, 0x56);

        cpu.A = 0x41;
        cpu.F = 0x03;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x41);
        TEST_ASSERT_EQ8(cpu.F, 0x87);

    TEST_END;


    TEST_BEGIN("AND"); // TODO: test flags
        MEM_LOAD(0x0000,
            ANA_B,
            ANA_C,
            ANA_D,
            ANA_E,
            ANA_H,
            ANA_L,
            ANA_M,
            ANA_A,
            ANI, 0x5a,
        );

        cpu.A = 0x00; cpu.B = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0x10; cpu.C = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0x55; cpu.D = 0xaa;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0xff; cpu.E = 0x0f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x0f);

        cpu.A = 0xf0; cpu.H = 0xf1;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xf0);

        cpu.A = 0x33; cpu.L = 0xf2;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x32);

        cpu.A = 0xaa; 
        cpu.HL = 0x0400;
        mem[0x0400] = 0xdd;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x88);

        cpu.A = 0xcc;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xcc);

        cpu.A = 0x41;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x40);

    TEST_END;


    TEST_BEGIN("OR"); // TODO: test flags
        MEM_LOAD(0x0000,
            ORA_B,
            ORA_C,
            ORA_D,
            ORA_E,
            ORA_H,
            ORA_L,
            ORA_M,
            ORA_A,
            ORI, 0x5a,
        );

        cpu.A = 0x00; cpu.B = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0x10; cpu.C = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x10);

        cpu.A = 0x55; cpu.D = 0xaa;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xff);

        cpu.A = 0xff; cpu.E = 0x0f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xff);

        cpu.A = 0xf0; cpu.H = 0xf1;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xf1);

        cpu.A = 0x33; cpu.L = 0xf2;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xf3);

        cpu.A = 0xaa; 
        cpu.HL = 0x0400;
        mem[0x0400] = 0xdd;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xff);

        cpu.A = 0xcc;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xcc);

        cpu.A = 0x41;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x5b);

    TEST_END;


    TEST_BEGIN("XOR"); // TODO: test flags
        MEM_LOAD(0x0000,
            XRA_B,
            XRA_C,
            XRA_D,
            XRA_E,
            XRA_H,
            XRA_L,
            XRA_M,
            XRA_A,
            XRI, 0x5a,
        );

        cpu.A = 0x00; cpu.B = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0x10; cpu.C = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x10);

        cpu.A = 0x55; cpu.D = 0xaa;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xff);

        cpu.A = 0xff; cpu.E = 0x0f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xf0);

        cpu.A = 0xf0; cpu.H = 0xf1;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x01);

        cpu.A = 0x33; cpu.L = 0xf2;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0xc1);

        cpu.A = 0xaa;
        cpu.HL = 0x0400;
        mem[0x0400] = 0xdd;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x77);

        cpu.A = 0xcc;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x00);

        cpu.A = 0x41;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x1b);

    TEST_END;


    TEST_BEGIN("ROT");
        MEM_LOAD(0x0000,
            RLC,
            RAL,
            RRC,
            RAR,
        );

        cpu.A = 0x55;
        cpu.F = 2;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A,  0xaa);
        TEST_ASSERT_EQ8(cpu.F, 0x02);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x54);
        TEST_ASSERT_EQ8(cpu.F, 0x03);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x2a);
        TEST_ASSERT_EQ8(cpu.F, 0x02);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x15);
        TEST_ASSERT_EQ8(cpu.F, 0x02);

    TEST_END;


    TEST_BEGIN("INR");
        MEM_LOAD(0x0000,
            INR_B,
            INR_C,
            INR_D,
            INR_E,
            INR_H,
            INR_L,
            INR_M,
            INR_A,
        );

        /* Reset carry */
        cpu.F &= ~FLAG_C;

        cpu.B = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.B, 0x01);
        TEST_ASSERT_EQ8(cpu.F, 0x02);

        cpu.C = 0xff;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.C, 0x00);
        TEST_ASSERT_EQ8(cpu.F, 0x56);

        cpu.D = 0x1f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.D, 0x20);
        TEST_ASSERT_EQ8(cpu.F, 0x12);

        cpu.E = 0x80;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.E, 0x81);
        TEST_ASSERT_EQ8(cpu.F, 0x86);

        cpu.H = 0x7f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.H, 0x80);
        TEST_ASSERT_EQ8(cpu.F, 0x92);

        cpu.L = 0x0e;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.L, 0x0f);
        TEST_ASSERT_EQ8(cpu.F, 0x06);

        mem[0x0400] = 0x10;
        cpu.HL = 0x0400;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x0400], 0x11);
        TEST_ASSERT_EQ8(cpu.F, 0x06);

        cpu.A = 0x0f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x10);
        TEST_ASSERT_EQ8(cpu.F, 0x12);

    TEST_END;


    TEST_BEGIN("DCR");
        MEM_LOAD(0x0000,
            DCR_B,
            DCR_C,
            DCR_D,
            DCR_E,
            DCR_H,
            DCR_L,
            DCR_M,
            DCR_A,
        );

        /* Reset carry */
        cpu.F &= ~FLAG_C;

        cpu.B = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.B, 0xff);
        TEST_ASSERT_EQ8(cpu.F, 0x86);

        cpu.C = 0xff;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.C, 0xfe);
        TEST_ASSERT_EQ8(cpu.F, 0x92);

        cpu.D = 0x1f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.D, 0x1e);
        TEST_ASSERT_EQ8(cpu.F, 0x16);

        cpu.E = 0x80;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.E, 0x7f);
        TEST_ASSERT_EQ8(cpu.F, 0x02);

        cpu.H = 0x7f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.H, 0x7e);
        TEST_ASSERT_EQ8(cpu.F, 0x16);

        cpu.L = 0x0e;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.L, 0x0d);
        TEST_ASSERT_EQ8(cpu.F, 0x12);

        mem[0x0400] = 0x10;
        cpu.HL = 0x0400;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x0400], 0x0f);
        TEST_ASSERT_EQ8(cpu.F, 0x06);

        cpu.A = 0x0f;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x0e);
        TEST_ASSERT_EQ8(cpu.F, 0x12);

    TEST_END;


    TEST_BEGIN("DAD");

        MEM_LOAD(0x0000,
            DAD_B,
            DAD_D,
            DAD_H,
            DAD_SP,
        );

        cpu.HL = 0x0000;
        cpu.BC = 0xabcd;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0xabcd);
        TEST_ASSERT_EQ(cpu.F & FLAG_C, 0);

        cpu.DE = 0x1122;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0xbcef);
        TEST_ASSERT_EQ(cpu.F & FLAG_C, 0);

        // Add HL to HL (shift HL by one)
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0x79de);
        TEST_ASSERT_EQ(cpu.F & FLAG_C, FLAG_C);

        cpu.SP = 0x1fff;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0x99dd);
        TEST_ASSERT_EQ(cpu.F & FLAG_C, 0);

    TEST_END;


    TEST_BEGIN("INX");
        MEM_LOAD(0x0000,
            INX_B,
            INX_D,
            INX_H,
            INX_SP,
        );

        cpu.B = 0x0a; cpu.C = 0xff;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.BC, 0x0b00);

        cpu.D = 0xff; cpu.E = 0xff;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.DE, 0x0000);

        cpu.H = 0x0c; cpu.L = 0xfe;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0x0cff);

        cpu.SP = 0x00ff;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.SP, 0x0100);

    TEST_END;


    TEST_BEGIN("DCX");
        MEM_LOAD(0x0000,
            DCX_B,
            DCX_D,
            DCX_H,
            DCX_SP,
        );

        cpu.B = 0x01; cpu.C = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.BC, 0x00ff);

        cpu.D = 0x00; cpu.E = 0x00;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.DE, 0xffff);

        cpu.H = 0x0c; cpu.L = 0xfe;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.HL, 0x0cfd);

        cpu.SP = 0x0100;
        cpu_step(&cpu);
        TEST_ASSERT_EQ16(cpu.SP, 0x00ff);

    TEST_END;



    TEST_BEGIN("IN-OUT");
        MEM_LOAD(0x0000,
            IN,  0x80,
            OUT, 0x81,
        );

        mem[0x80] = 0x99;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x99);

        cpu_step(&cpu);
        TEST_ASSERT_EQ8(mem[0x81], 0x99);

    TEST_END;


    // TODO tests for RET CALL and RST
    TEST_BEGIN("IRQ");
        data_t ins[3];

        /* RST 4 procedure */
        MEM_LOAD(0x0020,
            MVI_A, 0x77,
            EI,
            RET,
        );

        /* Main program */
        MEM_LOAD(0x0100,
            MVI_A, 0x10,
            MVI_A, 0x11,
            MVI_A, 0x12,
            MVI_A, 0x13,
            MVI_A, 0x14,
        );

        /* Step normal instruction */
        cpu.PC = 0x0100;
        cpu.inte = true;
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x10);
        TEST_ASSERT_EQ16(cpu.PC, 0x102);

        /* Send IRQ with instruction MVI A, $99 */
        ins[0] = MVI_A;
        ins[1] = 0x99;
        cpu.irq_data = ins;
        cpu_step(&cpu);
        TEST_ASSERT_EQ(cpu.irq_ack, 1);
        TEST_ASSERT_EQ8(cpu.A, 0x99);
        TEST_ASSERT_EQ16(cpu.PC, 0x102); // Normal execution continues

        /* Resend IRQ (not executed because INTE not reenabled) */
        cpu.irq_data = ins;
        cpu_step(&cpu); // MVI A, $11 is executed
        TEST_ASSERT_EQ(cpu.irq_ack, 0);
        TEST_ASSERT_EQ8(cpu.A, 0x11);
        TEST_ASSERT_EQ16(cpu.PC, 0x104);

        /* Step after IRQ (continue execution) */
        cpu_step(&cpu);
        TEST_ASSERT_EQ8(cpu.A, 0x12);
        TEST_ASSERT_EQ16(cpu.PC, 0x106);

        /* Reset inte, send IRQ with LDA $0400 */
        ins[0] = LDA; // 
        ins[1] = 0x00;
        ins[2] = 0x04;
        mem[0x0400] = 0x88;
        cpu.inte = true;
        cpu.irq_data = ins;
        cpu_step(&cpu);
        TEST_ASSERT_EQ(cpu.inte, false);
        TEST_ASSERT_EQ(cpu.irq_ack, 1);
        TEST_ASSERT_EQ8(cpu.A, 0x88);
        TEST_ASSERT_EQ16(cpu.PC, 0x106);

        /* Resend again (fail, interrupts still disabled) */
        cpu.irq_data = ins;
        cpu_step(&cpu); // MVI A, $13 is executed instead
        TEST_ASSERT_EQ(cpu.inte, false);
        TEST_ASSERT_EQ(cpu.irq_ack, 0);
        TEST_ASSERT_EQ8(cpu.A, 0x13);
        TEST_ASSERT_EQ16(cpu.PC, 0x108);

        /* Reset inte, send RST instruction */
        ins[0] = RST_4;
        cpu.inte = true;
        cpu.irq_data = ins;
        cpu_step(&cpu);
        TEST_ASSERT_EQ(cpu.irq_ack, 1);
        TEST_ASSERT_EQ8(cpu.A, 0x13);
        TEST_ASSERT_EQ16(cpu.PC, 0x20);

        /* Resend (fail) */
        ins[0] = RST_4;
        cpu.irq_data = ins;
        cpu_step(&cpu); // MVI A, $77 is executed
        TEST_ASSERT_EQ(cpu.inte, false);
        TEST_ASSERT_EQ(cpu.irq_ack, false);
        TEST_ASSERT_EQ(cpu.irq_data == NULL, true);
        TEST_ASSERT_EQ8(cpu.A, 0x77);
        TEST_ASSERT_EQ16(cpu.PC, 0x22);

        /* Step through RST 4 procedure */
        cpu_step(&cpu); // EI
        TEST_ASSERT_EQ(cpu.inte, true);
        TEST_ASSERT_EQ(cpu.irq_data == NULL, true);
        TEST_ASSERT_EQ8(cpu.A, 0x77);
        TEST_ASSERT_EQ16(cpu.PC, 0x23);

        cpu_step(&cpu); // RET
        TEST_ASSERT_EQ(cpu.inte, true);
        TEST_ASSERT_EQ8(cpu.A, 0x77);
        TEST_ASSERT_EQ16(cpu.PC, 0x108);

        cpu_step(&cpu); // MVI A, $12 (back to main program)
        TEST_ASSERT_EQ(cpu.inte, true);
        TEST_ASSERT_EQ8(cpu.A, 0x14);
        TEST_ASSERT_EQ16(cpu.PC, 0x10a);

    TEST_END;

    return status;
}
