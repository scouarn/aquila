10 INPUT "CODE MORSE "; X$
20 PRINT "LE CARACTERE EST ";
30 IF X$ = ".-" THEN PRINT "A":GOTO 10
40 IF X$ = "-..." THEN PRINT "B":GOTO 10
50 IF X$ = "-.-." THEN PRINT "C":GOTO 10
60 IF X$ = "-.." THEN PRINT "D":GOTO 10
70 IF X$ = "." THEN PRINT "E":GOTO 10
80 IF X$ = "..-." THEN PRINT "F":GOTO 10
90 IF X$ = "--." THEN PRINT "G":GOTO 10
100 IF X$ = "...." THEN PRINT "H":GOTO 10
110 IF X$ = ".." THEN PRINT "I":GOTO 10
120 IF X$ = ".---" THEN PRINT "J":GOTO 10
130 IF X$ = "-.-" THEN PRINT "K":GOTO 10
140 IF X$ = ".-.." THEN PRINT "L":GOTO 10
150 IF X$ = "--" THEN PRINT "M":GOTO 10
160 IF X$ = "-." THEN PRINT "N":GOTO 10
170 IF X$ = "---" THEN PRINT "O":GOTO 10
180 IF X$ = ".--." THEN PRINT "P":GOTO 10
190 IF X$ = "--.-" THEN PRINT "Q":GOTO 10
200 IF X$ = ".-." THEN PRINT "R":GOTO 10
210 IF X$ = "..." THEN PRINT "S":GOTO 10
220 IF X$ = "-" THEN PRINT "T":GOTO 10
230 IF X$ = "..-" THEN PRINT "U":GOTO 10
240 IF X$ = "...-" THEN PRINT "V":GOTO 10
250 IF X$ = ".--" THEN PRINT "W":GOTO 10
260 IF X$ = "-..-" THEN PRINT "X":GOTO 10
270 IF X$ = "-.--" THEN PRINT "Y":GOTO 10
280 IF X$ = ".----" THEN PRINT "1":GOTO 10
290 IF X$ = "..---" THEN PRINT "2":GOTO 10
300 IF X$ = "...--" THEN PRINT "3":GOTO 10
310 IF X$ = "....-" THEN PRINT "4":GOTO 10
320 IF X$ = "....." THEN PRINT "5":GOTO 10
330 IF X$ = "-...." THEN PRINT "6":GOTO 10
340 IF X$ = "--..." THEN PRINT "7":GOTO 10
360 IF X$ = "---.." THEN PRINT "8":GOTO 10
370 IF X$ = "----." THEN PRINT "9":GOTO 10
380 IF X$ = "-----" THEN PRINT "0":GOTO 10
500 PRINT "CODE NON RECONNU":GOTO 10

1000 PRINT CHR$(27) "[2J";
1010 PRINT CHR$(27) "[H";
1020 PRINT "                ------------"
1030 PRINT "                |          |"
1030 PRINT "                | 01010111 |"
1040 PRINT "                | 01001001 |"
1050 PRINT "                | 01000110 |"
1060 PRINT "                | 01001001 |"
1070 PRINT "  O             |--   -----|    O"
1080 PRINT "  |                | /          |"
1090 PRINT "  |                |/           |"
1100 PRINT "  |   -----------------------   |"
1110 PRINT " ----/                       \----"
1120 PRINT "|    |                       |    |"
1130 PRINT "|    |    /o\         /o\    |    |"
1140 PRINT "|    |    \ /         \ /    |    |"
1150 PRINT "|    |                       |    |"
1150 PRINT "|    |                       |    |"
1160 PRINT "|    |                       |    |"
1170 PRINT "|    |                       |    |"
1180 PRINT "|    |                       |    |"
1190 PRINT "|    |   /===============\   |    |"
1200 PRINT "|    |   | | | | | | | | |   |    |"
1240 PRINT "|----|   \===============/   |----|"
1280 PRINT "     \                       /"
1320 PRINT "      -----------------------"

1500 INPUT "CODE ASCII "; S$
1510 X=INP(255)
1520 PRINT "LE CARACTERE EST : ";
1530 PRINT CHR$(X)
1540 GOTO 1500

2000 PRINT CHR$(27) "[2J";
2010 PRINT CHR$(27) "[H";
2020 PRINT "               ---"
2030 PRINT "            .-9 9 `\"
2040 PRINT "          =(:(::)=  ;"
2050 PRINT "            ||||     \"
2060 PRINT "            ||||      `-."
2070 PRINT "           ,\|\|         `,"
2080 PRINT "          /                \"
2090 PRINT "         ;                  `'---.,"
2100 PRINT "         |                         `\"
2110 PRINT "         ;                     /     |"
2120 PRINT "         \                    |      /"
2130 PRINT "  jgs     )           \  --,.--\    /"
2140 PRINT "       .-' \,...-\     \`   .-'  .-'"
2150 PRINT "      `-=``      `:    |   /-/-/`"
2160 PRINT "                   `.--/"
2200 GOTO 10

3000 X=RND(1)
3010 IF X > 0.5 THEN PRINT "/";:GOTO 3000
3020 PRINT "\";:GOTO 3000

