#define _POSIX_SOURCE
#include "devices.h"

/* TTY and disk drive implementation with stdio */

#include <stdio.h>
#include <termios.h>
#include <unistd.h>

static struct termios raw, cooked;
static FILE *tape_in, *tape_out;
static int buff_in, buff_out;

void tty_init(void)
{
    tcgetattr(0, &cooked);
    raw = cooked;
    raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= (CS8);
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    raw.c_cc[VMIN]  = 0;
    raw.c_cc[VTIME] = 0;
    tcsetattr(0, TCSANOW, &raw);
    setvbuf(stdout, NULL, _IONBF, 0);
}

void tty_uninit(void)
{
    tcsetattr(0, TCSANOW, &cooked);

    if (tape_in != NULL) {
        fclose(tape_in);
    }

    if (tape_out != NULL) {
        fclose(tape_out);
    }
}

void tty_update(void)
{
    data_t c;
    if (read(STDIN_FILENO, &c, 1) > 0) {
        buff_in = c;
    } else if (tape_in != NULL) {
        int res = read(fileno(tape_in), &c, 1);
        if (res <= 0) {
            fclose(tape_in);
            tape_in = NULL;
        } else {
            buff_in = c;
        }
    }

    if (buff_out >= 0) {
        /* Unset bit 7 */
        // TODO: Don't do it if stdout is not a tty ?
        // Is 7bits serial actually 7bits ? How did binary tape loaders work ?
        data_t data = buff_out & 0x7f;

        putchar(data);
        if (tape_out != NULL) {
            fputc(data, tape_out);
        }
        buff_out = -1;
    }
}

bool tty_input_ready(port_t port)
{
    (void)port;
    return buff_in >= 0;
}

bool tty_output_ready(port_t port)
{
    (void)port;
    return buff_out < 0;
}

data_t tty_input(port_t port)
{
    (void)port;
    if (buff_in < 0) {
        return 0;
    }

    data_t data = (data_t)buff_in;
    buff_in = -1;
    return data;
}

void tty_output(port_t port, data_t data)
{
    (void)port;
    buff_out = data;
}

data_t tty_peek(void)
{
    if (buff_in < 0) {
        return 0;
    }

    return (data_t)buff_in;
}

bool tty_tape_in(const char *fname)
{
    if (tape_in != NULL) {
        fclose(tape_in);
        tape_in = NULL;
    }

    if (fname == NULL) {
        return true;
    }

    tape_in = fopen(fname, "rb");
    if (tape_in == NULL) {
        return false;
    }

    return true;
}

bool tty_tape_out(const char *fname)
{
    if (tape_out != NULL || fname == NULL) {
        fclose(tape_out);
        tape_out = NULL;
    }

    if (fname == NULL) {
        return true;
    }

    tape_out = fopen(fname, "wb");
    if (tape_out == NULL) {
        return false;
    }

    return true;
}



static FILE *files[DCDD_NUM_DRIVES] = {0};

void disk_init() {}

void disk_uninit(void)
{
    for (int i = 0; i < DCDD_NUM_DRIVES; i++) {
        if (files[i] != NULL) {
            fclose(files[i]);
        }
    }
}

void disk_write_sector(uint8_t drive, uint8_t track, uint8_t sector, const data_t data[DCDD_SECTOR_SIZE])
{
    if (drive > DCDD_NUM_DRIVES) {
        return;
    }

    FILE *fp = files[drive];
    if (fp == NULL) {
        //printf("** ERROR: Trying to write to drive %d which is inactive\r\n", drive);
        return;
    }

    long offset = (track * DCDD_NUM_SECTORS + sector) * DCDD_SECTOR_SIZE;
    fseek(fp, offset, SEEK_SET);

    fwrite(data, 1, DCDD_SECTOR_SIZE, fp);
}

void disk_read_sector(uint8_t drive, uint8_t track, uint8_t sector, data_t data[DCDD_SECTOR_SIZE])
{
    if (drive > DCDD_NUM_DRIVES) {
        return;
    }

    FILE *fp = files[drive];
    if (fp == NULL) {
        //printf("** ERROR: Trying to read from drive %d which is inactive\r\n", drive);
        for (int i = 0; i < DCDD_SECTOR_SIZE; i++) {
            data[i] = 0;
        }
        return;
    }

    long offset = (track * DCDD_NUM_SECTORS + sector) * DCDD_SECTOR_SIZE;
    fseek(fp, offset, SEEK_SET);
    fread(data, 1, DCDD_SECTOR_SIZE, fp);
}

bool disk_attach(uint8_t drive, const char *fname)
{
    if (drive > DCDD_NUM_DRIVES) {
        return false;
    }

    if (files[drive] != NULL) {
        fclose(files[drive]);
        files[drive] = NULL;
    }

    if (fname == NULL) {
        return true;
    }

    files[drive] = fopen(fname, "rb+");
    if (files[drive] == NULL) {
        return false;
    }

    return true;
}
