#define _POSIX_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <dirent.h>

#include "i8080.h"
#include "devices.h"


/* Necessary declarations */
static void cleanup(void);

/* Timing */

static float freq_MHz = 2.0f;
uint64_t runtime_usecs = 0; /* CPU real runtime since last menu command */
static uint64_t time_usecs(void)
{
    struct timeval now;
    gettimeofday(&now, NULL);
    return (now.tv_sec) * 1000000 + now.tv_usec;
}


/* CPU */

static cpu_t cpu = {
    .wait = true,
};


/* RAM */

// TODO: bank switching, read-only areas...
#define MEM_SIZE 0x10000
static data_t mem[MEM_SIZE] = {0};

data_t cpu_user_load(cpu_t *cpu, addr_t addr)
{
    (void)cpu;
    return mem[addr];
}

void cpu_user_store(cpu_t *cpu, addr_t addr, data_t data)
{
    (void)cpu;
    mem[addr] = data;
}

/* Serial IO */

static sio_t sio = {
    .base_port = SIO_DEFAULT_PORT,
};

static sio2_t sio2 = {
    .base_port = SIO2_DEFAULT_PORT,
};


/* Disk IO */

static dcdd_t dcdd = {
    .present_drives = (1 << 0), /* Only drive 0 */
};


/* CPU IO callbacks */

static data_t sense = 0x00;

data_t cpu_user_input(cpu_t *cpu, port_t port)
{
    (void)cpu;

    data_t data = 0x00;
    data |= sio_update_read(&sio,   port);
    data |= sio2_update_read(&sio2, port);
    data |= dcdd_update_read(&dcdd, port);

    if (port == SENSE_PORT) {
        data |= sense;
    }

    return data;
}

void cpu_user_output(cpu_t *cpu, port_t port, data_t data)
{
    (void)cpu;
    sio_update_write(&sio,   port, data);
    sio2_update_write(&sio2, port, data);
    dcdd_update_write(&dcdd, port, data);
}


/* Emulator shell */

#define MENU_KEY 1 /* The character C^A */

char menu_getc(void)
{
    data_t c = 0;
    while (read(STDIN_FILENO, &c, 1) == 0) {}
    return c;
}

uint32_t menu_hextoi(const char *s)
{
    uint32_t res = 0;
    char c;
    while ((c = *(s++)) != '\0') {
        res <<= 4;
        if (c >= 'A' && c <= 'F') {
            res |= c - 'A' + 10;
        } else if (c >= 'a' && c <= 'f') {
            res |= c - 'a' + 10;
        } else if (c >= '0' && c <= '9') {
            res |= c - '0';
        } else {
            return 0;
        }
    }
    return res;
}

int menu_readline(char buff[], int buff_sz)
{
    int i = 0;

    while (1) {
        char c = menu_getc();

        if (i >= buff_sz-1) { /* End of buffer */
            break;
        }

        if (c == '\r' || c == '\n') { /* End of line */
            break;
        }

        if (c == 0x7f && i > 0) { /* Del/backspace */
            putchar('\b');
            putchar(' ');
            putchar('\b');
            i--;
            buff[i] = '\0';
        } else if (c >= 0x20 && c <= 0x7e) {
            putchar(c); /* Echo */
            buff[i++] = c;
        } else if (c == 4 && i == 0) { /* C^D Return exit status */
            buff[0] = '\0';
            return -1;
        } else if (c == 3 || c == 4) { /* Return empty command (cancel) */
            buff[0] = '\0';
            return 0;
        } else {
            /* Ignore this char */
        }
    }

    buff[i] = '\0';
    return i;
}

/* Return current the next command by returning the current line pointer,
    put a \0 at the end if it in the buffer, and advance line to
    start of next space-separated word.
   Return NULL if it was the end of the buffer */
const char *menu_next_cmd(char **line)
{
    char c;
    const char *res = NULL;

    /* Skip spaces */
    while ((c = **line) != '\0' && c == ' ')  {
        (*line)++;
    }

    /* No word or comment */
    if (c == '\0' || c == '#') {
        return res;
    }

    res = *line;

    /* Skip current word */
    while ((c = **line) != '\0' && c != '#' && c != ' ')  {
        (*line)++;
    }

    /* Set end of word */
    if (c != '\0') {
        **line = '\0';
        (*line)++;
    }

    return res;
}

#define MENU_SUCCESS    0
#define MENU_ERROR      1
#define MENU_WRONG_ARGS 2
typedef int (menu_func_t)(char *cmd, bool included);

typedef struct {
    const char *name;
    menu_func_t *func;
    const char *params, *desc;
} menu_binding_t;

menu_func_t menu_help;
void menu_exec(char *cmd_line, bool included);

int menu_quit(char *cmd_line, bool included)
{
    (void)cmd_line;
    if (included) {
        printf("Cannot exit while reading file");
        return MENU_ERROR;
    }

    printf("Bye!\r\n");
    cleanup();
    exit(0);
    return MENU_SUCCESS;
}

int menu_dir(char *cmd_line, bool included)
{
    (void) included;
    const char *path = menu_next_cmd(&cmd_line);
    if (path == NULL) {
        path = ".";
    }

    DIR *dp = opendir(path);
    if (dp == NULL) {
        printf("Couldn't open '%s': %s", path, strerror(errno));
        return MENU_ERROR;
    }

    struct dirent *dir;
    while ((dir = readdir(dp)) != NULL) {
        printf("\r\n%s", dir->d_name);
    }
    closedir(dp);
    return MENU_SUCCESS;
}

int menu_type(char *cmd_line, bool included)
{
    (void) included;
    const char *path = menu_next_cmd(&cmd_line);
    if (path == NULL) {
        return MENU_WRONG_ARGS;
    }

    FILE *fp = fopen(path, "rb");
    if (fp == NULL) {
        printf("Couldn't open '%s': %s", path, strerror(errno));
        return MENU_ERROR;
    }

    printf("\r\n");

    char buff[32];
    size_t size;
    while ((size = fread(buff, 1, sizeof(buff), fp)) > 0) {
        fwrite(buff, 1, size, stdout);
    }

    fclose(fp);
    return MENU_SUCCESS;
}

int menu_read(char *cmd_line, bool included)
{
    const char *fname = menu_next_cmd(&cmd_line);
    if (fname == NULL) {
        return MENU_WRONG_ARGS;
    }

    if (included) { /* If exit allowed in recursive read it makes it hard to close all opened file handles */
        printf("Recursive reads are not allowed");
        return MENU_ERROR;
    }

    FILE *fp = fopen(fname, "r");
    if (fp == NULL) {
        printf("Couldn't open '%s': %s", fname, strerror(errno));
        return MENU_ERROR;
    }

    printf("\r\n");
    char buff[80];
    char *cmd;

    while ((cmd = fgets(buff, sizeof(buff)-1, fp)) != NULL) {
        /* NOTE: fgets already puts a '\0' at the end */
        int len = strlen(buff);
        if (buff[len-1] != '\n') {
            printf("Line too long: %s", buff);
        } else {
            cmd[len-1] = '\0'; /* menu_next_cmd expects no \n */
            menu_exec(cmd, true);
        }
    }

    fclose(fp);
    return MENU_SUCCESS;
}

int menu_freq(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    float freq = runtime_usecs == 0 ? 0 : (float)cpu.cycles / runtime_usecs;
    printf("Average freq: %.03fMHz", freq);
    return MENU_SUCCESS;
}

int menu_clock(char *cmd_line, bool included)
{
    (void) included;
    const char *arg = menu_next_cmd(&cmd_line);
    if (arg == NULL) {
        return MENU_WRONG_ARGS;
    } else if (strcmp(arg, "fast") == 0) {
        freq_MHz = -1.0f;
    } else if (strcmp(arg, "normal") == 0) {
        freq_MHz = 2.0f;
    } else {
        freq_MHz = atof(arg);
    }
    return MENU_SUCCESS;
}

int menu_stop(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    cpu.wait = true;
    return MENU_SUCCESS;
}

int menu_run(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    cpu.wait = false;
    return MENU_SUCCESS;
}

int menu_step(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    cpu.wait = false;
    cpu_step(&cpu);
    cpu.wait = true;
    return MENU_SUCCESS;
}

int menu_reset(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    cpu.reset = true;
    cpu.wait = false;
    cpu_step(&cpu);
    cpu.wait = true;
    return MENU_SUCCESS;
}

int menu_jmp(char *cmd_line, bool included)
{
    (void) included;
    const char *arg = menu_next_cmd(&cmd_line);
    if (arg == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(arg);
    printf("Jumping to %04x", addr);
    cpu.PC = addr;
    return MENU_SUCCESS;
}

int menu_ex(char *cmd_line, bool included)
{
    (void) included;
    const char *saddr = menu_next_cmd(&cmd_line);
    if (saddr == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(saddr);

    char c;
    do {
        printf("\r\n%04x: ", addr);
        do {
            printf("%02hhx ", cpu_user_load(&cpu, addr++));
        } while (addr % 8 != 0);

        c = menu_getc();
    } while (c == ' ');
    return MENU_SUCCESS;
}

int menu_dep(char *cmd_line, bool included)
{
    (void) included;
    const char *arg = menu_next_cmd(&cmd_line);
    if (arg == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(arg);
    printf("\r\n%04x: ", addr);

    while (1) {
        int byte = 0;
        for (int i = 0; i < 2; i++) {
            char c = menu_getc();
            byte <<= 4;
            if (c >= 'A' && c <= 'F') {
                byte |= c - 'A' + 10;
            } else if (c >= 'a' && c <= 'f') {
                byte |= c - 'a' + 10;
            } else if (c >= '0' && c <= '9') {
                byte |= c - '0';
            } else {
                return MENU_SUCCESS;
            }
            putchar(c);
        }

        putchar(' ');
        cpu_user_store(&cpu, addr++, byte);
        if (addr % 8 == 0) {
            printf("\r\n%04x: ", addr);
        }
    }
    return MENU_SUCCESS;
}

int menu_sense(char *cmd_line, bool included)
{
    (void) included;
    const char *arg = menu_next_cmd(&cmd_line);
    if (arg == NULL) {
        return MENU_WRONG_ARGS;
    }
    sense = menu_hextoi(arg);
    printf("Sense %02hhx", sense);
    return MENU_SUCCESS;
}

int menu_regs(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    printf("\r\n PSW %04x"
           "\r\n BC  %04x"
           "\r\n DE  %04x"
           "\r\n HL  %04x"
           "\r\n SP  %04x"
           "\r\n PC  %04x",
        cpu.PSW, cpu.BC, cpu.DE, cpu.HL, cpu.SP, cpu.PC
    );
    return MENU_SUCCESS;
}

int menu_load(char *cmd_line, bool included)
{
    (void) included;
    const char *saddr = menu_next_cmd(&cmd_line);
    if (saddr == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(saddr);

    const char *fname = menu_next_cmd(&cmd_line);
    if (fname == NULL) {
        return MENU_WRONG_ARGS;
    }

    FILE *fp = fopen(fname, "rb");
    if (fp == NULL) {
        printf("Couldn't open '%s': %s", fname, strerror(errno));
        return MENU_ERROR;
    }

    size_t load_size = MEM_SIZE - addr;
    size_t actual_size = fread(mem + addr, 1, load_size, fp);
    fclose(fp);
    printf("Loaded %zu bytes at %04x from %s", actual_size, addr, fname);
    return MENU_SUCCESS;
}

int menu_dump(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    const char *saddr = menu_next_cmd(&cmd_line);
    if (saddr == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(saddr);

    const char *ssize = menu_next_cmd(&cmd_line);
    if (saddr == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t size = menu_hextoi(ssize);

    const char *fname = menu_next_cmd(&cmd_line);
    if (fname == NULL) {
        return MENU_WRONG_ARGS;
    }

    FILE *fp = fopen(fname, "wb");
    if (fp == NULL) {
        printf("Couldn't open '%s': %s", fname, strerror(errno));
        return MENU_ERROR;
    }

    size_t actual_size = fwrite(mem + addr, 1, size, fp);
    printf("Dumped %zu bytes from %04x to %s", actual_size, addr, fname);
    return MENU_SUCCESS;
}

int menu_disk(char *cmd_line, bool included)
{
    (void) included;
    const char *sdisk = menu_next_cmd(&cmd_line);
    if (sdisk == NULL) {
        return MENU_WRONG_ARGS;
    }
    int disk = atoi(sdisk);

    const char *fname = menu_next_cmd(&cmd_line);
    if (!disk_attach(disk, fname)) { /* If fname is NULL it ejects */
        printf("Couldn't open '%s'", fname);
        return MENU_ERROR;
    }

    printf("Inserted #%d %s ", disk, fname);
    return MENU_SUCCESS;
}

int menu_tape(char *cmd_line, bool included)
{
    (void) included;
    const char *fname = menu_next_cmd(&cmd_line);
    if (!tty_tape_in(fname)) {
        printf("Couldn't open '%s': %s", fname, strerror(errno));
        return MENU_ERROR;
    }
    return MENU_SUCCESS;
}

int menu_punch(char *cmd_line, bool included)
{
    (void) included;
    const char *fname = menu_next_cmd(&cmd_line);
    if (!tty_tape_out(fname)) {
        printf("Couldn't open '%s': %s", fname, strerror(errno));
        return MENU_ERROR;
    }
    return MENU_SUCCESS;
}

const menu_binding_t menu_bindings[] = {
    { "help",    menu_help,     "", "Print this message"                  },
    { "quit",    menu_quit,     "", "Force quit emulator"                 },
    { "exit",    menu_quit,     "", "Force quit emulator"                 },
    { "dir",     menu_dir,      "", "Emulator CWD listing"                },
    { "type",    menu_type,     "", "Show contents of file"               },
    { "read",    menu_read,     "", "Read commands from file"             },
    { "freq",    menu_freq,     "", "Print average clock frequency since last command" },
    { "clock",   menu_clock,    "(fast|normal|<MHz>)", "Set clock speed" },
    { "stop",    menu_stop,     "", "Halt" },
    { "run",     menu_run,      "", "Run" },
    { "step",    menu_step,     "", "Step" },
    { "reset",   menu_reset,    "", "Reset" },
    { "regs",    menu_regs,     "", "Print registers" },
    { "jmp",     menu_jmp,      "<addr>", "Jump" },
    { "ex",      menu_ex,       "<addr>", "Examine (space for more)" },
    { "dep",     menu_dep,      "<addr>", "Deposit" },
    { "sense",   menu_sense,    "<data>", "Set sense switches" },
    { "load",    menu_load,     "<addr> <file>", "Load binary file to RAM" },
    { "dump",    menu_dump,     "<addr> <size> <file>", "Dump RAM to file" },
    { "disk",    menu_disk,     "<n> [<file>]", "Load disk image file in drive #n (no file to eject)" },
    { "tape",    menu_tape,     "[<file>]", "Read tape file (no arg to stops)" },
    { "punch",   menu_punch,    "[<file>]", "Write tape file (no arg to stops)" },
};

#define MENU_NB_BINDINGS (sizeof(menu_bindings)/sizeof(*menu_bindings))

int menu_help(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    for (size_t i = 0; i < MENU_NB_BINDINGS; i++) {
        printf("\r\n  %s %s %s",
            menu_bindings[i].name,
            menu_bindings[i].params,
            menu_bindings[i].desc);
    }
    return MENU_SUCCESS;
}

void menu_exec(char *cmd_line, bool included) /* Mutates contents cmd_line ! */
{
    if (included) {
        printf("> %s ", cmd_line);
    }

    const char *cmd = menu_next_cmd(&cmd_line);

    if (cmd == NULL) {
        goto exit_menu;
    }

    for (size_t i = 0; i < MENU_NB_BINDINGS; i++) {
        if (strcmp(cmd, menu_bindings[i].name) == 0) {
            if (menu_bindings[i].func == NULL) {
                printf("NOT IMPLEMENTED: %s", cmd);
            } else if (menu_bindings[i].func(cmd_line, included) == MENU_WRONG_ARGS) {
                printf("Usage: %s %s %s",
                    menu_bindings[i].name,
                    menu_bindings[i].params,
                    menu_bindings[i].desc);
            }
            goto exit_menu;
        }
    }

    printf("Unknown command '%s', type 'help' for help", cmd);

exit_menu:
    printf("\r\n");
}

void menu_toplevel(void)
{
    while (1) {
        if (cpu.wait) {
            printf("STOP ");
        }

        printf("AQUILA> ");

        char buff[80];
        if (menu_readline(buff, sizeof(buff)) < 0) {
            printf("\r\n");
            break;
        }

        putchar(' ');
        menu_exec(buff, false);
    }

    runtime_usecs = 0;
    cpu.cycles = 0;
}


/* Main program */

static void cleanup(void)
{
    tty_uninit();
    disk_uninit();
}

int main(int argc, char **argv)
{
    if (argc > 1 && strcmp(argv[1], "--help") == 0) {
        printf("Usage: %s [command file]\n", argv[0]);
        return 0;
    }

    tty_init(); /* Goes to raw mode */
    disk_init();

    /* Parse args */
    for (int i = 1; i < argc; i++) {
        menu_exec(argv[i], false);
    }

    /* Run until halted */
    runtime_usecs = 0;
    uint64_t last = time_usecs();
    uint64_t last_input = 0;

    while (1) {
        uint64_t now = time_usecs();
        uint64_t dt = now - last;
        last = now;

        if (!cpu.wait) {
            runtime_usecs += dt;

            /* Cap at 2Mhz */
            int max_cycles = freq_MHz*runtime_usecs - cpu.cycles;
            if (freq_MHz < 0.0f || max_cycles > 10000) {
                max_cycles = 10000;
            }

            /* Update CPU */
            /* It is way faster to do this in batches instead of getting the time each iteration */
            int cycles = 0;
            while (cycles < max_cycles) {
                 cycles += cpu_step(&cpu);
            }

        } else {
            menu_toplevel();
        }

        /* TODO: limit tape speed, basic expects the port to be busy
        and skips bytes if we send too fast, this is so weird
            TODO: emulate baud rate
            TODO: check if basic actually does that or what */
        if (now - last_input > 1000*15) {
            last_input = now;
            tty_update();

            if (tty_peek() == MENU_KEY) {
                tty_input(0);
                menu_toplevel();
            }
        }

        // TODO: dcdd_update(); // Spin disks
    }

    cleanup();
    return 0;
}

