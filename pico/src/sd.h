#ifndef SD_H
#define SD_H

/* SPI SD card driver. Read and write sectors. */

#include <stdint.h>
#include <stdbool.h>

#include "hardware/spi.h"

#define SD_SECTOR_SIZE 512

typedef enum {
    SD_INVALID = 0,
    SD_V1,
    SD_V2SC,
    SD_V2HC,
} sd_version_t;

static inline const char *sd_version_str(sd_version_t v)
{
    switch (v) {
        case SD_V1:   return "V1";
        case SD_V2SC: return "V2-SC";
        case SD_V2HC: return "V2-HC";
        default:      return "INVALID";
    }
}

typedef struct {
    /* Set by user */
    unsigned int pin_mosi, pin_miso, pin_sck, pin_csn;
    spi_inst_t *spi;

    /* Set by program */
    sd_version_t version;
    uint64_t num_sectors;
    bool init;
} sd_t;

bool sd_init(sd_t *sd); /* Init SPI communication with the SD card */

bool sd_read(sd_t  *sd,       uint8_t data[/* SEC_SIZE * NUM */], uint32_t start_sector, uint32_t num_sectors);
bool sd_write(sd_t *sd, const uint8_t data[/* SEC_SIZE * NUM */], uint32_t start_sector, uint32_t num_sectors);

#endif
