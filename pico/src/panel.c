#include "panel.h"

#include "hardware/gpio.h"

/* SPI (we are not using the hardware SPI) */
#define PIN_CLK     10
#define PIN_LATCH   13
#define PIN_MOSI    11
#define PIN_MISO    12

/* Other pins */
#define PIN_STATUS_0 14
#define PIN_STATUS_1 15
#define PIN_STATUS_2 20
#define PIN_STATUS_3 21
#define PIN_POWER_SW 22
#define PIN_STATUS_HLDA PIN_STATUS_2
#define PIN_STATUS_INTE PIN_STATUS_1
#define PIN_STATUS_PROT PIN_STATUS_3
#define PIN_STATUS_WAIT PIN_STATUS_0


/* Wait for 150ns (see SN74HC166 datasheet) :
    Do 20 NOPs, at 133MHz one NOP is 7.5ns */
#define SPI_DELAY() do { \
    __asm volatile ("nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n"); \
    __asm volatile ("nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n"); \
} while (0)


const char * const panel_ctrlstr[] = {
    "STOP",
    "RUN",
    "SLOW",
    "STEP",
    "EX",
    "EXNEXT",
    "DEPNEXT",
    "DEP",
    "RESET",
    "CLEAR",
    "UNPROT",
    "PROT",
    "AUX1_UP",
    "AUX1_DN",
    "AUX2_DN",
    "AUX2_UP",
    "NONE",
};

void panel_init(void)
{
    gpio_init(PIN_MOSI);
    gpio_init(PIN_MISO);
    gpio_init(PIN_LATCH);
    gpio_init(PIN_CLK);
    gpio_set_dir(PIN_MOSI,  GPIO_OUT);
    gpio_set_dir(PIN_MISO,  GPIO_IN);
    gpio_set_dir(PIN_LATCH, GPIO_OUT);
    gpio_set_dir(PIN_CLK,   GPIO_OUT);

    gpio_init(PIN_STATUS_0);
    gpio_init(PIN_STATUS_1);
    gpio_init(PIN_STATUS_2);
    gpio_init(PIN_STATUS_3);
    gpio_init(PIN_POWER_SW);
    gpio_set_dir(PIN_STATUS_0, GPIO_OUT);
    gpio_set_dir(PIN_STATUS_1, GPIO_OUT);
    gpio_set_dir(PIN_STATUS_2, GPIO_OUT);
    gpio_set_dir(PIN_STATUS_3, GPIO_OUT);
    gpio_set_dir(PIN_POWER_SW, GPIO_IN);
}

void panel_io(panel_t *p)
{
    gpio_put(PIN_STATUS_HLDA, p->led_hlda);
    gpio_put(PIN_STATUS_INTE, p->led_inte);
    gpio_put(PIN_STATUS_PROT, p->led_prot);
    gpio_put(PIN_STATUS_WAIT, p->led_wait);

    uint32_t data_out = p->led_addr | (p->led_data << 16) | (p->led_stat << 24);
    uint32_t data_in = 0;

    data_out = __builtin_bswap32(data_out);

    /* 74x166: Enable loading, load and redisable loading */
    gpio_put(PIN_LATCH, 0); SPI_DELAY();
    gpio_put(PIN_CLK,   1); SPI_DELAY(); // Will also shift the 74x595 (?)
    gpio_put(PIN_CLK,   0); SPI_DELAY();
    gpio_put(PIN_LATCH, 1); SPI_DELAY();

    for (int bit = 0; bit < 32; bit++) {
        gpio_put(PIN_MOSI, data_out & (1<<31));
        data_out <<= 1;

        data_in  >>= 1;
        data_in |= gpio_get(PIN_MISO) << 31;

        /* Shift */
        if (bit != 31) { // Why is it needed ? Otherwise output is shifted by one
            gpio_put(PIN_CLK, 1); SPI_DELAY();
            gpio_put(PIN_CLK, 0); SPI_DELAY();
        }
    }

    /* 74x595: Save outputs */
    gpio_put(PIN_LATCH, 1);
    gpio_put(PIN_LATCH, 0);

    p->data_in = data_in;
}

void panel_update(panel_t *p)
{
    uint32_t data_in = p->data_in;

    p->sw_addr  = data_in & 0xffff;
    p->sw_data  = data_in & 0xff;
    p->sw_sense = (data_in >> 8) & 0xff;
    p->sw_power = gpio_get(PIN_POWER_SW);

    uint16_t last_ctrl = p->sw_ctrl;
    p->sw_ctrl = (data_in >> 16) & 0xffff;
    uint16_t change = p->sw_ctrl & ~last_ctrl; /* If a button wasn't released then it shouldn't be register again */
    p->event = CTRL_NONE;
    for (int i = 0; i < 16; i++) {
        if (change & (1 << 15)) {
            p->event = i;
            break;
        }
        change <<= 1;
    }
}
