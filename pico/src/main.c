#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <pico/stdlib.h>
#include <pico/bootrom.h>
#include <pico/multicore.h>

#include "ff.h" // TODO: wrap stdio instead of using ff
#include "diskio.h"

#include "i8080.h"
#include "devices.h"
#include "panel.h"


/* Necessary declarations */

static void cleanup(void);
static void menu(void);


/* Timing */

static float freq_MHz = 2.0f;
static uint64_t runtime_usecs = 0; /* CPU real runtime since last menu command */
#define time_usecs() time_us_64()


/* CPU */

static cpu_t cpu = {
    .wait = true,
};


/* Front panel */

static panel_t panel = { 0 };


/* RAM */

// TODO: bank switching, read-only areas...
#define MEM_SIZE 0x10000
static data_t mem[MEM_SIZE] = {0};

data_t cpu_user_load(cpu_t *cpu, addr_t addr)
{
    (void)cpu;
    data_t data = mem[addr];
    panel.led_addr = addr;
    panel.led_data = data;
    return data;
}

void cpu_user_store(cpu_t *cpu, addr_t addr, data_t data)
{
    (void)cpu;
    mem[addr] = data;
    panel.led_addr = addr;
    panel.led_data = data;
}

/* IO devices */

static sio_t sio = {
    .base_port = SIO_DEFAULT_PORT,
};

static sio2_t sio2 = {
    .base_port = SIO2_DEFAULT_PORT,
};

static dcdd_t dcdd = {
    .present_drives = (1 << 0), /* Only drive 0 */
};


/* CPU IO callbacks */

static data_t sense = 0x00;

data_t cpu_user_input(cpu_t *cpu, port_t port)
{
    (void)cpu;
    data_t data = 0x00;
    data |= sio_update_read(&sio,   port);
    data |= sio2_update_read(&sio2, port);
    data |= dcdd_update_read(&dcdd, port);

    if (port == SENSE_PORT) {
        data |= sense;
        data |= panel.sw_sense;
    }

    panel.led_addr = port << 8 | port;
    panel.led_data = data;

    return data;
}

void cpu_user_output(cpu_t *cpu, port_t port, data_t data)
{
    (void)cpu;
    sio_update_write(&sio,   port, data);
    sio2_update_write(&sio2, port, data);
    dcdd_update_write(&dcdd, port, data);

    if (port == SENSE_PORT) {
        panel.led_stat = data;
    }

    panel.led_addr = port << 8 | port;
    panel.led_data = data;
}


/* Emulator shell */

#define MENU_KEY 1 /* The character C^A */
#define menu_getc() getchar()

FATFS fs; // TODO: wrap stdio so both versions of the emulator share most functions

static uint32_t menu_hextoi(const char *s)
{
    uint32_t res = 0;
    char c;
    while ((c = *(s++)) != '\0') {
        res <<= 4;
        if (c >= 'A' && c <= 'F') {
            res |= c - 'A' + 10;
        } else if (c >= 'a' && c <= 'f') {
            res |= c - 'a' + 10;
        } else if (c >= '0' && c <= '9') {
            res |= c - '0';
        } else {
            return 0;
        }
    }
    return res;
}

static int menu_readline(char buff[], int buff_sz)
{
    int i = 0;

    while (1) {
        char c = menu_getc();

        if (i >= buff_sz-1) { /* End of buffer */
            break;
        }

        if (c == '\r' || c == '\n') { /* End of line */
            break;
        }

        if (c == 0x7f && i > 0) { /* Del/backspace */
            putchar('\b');
            putchar(' ');
            putchar('\b');
            i--;
            buff[i] = '\0';
        } else if (c >= 0x20 && c <= 0x7e) {
            putchar(c); /* Echo */
            buff[i++] = c;
        } else if (c == 4 && i == 0) { /* C^D Return exit status */
            buff[0] = '\0';
            return -1;
        } else if (c == 3 || c == 4) { /* Return empty command (cancel) */
            buff[0] = '\0';
            return 0;
        } else {
            /* Ignore this char */
        }
    }

    buff[i] = '\0';
    return i;
}

/* Return current the next command by returning the current line pointer,
    put a \0 at the end if it in the buffer, and advance line to
    start of next space-separated word.
   Return NULL if it was the end of the buffer */
static const char *menu_next_cmd(char **line)
{
    char c;
    const char *res = NULL;

    /* Skip spaces */
    while ((c = **line) != '\0' && c == ' ')  {
        (*line)++;
    }

    /* No word or comment */
    if (c == '\0' || c == '#') {
        return res;
    }

    res = *line;

    /* Skip current word */
    while ((c = **line) != '\0' && c != '#' && c != ' ')  {
        (*line)++;
    }

    /* Set end of word */
    if (c != '\0') {
        **line = '\0';
        (*line)++;
    }

    return res;
}

#define MENU_SUCCESS    0
#define MENU_ERROR      1
#define MENU_WRONG_ARGS 2
typedef int (menu_func_t)(char *cmd, bool included);

typedef struct {
    const char *name;
    menu_func_t *func;
    const char *params, *desc;
} menu_binding_t;

menu_func_t menu_help;
void menu_exec(char *cmd_line, bool included);

int menu_quit(char *cmd_line, bool included)
{
    (void)cmd_line;
    if (included) {
        printf("Cannot exit while reading file");
        return MENU_ERROR;
    }

    printf("Closing all files"); // TODO: coldstart
    cleanup();
    cpu.hold = true;
    return MENU_SUCCESS;
}

int menu_ptest(char *cmd_line, bool included)
{
    (void)cmd_line; (void) included;
    do {
        panel_update(&panel);
        printf("\r\n  Event: %-7s Pwr: %d Sense: %02x Data: %02x Addr: %04x",
            panel_ctrlstr[panel.event],
            panel.sw_power,
            panel.sw_sense,
            panel.sw_data,
            panel.sw_addr);

        panel.led_data = panel.sw_data;
        panel.led_stat = panel.sw_sense;
        panel.led_addr = panel.sw_addr;

        sleep_ms(100);
    } while (getchar_timeout_us(0) == PICO_ERROR_TIMEOUT);
    return MENU_SUCCESS;
}

int menu_term(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;

    stdio_set_driver_enabled(&stdio_uart, false); /* Prevent echo on uart device */

    while (1) {
        if (uart_is_readable(uart0)) {
            char c = uart_getc(uart0);
            if (c == MENU_KEY) {
                break;
            }
            putchar_raw(c);
        }

        int c = getchar_timeout_us(0);
        if (c == MENU_KEY) {
            break;
        }
        if (c != PICO_ERROR_TIMEOUT) {
            uart_putc_raw(uart0, c);
        }
    }

    stdio_set_driver_enabled(&stdio_uart, true);

    return MENU_SUCCESS;
}

int menu_boot(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    stdio_set_driver_enabled(&stdio_usb, false);
    reset_usb_boot(0, 0);
    return MENU_SUCCESS;
}

int menu_dir(char *cmd_line, bool included)
{
    (void) included;
    const char *path = menu_next_cmd(&cmd_line);
    if (path == NULL) {
        path = "/";
    }

    DIR dir;
    FRESULT fr = f_opendir(&dir, path);
    if (fr != FR_OK) {
        printf("Couldn't open '%s': %d", path, fr);
        return MENU_ERROR;
    }

    while (1) {
        FILINFO fno;
        FRESULT fr = f_readdir(&dir, &fno);
        if (fr != FR_OK) {
            printf("Error occured: %d", fr);
            break;
        }

        if (fno.fname[0] == '\0') {
            break;
        }

        if (fno.fattrib & AM_DIR) { /* Directory */
            printf("\r\n   <DIR>   %s", fno.fname);
        } else { /* File */
            printf("\r\n%10lu %s", fno.fsize, fno.fname);
        }
    }

    f_closedir(&dir);
    return MENU_SUCCESS;
}

int menu_rm(char *cmd_line, bool included)
{
    (void) included;
    const char *path = menu_next_cmd(&cmd_line);
    if (path == NULL) {
        return MENU_WRONG_ARGS;
    }

    FRESULT fr = f_unlink(path);
    if (fr != FR_OK) {
        printf("Couldn't remove %s: %d", path, fr);
        return MENU_ERROR;
    }

    printf("Removed %s", path);
    return MENU_SUCCESS;
}

int menu_mv(char *cmd_line, bool included)
{
    (void) included;
    const char *path = menu_next_cmd(&cmd_line);
    const char *new  = menu_next_cmd(&cmd_line);
    if (path == NULL || new == NULL) {
        return MENU_WRONG_ARGS;
    }

    FRESULT fr = f_rename(path, new);
    if (fr != FR_OK) {
        printf("Couldn't mov %s: %d", path, fr);
        return MENU_ERROR;
    }

    printf("Moved %s to %s", path, new);
    return MENU_SUCCESS;
}

int menu_type(char *cmd_line, bool included)
{
    (void) included;
    const char *path = menu_next_cmd(&cmd_line);
    if (path == NULL) {
        return MENU_WRONG_ARGS;
    }

    FIL file;
    FRESULT fr = f_open(&file, path, FA_READ);
    if (fr != FR_OK) {
        printf("Couldn't open '%s': %d", path, fr);
        return MENU_ERROR;
    }

    printf("\r\n");

    char buff[32];
    UINT size;
    while ((fr = f_read(&file, buff, sizeof(buff), &size)) == FR_OK && size > 0) {
        fwrite(buff, 1, size, stdout);
    }

    if (fr != FR_OK) {
        printf("Error occured: %d", fr);
        f_close(&file);
        return MENU_SUCCESS;
    }

    f_close(&file);
    return MENU_SUCCESS;
}

int menu_xdump(char *cmd_line, bool included)
{
    (void) included;
    const char *path = menu_next_cmd(&cmd_line);
    if (path == NULL) {
        return MENU_WRONG_ARGS;
    }

    FIL file;
    FRESULT fr = f_open(&file, path, FA_READ);
    if (fr != FR_OK) {
        printf("Couldn't open '%s': %d", path, fr);
        return MENU_ERROR;
    }

    uint8_t byte;
    UINT addr = 0;
    UINT size;
    while ((fr = f_read(&file, &byte, 1, &size)) == FR_OK && size > 0) {
        if (addr % 8 == 0) {
            printf("\r\n%04hx ", addr);
        }
        addr++;
        printf("%02hhx ", byte);
    }

    if (fr != FR_OK) {
        printf("Error occured: %d", fr);
        f_close(&file);
        return MENU_SUCCESS;
    }

    f_close(&file);
    return MENU_SUCCESS;
}

int menu_read(char *cmd_line, bool included)
{
    const char *fname = menu_next_cmd(&cmd_line);
    if (fname == NULL) {
        return MENU_WRONG_ARGS;
    }

    if (included) { /* It make us have to deal with closing more files (?) */
        printf("Recursive reads are not allowed");
        return MENU_ERROR;
    }

    FIL file;
    FRESULT fr = f_open(&file, fname, FA_READ);
    if (fr != FR_OK) {
        printf("Couldn't open '%s': %d", fname, fr);
        return MENU_ERROR;
    }

    printf("\r\n");
    char buff[80];

    while (1) {
        /* Rad line from file */
        size_t i = 0;
        while (1) { 
            char c;
            UINT size;
            FRESULT fr = f_read(&file, &c, 1, &size);

            if (size == 0 || fr != FR_OK) {
                goto end_read; /* EOF: stop executing commands */
            }

            if (c == '\r' || c == '\n') {
                goto end_readline;
            }

            buff[i++] = c;

            if (i >= sizeof(buff)-1) {
                break;
            }
        }
end_readline:
        buff[i] = '\0';

        printf("> %s ", buff);
        menu_exec(buff, true);
    }
end_read:

    f_close(&file);
    return MENU_SUCCESS;
}

int menu_freq(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    float freq = runtime_usecs == 0 ? 0 : (float)cpu.cycles / runtime_usecs;
    printf("Average freq: %.03fMHz", freq);
    return MENU_SUCCESS;
}

int menu_clock(char *cmd_line, bool included)
{
    (void) included;
    const char *arg = menu_next_cmd(&cmd_line);
    if (arg == NULL) {
        return MENU_WRONG_ARGS;
    } else if (strcmp(arg, "fast") == 0) {
        freq_MHz = -1.0f;
    } else if (strcmp(arg, "normal") == 0) {
        freq_MHz = 2.0f;
    } else {
        freq_MHz = atof(arg);
    }
    return MENU_SUCCESS;
}

int menu_stop(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    cpu.wait = true;
    return MENU_SUCCESS;
}

int menu_run(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    cpu.wait = false;
    return MENU_SUCCESS;
}

int menu_step(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    cpu.wait = false;
    cpu_step(&cpu);
    cpu.wait = true;
    // runtime_usecs += 2; /* Not really worth accounting for it */
    return MENU_SUCCESS;
}

int menu_reset(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    cpu.wait = false;
    cpu.reset = true;
    cpu_step(&cpu);
    cpu.wait = true;
    return MENU_SUCCESS;
}

int menu_jmp(char *cmd_line, bool included)
{
    (void) included;
    const char *arg = menu_next_cmd(&cmd_line);
    if (arg == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(arg);
    printf("Jumping to %04x", addr);
    cpu.PC = addr;
    return MENU_SUCCESS;
}

int menu_ex(char *cmd_line, bool included)
{
    (void) included;
    const char *saddr = menu_next_cmd(&cmd_line);
    if (saddr == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(saddr);

    char c;
    do {
        printf("\r\n%04x: ", addr);
        do {
            printf("%02hhx ", cpu_user_load(&cpu, addr++));
        } while (addr % 8 != 0);

        c = menu_getc();
    } while (c == ' ');
    return MENU_SUCCESS;
}

int menu_dep(char *cmd_line, bool included)
{
    (void) included;
    const char *arg = menu_next_cmd(&cmd_line);
    if (arg == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(arg);
    printf("\r\n%04x: ", addr);

    while (1) {
        int byte = 0;
        for (int i = 0; i < 2; i++) {
            char c = menu_getc();
            byte <<= 4;
            if (c >= 'A' && c <= 'F') {
                byte |= c - 'A' + 10;
            } else if (c >= 'a' && c <= 'f') {
                byte |= c - 'a' + 10;
            } else if (c >= '0' && c <= '9') {
                byte |= c - '0';
            } else {
                return MENU_SUCCESS;
            }
            putchar(c);
        }

        putchar(' ');
        cpu_user_store(&cpu, addr++, byte);
        if (addr % 8 == 0) {
            printf("\r\n%04x: ", addr);
        }
    }
    return MENU_SUCCESS;
}

int menu_sense(char *cmd_line, bool included)
{
    (void) included;
    const char *arg = menu_next_cmd(&cmd_line);
    if (arg == NULL) {
        return MENU_WRONG_ARGS;
    }
    sense = menu_hextoi(arg);
    printf("Sense %02hhx", sense);
    return MENU_SUCCESS;
}

int menu_regs(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    printf("\r\n PSW %04x"
           "\r\n BC  %04x"
           "\r\n DE  %04x"
           "\r\n HL  %04x"
           "\r\n SP  %04x"
           "\r\n PC  %04x",
        cpu.PSW, cpu.BC, cpu.DE, cpu.HL, cpu.SP, cpu.PC
    );
    return MENU_SUCCESS;
}

int menu_load(char *cmd_line, bool included)
{
    (void) included;
    const char *saddr = menu_next_cmd(&cmd_line);
    const char *fname = menu_next_cmd(&cmd_line);
    if (saddr == NULL || fname == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(saddr);

    FIL file;
    FRESULT fr = f_open(&file, fname, FA_READ);
    if (fr != FR_OK) {
        printf("Couldn't open '%s': %d", fname, fr);
        return MENU_ERROR;
    }

    UINT load_size = MEM_SIZE - addr;
    UINT actual_size;
    fr = f_read(&file, mem + addr, load_size, &actual_size);
    f_close(&file);

    printf("Loaded %zu bytes at %04x from %s", actual_size, addr, fname);
    return MENU_SUCCESS;
}

int menu_dump(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    const char *saddr = menu_next_cmd(&cmd_line);
    if (saddr == NULL) {
        return MENU_WRONG_ARGS;
    }
    addr_t addr = menu_hextoi(saddr);

    const char *ssize = menu_next_cmd(&cmd_line);
    if (saddr == NULL) {
        return MENU_WRONG_ARGS;
    }
    UINT size = menu_hextoi(ssize);

    const char *fname = menu_next_cmd(&cmd_line);
    if (fname == NULL) {
        return MENU_WRONG_ARGS;
    }

    FIL file;
    FRESULT fr = f_open(&file, fname, FA_WRITE | FA_CREATE_ALWAYS);
    if (fr != FR_OK) {
        printf("Couldn't open '%s': %d", fname, fr);
        return MENU_ERROR;
    }

    UINT actual_size;
    f_write(&file, mem + addr, size, &actual_size);
    f_close(&file);
    printf("Dumped %zu bytes from %04x to %s", actual_size, addr, fname);
    return MENU_SUCCESS;
}

int menu_disk(char *cmd_line, bool included)
{
    (void) included;
    const char *sdisk = menu_next_cmd(&cmd_line);
    if (sdisk == NULL) {
        return MENU_WRONG_ARGS;
    }
    int disk = atoi(sdisk);

    const char *fname = menu_next_cmd(&cmd_line);
    if (!disk_attach(disk, fname)) { /* If fname is NULL it ejects */
        printf("Couldn't open '%s'", fname);
        return MENU_ERROR;
    }

    printf("Inserted #%d %s ", disk, fname);
    return MENU_SUCCESS;
}

int menu_tape(char *cmd_line, bool included)
{
    (void) included;
    const char *fname = menu_next_cmd(&cmd_line);
    if (!tty_tape_in(fname)) {
        printf("Couldn't open '%s'", fname);
        return MENU_ERROR;
    }
    return MENU_SUCCESS;
}

int menu_punch(char *cmd_line, bool included)
{
    (void) included;
    const char *fname = menu_next_cmd(&cmd_line);
    if (!tty_tape_out(fname)) {
        printf("Couldn't open '%s'", fname);
        return MENU_ERROR;
    }
    return MENU_SUCCESS;
}

const menu_binding_t menu_bindings[] = {
    { "help",    menu_help,     "", "Print this message" },
    { "quit",    menu_quit,     "", "Force quit emulator" },
    { "exit",    menu_quit,     "", "Force quit emulator" },
    { "ptest",   menu_ptest,    "", "Test panel switches and leds" },
    { "term",    menu_term,     "", "Bridge USB and serial so the Minitel can be used as a Linux terminal" },
    { "boot",    menu_boot,     "", "Put device in USB boot mode" },
    { "dir",     menu_dir,      "", "Emulator CWD listing" },
    { "rm",      menu_rm,       "<file>", "Delete file" },
    { "mv",      menu_mv,       "<file> <new>", "Rename/mov file"  },
    { "type",    menu_type,     "<file>", "Show contents of file" },
    { "xdump",   menu_xdump,    "<file>", "Hex dump file" },
    { "read",    menu_read,     "", "Read commands from file" },
    { "freq",    menu_freq,     "", "Print average clock frequency since last command" },
    { "clock",   menu_clock,    "(fast|normal|<MHz>)", "Set clock speed" },
    { "stop",    menu_stop,     "", "Halt" },
    { "run",     menu_run,      "", "Run" },
    { "step",    menu_step,     "", "Step" },
    { "reset",   menu_reset,    "", "Reset" },
    { "regs",    menu_regs,     "", "Print registers" },
    { "jmp",     menu_jmp,      "<addr>", "Jump" },
    { "ex",      menu_ex,       "<addr>", "Examine (space for more)" },
    { "dep",     menu_dep,      "<addr>", "Deposit" },
    { "sense",   menu_sense,    "<data>", "Set sense switches" },
    { "load",    menu_load,     "<addr> <file>", "Load binary file to RAM" },
    { "dump",    menu_dump,     "<addr> <size> <file>", "Dump RAM to file" },
    { "disk",    menu_disk,     "<n> [<file>]", "Load disk image file in drive #n (no file to eject)" },
    { "tape",    menu_tape,     "[<file>]", "Read tape file (no arg to stops)" },
    { "punch",   menu_punch,    "[<file>]", "Write tape file (no arg to stops)" },
};

#define MENU_NB_BINDINGS (sizeof(menu_bindings)/sizeof(*menu_bindings))

int menu_help(char *cmd_line, bool included)
{
    (void) cmd_line; (void) included;
    for (size_t i = 0; i < MENU_NB_BINDINGS; i++) {
        printf("\r\n  %s\t%s\t%s",
            menu_bindings[i].name,
            menu_bindings[i].params,
            menu_bindings[i].desc);
    }
    return MENU_SUCCESS;
}

void menu_exec(char *cmd_line, bool included) /* Mutates contents cmd_line ! */
{
    const char *cmd = menu_next_cmd(&cmd_line);

    if (cmd == NULL) {
        goto exit_menu;
    }

    for (size_t i = 0; i < MENU_NB_BINDINGS; i++) {
        if (strcmp(cmd, menu_bindings[i].name) == 0) {
            if (menu_bindings[i].func == NULL) {
                printf("NOT IMPLEMENTED: %s", cmd);
            } else if (menu_bindings[i].func(cmd_line, included) == MENU_WRONG_ARGS) {
                printf("Usage: %s %s %s",
                    menu_bindings[i].name,
                    menu_bindings[i].params,
                    menu_bindings[i].desc);
            }
            goto exit_menu;
        }
    }

    printf("Unknown command '%s', type 'help' for help", cmd);

exit_menu:
    printf("\r\n");
}

static void menu(void)
{
    while (1) {
        if (cpu.wait) {
            printf("STOP ");
        }

        printf("AQUILA> ");

        char buff[80];
        if (menu_readline(buff, sizeof(buff)) < 0) {
            printf("\r\n");
            break;
        }

        putchar(' ');
        menu_exec(buff, false);
    }

    runtime_usecs = 0;
    cpu.cycles = 0;
}


/* Main program */

static void cleanup(void)
{
    tty_uninit();
    disk_uninit(); /* Closes all files */
    f_unmount("");
    void diskio_uninit(void); diskio_uninit(); /* Uninit SD card */
    multicore_reset_core1();
}

void main_core1(void)
{
    while (1) {
        panel.led_hlda = cpu.hold;
        panel.led_inte = cpu.inte;
        panel.led_prot = false;
        panel.led_wait = cpu.wait;
        panel_io(&panel);
    }
}

int main(void)
{
    stdio_init_all();
    stdio_set_translate_crlf(&stdio_usb,  false);
    stdio_set_translate_crlf(&stdio_uart, false);
    f_mount(&fs, "", 0);
    tty_init();
    disk_init();
    panel_init();
    for (uint32_t i = 0; i < MEM_SIZE; i++) {
        mem[i] = rand();
    }

    multicore_launch_core1(main_core1);

    /* Run until halted */
    runtime_usecs = 0;
    uint64_t last = time_usecs();
    uint64_t last_input = last;

    // TODO: Handle the power button
    while (1) {
        uint64_t now = time_usecs();
        uint64_t dt = now - last;
        last = now;

        enum panel_ctrl event = CTRL_NONE;

        runtime_usecs += dt;

        /* Cap at 2Mhz */
        int max_cycles = freq_MHz*runtime_usecs - cpu.cycles;
        if (freq_MHz < 0.0f || max_cycles > 10000) {
            max_cycles = 10000;
        }

        /* Update CPU */
        /* It is way faster to do this in batches instead of getting the time each iteration */
        int cycles = 0;
        while (cycles < max_cycles) {
            cycles += cpu_step(&cpu);
        }

        panel_update(&panel);
        if (panel.event != CTRL_NONE) { // This is not redundant when in the while loop
            event = panel.event;
        }

        /* Execute button press */
        switch (event)
        {
            case CTRL_RUN :
                cpu.wait = false;
            break;

            case CTRL_STOP :
                cpu.wait = true;
                panel.led_addr = cpu.PC;
                panel.led_data = cpu_user_load(&cpu, cpu.PC);
            break;

            case CTRL_STEP :
                cpu.wait = false;
                cpu_step(&cpu);
                cpu.wait = true;
                panel.led_addr = cpu.PC;
                panel.led_data = cpu_user_load(&cpu, cpu.PC);
            break;

            case CTRL_EX :
                cpu.PC = panel.sw_addr;
                cpu_user_load(&cpu, cpu.PC);
            break;

            case CTRL_EXNEXT :
                cpu_user_load(&cpu, ++cpu.PC);
            break;

           case CTRL_DEP :
                cpu_user_store(&cpu, cpu.PC, panel.sw_data);
            break;

            case CTRL_DEPNEXT :
                cpu_user_store(&cpu, ++cpu.PC, panel.sw_data);
            break;

            case CTRL_RESET :
                cpu.wait = false;
                cpu.reset = true;
                cpu_step(&cpu);
                cpu.wait = true;
                panel.led_data = 0x00;
                panel.led_addr = 0x0000;
            break;

            case CTRL_AUX1_UP:
                menu();
            break;

            default: break;
        }

        /* Update TTY every 15ms otherwise the pico can't manage 2MHz
            and also BASIC skips bytes expecting the port to be busy
            TODO: emulate baud rate
            TODO: check if basic actually does that or what */
        now = time_usecs();
        if (now - last_input > 1000*15) {
            tty_update();
            last_input = now;
        }
    }

    cleanup();
    return 0;
}

