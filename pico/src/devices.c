#include "devices.h"

/* TTY and disk drive implementation with Pico stdio and fatfs */

#include <stdio.h>
#include "pico/stdlib.h"

#include "ff.h"

/* Compatible with Minitel serial */
#define UART0_BAUD        4800
#define UART0_DATA_BITS   7
#define UART0_STOP_BITS   1
#define UART0_PARITY      UART_PARITY_EVEN
#define UART0_TX          0
#define UART0_RX          1

#define UART1_BAUD        4800
#define UART1_DATA_BITS   7
#define UART1_STOP_BITS   1
#define UART1_PARITY      UART_PARITY_EVEN
#define UART1_TX          4
#define UART1_RX          5


static FIL *tape_in, *tape_out;
static int buff_in, buff_out;

void tty_init(void)
{
    /* Init uart0 */
    uart_init(uart0, UART0_BAUD);
    uart_set_format(uart0, UART0_DATA_BITS, UART0_STOP_BITS, UART0_PARITY);
    uart_set_hw_flow(uart0, false, false);
    uart_set_fifo_enabled(uart0, false);
    gpio_set_function(UART0_TX, GPIO_FUNC_UART);
    gpio_set_function(UART0_RX, GPIO_FUNC_UART);
    //gpio_disable_pulls(UART0_RX);
    gpio_pull_up(UART0_RX);

    /* Init UART1 */
    uart_init(uart1, UART1_BAUD);
    uart_set_format(uart1, UART1_DATA_BITS, UART1_STOP_BITS, UART1_PARITY);
    uart_set_hw_flow(uart1, false, false);
    uart_set_fifo_enabled(uart1, false);
    gpio_set_function(UART1_TX, GPIO_FUNC_UART);
    gpio_set_function(UART1_RX, GPIO_FUNC_UART);
    //gpio_disable_pulls(UART1_RX);
    gpio_pull_up(UART1_RX);
}

void tty_uninit(void)
{
    if (tape_in != NULL) {
        f_close(tape_in);
    }

    if (tape_out != NULL) {
        f_close(tape_out);
    }
}

void tty_update(void)
{
    int c;
    if ((c = getchar_timeout_us(0)) != PICO_ERROR_TIMEOUT) {
        buff_in = c;
    } else if (tape_in != NULL) {
        UINT size;
        data_t c;
        FRESULT fr = f_read(tape_in, &c, 1, &size);
        if (fr != FR_OK || size != 1) {
            f_close(tape_in);
            tape_in = NULL;
        } else {
            buff_in = c;
        }
    }

    if (buff_out >= 0) {
        data_t data = buff_out; // TODO: & 0x7f if not punching tape (?) Can't find an option to strip the bit in picocom...
        putchar(data);
        if (tape_out != NULL) {
            UINT size;
            f_write(tape_out, &data, 1, &size);
        }
        buff_out = -1;
    }
}

bool tty_input_ready(port_t port)
{
    (void)port;
    return buff_in >= 0;
}

bool tty_output_ready(port_t port)
{
    (void)port;
    return buff_out < 0;
}

data_t tty_input(port_t port)
{
    (void)port;
    if (buff_in < 0) {
        return 0;
    }

    data_t data = (data_t)buff_in;
    buff_in = -1;
    return data;
}

void tty_output(port_t port, data_t data)
{
    (void)port;
    buff_out = data;
}

data_t tty_peek(void)
{
    if (buff_in < 0) {
        return 0;
    }

    return (data_t)buff_in;
}

bool tty_tape_in(const char *fname)
{
    static FIL tape;

    if (tape_in != NULL) {
        f_close(tape_in);
        tape_in = NULL;
    }

    if (fname == NULL) {
        return true;
    }

    if (f_open(&tape, fname, FA_READ) != FR_OK) {
        return false;
    }

    tape_in = &tape;
    return true;
}

bool tty_tape_out(const char *fname)
{
    static FIL tape;

    if (tape_out != NULL) {
        f_close(tape_out);
        tape_out = NULL;
    }

    if (fname == NULL) {
        return true;
    }

    if (f_open(&tape, fname, FA_WRITE | FA_CREATE_ALWAYS) != FR_OK) {
        return false;
    }

    tape_out = &tape;
    return true;
}



static FIL *files[DCDD_NUM_DRIVES];

void disk_init(void) {}

void disk_uninit(void)
{
    for (int i = 0; i < DCDD_NUM_DRIVES; i++) {
        if (files[i] != NULL) {
            f_close(files[i]);
        }
    }
}

void disk_read_sector(uint8_t drive, uint8_t track, uint8_t sector, data_t data[DCDD_SECTOR_SIZE])
{
    if (drive > DCDD_NUM_DRIVES) {
        return; // ERROR
    }

    FIL *fp = files[drive];
    if (fp == NULL) {
        //printf("** ERROR: Trying to write to drive %d which is inactive\r\n", drive);
        return;
    }

    long offset = (track * DCDD_NUM_SECTORS + sector) * DCDD_SECTOR_SIZE;
    if (f_lseek(fp, offset) != FR_OK) {
        return; // ERROR
    }

    UINT size;
    if (f_read(fp, data, DCDD_SECTOR_SIZE, &size) != FR_OK) {
        return; // ERROR
    }
}

void disk_write_sector(uint8_t drive, uint8_t track, uint8_t sector, const data_t data[DCDD_SECTOR_SIZE])
{
    if (drive > DCDD_NUM_DRIVES) {
        return; // ERROR
    }

    FIL *fp = files[drive];
    if (fp == NULL) {
        //printf("** ERROR: Trying to write to drive %d which is inactive\r\n", drive);
        return;
    }

    long offset = (track * DCDD_NUM_SECTORS + sector) * DCDD_SECTOR_SIZE;
    if (f_lseek(fp, offset) != FR_OK) {
        return; // ERROR
    }

    UINT size;
    if (f_write(fp, data, DCDD_SECTOR_SIZE, &size) != FR_OK) {
        return; // ERROR
    }
}

bool disk_attach(uint8_t drive, const char *fname)
{
    static FIL _files[DCDD_NUM_DRIVES];

    if (drive > DCDD_NUM_DRIVES) {
        return false;
    }

    if (files[drive] != NULL) {
        f_close(files[drive]);
        files[drive] = NULL;
    }

    if (fname == NULL) {
        return true;
    }

    if (f_open(&_files[drive], fname, FA_READ | FA_WRITE) != FR_OK) {
        return false;
    }

    files[drive] = &_files[drive];
    return true;
}

