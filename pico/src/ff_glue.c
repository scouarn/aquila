/* Interface between FatFS and SD driver */

#include "sd.h"
#include "ff.h"
#include "diskio.h"

#define DBG_DISKIO(...) ;
//#define DBG_DISKIO printf


#define NUM_DRIVES 1
static sd_t drives[NUM_DRIVES] = {
    {
        .pin_mosi = 19,
        .pin_miso = 16,
        .pin_sck  = 18,
        .pin_csn  = 17,
        .spi = spi0
    }
};

void diskio_uninit(void)
{
    for (int i = 0; i < NUM_DRIVES; i++) {
        drives[i].init = false;
    }
}

DSTATUS disk_status(BYTE pdrv)
{
    DBG_DISKIO("STATUS(%d)\r\n", pdrv);
    if (pdrv > NUM_DRIVES) {
        return STA_NODISK;
    }

    if (!drives[pdrv].init) {
        return STA_NOINIT;
    }

    return 0;
}

DSTATUS disk_initialize(BYTE __unused pdrv)
{
    DBG_DISKIO("INIT(%d)\r\n", pdrv);
    if (pdrv > NUM_DRIVES) {
        return STA_NODISK;
    }

    if (drives[pdrv].init) {
        return 0;
    }

    if (!sd_init(&drives[pdrv])) {
        return STA_NOINIT;
    }

    return 0;
}


DRESULT disk_read(BYTE pdrv, BYTE *buff, LBA_t sector, UINT count)
{
    DBG_DISKIO("READ(%d, s=%lu, c=%u)\r\n", pdrv, sector, count);
    if (pdrv > NUM_DRIVES) {
        return RES_PARERR;
    }

    if (!sd_read(&drives[pdrv], buff, sector, count)) {
        return RES_ERROR;
    }

    return 0;
}

DRESULT disk_write(BYTE pdrv, const BYTE *buff, LBA_t sector, UINT count)
{
    DBG_DISKIO("WRITE(%d, s=%lu, c=%u)\r\n", pdrv, sector, count);
    if (pdrv > NUM_DRIVES) {
        return RES_PARERR;
    }

    if (!sd_write(&drives[pdrv], buff, sector, count)) {
        return RES_ERROR;
    }

    return 0;
}

DRESULT disk_ioctl(BYTE pdrv, BYTE cmd, void *buff)
{
    DBG_DISKIO("IOCTL(%d, %d)\r\n", pdrv, cmd);
    if (pdrv > NUM_DRIVES) {
        return RES_PARERR;
    }

    switch (cmd) {
        case CTRL_SYNC:
            return RES_OK;
        break;

        case GET_SECTOR_COUNT:
            *(LBA_t*)buff = drives[pdrv].num_sectors;
            return RES_OK;
        break;

        default:
            return RES_PARERR;
        break;
    }

    return 0;
}

