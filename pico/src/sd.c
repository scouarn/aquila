#include "sd.h"

#include "pico/stdlib.h"
#include "hardware/spi.h"

#define CMD(N) (0x40 | N)

#define R1_IDLE_STATE           0x01
#define R1_ERASE_RESET          0x02
#define R1_ILLEGAL_COMMAND      0x04
#define R1_COM_CRC_ERROR        0x08
#define R1_ERASE_SEQUENCE_ERROR 0x10
#define R1_ADDRESS_ERROR        0x20
#define R1_PARAMETER_ERROR      0x40

#define TOKEN_START_BLOCK       0xfe /* For read and write */
#define TOKEN_START_MULTIBLOCK  0xfc /* Only for multiblock writing */
#define TOKEN_STOP_TRAN         0xfd /* Only for multiblock writing */

#define CMD_TIMEOUT 16
#define FILL 0xff

#include <stdio.h>
#define DBG(...) ;
#define DBG_ERR printf

static inline void fill(sd_t *sd, int n) {
    uint8_t fill = FILL;
    for (int i = 0; i < n; i++) {
        spi_write_blocking(sd->spi, &fill, 1);
    }
}

static inline void enable(sd_t *sd)
{
    gpio_put(sd->pin_csn, 0);
    fill(sd, 1);
}

static inline void disable(sd_t *sd)
{
    gpio_put(sd->pin_csn, 1);
    fill(sd, 1);
}

static uint8_t send_cmd(sd_t *sd, uint8_t cmd, uint32_t arg, uint8_t crc)
{
    uint8_t buff[] = {
        cmd, arg >> 24, arg >> 16, arg >> 8, arg >> 0, crc
    };

    fill(sd, 1);
    spi_write_blocking(sd->spi, buff, sizeof(buff));

    if (cmd == CMD(12)) { /* Not shure why it is needed */
        fill(sd, 1);
    }

    uint8_t status;
    for (int i = 0; i < CMD_TIMEOUT; i++) { /* Wait for answer */
        spi_read_blocking(sd->spi, FILL, &status, 1);
        if ((status & 0x80) == 0) { /* Start of response has MSB=0 */
            break;
        }
    }

    return status;
}

static bool wait_token(sd_t *sd, uint8_t token)
{
    uint8_t data;
    DBG("WAIT(%02x) ", token);
    for (int iters = 0; iters < 100; iters++) {
        spi_read_blocking(sd->spi, FILL, &data, 1);
        DBG("%02x ", data);
        if (data == token) {
            DBG("ENDWAIT ");
            return true;
        }
    }
    return false;
}


bool sd_init(sd_t *sd)
{
    uint8_t status;

    DBG("\r\nInit SPI... ");
    gpio_put(sd->pin_csn, 1); /* Active low */
    gpio_init(sd->pin_csn);
    gpio_set_dir(sd->pin_csn, GPIO_OUT);
    gpio_put(sd->pin_csn, 1); /* Active low */
    spi_init(sd->spi, 100*1000);
    spi_set_format(sd->spi, 8, SPI_CPOL_0, SPI_CPHA_0, SPI_MSB_FIRST);
    gpio_set_function(sd->pin_mosi, GPIO_FUNC_SPI);
    gpio_set_function(sd->pin_miso, GPIO_FUNC_SPI);
    gpio_set_function(sd->pin_sck,  GPIO_FUNC_SPI);
    gpio_pull_up(sd->pin_miso);
    DBG("OK");

    DBG("\r\nSend fill bytes... ");
    disable(sd);
    fill(sd, 10);
    DBG("OK");
    enable(sd);

    // DBG("\r\nWait ready... ");
    // uint8_t resp;
    // absolute_time_t timeout_time = make_timeout_time_ms(2000);
    // do {
    //     spi_read_blocking(SD_SPI, FILL, &resp, 1);
    // } while (resp == 0x00 &&
    //          0 < absolute_time_diff_us(get_absolute_time(), timeout_time));
    // if (resp == 0x00) {
    //     DBG("RESP %d", resp);
    // }

    DBG("\r\nGo idle state... ");
    for (int i = 0; i < 20; i++) {
        status = send_cmd(sd, CMD(0), 0, 0x95); /* CMD0 */
        if (status & R1_IDLE_STATE) {
            break;
        }
        DBG("%02x ", status);
        busy_wait_us(100 * 1000);
    }
    if (status != R1_IDLE_STATE) {
        DBG_ERR("FAIL: Go idle state ");
        goto fail;
    }
    DBG("%02x OK", status);

    DBG("\r\nCheck version... ");
    status = send_cmd(sd, CMD(8), 0x1aa, 0x87); /* Everyone is using this check pattern.. Let's just reuse the known crc */
    DBG("%02x ", status);
    if (status & R1_ILLEGAL_COMMAND) {
        DBG("SDV1 OK");
        sd->version = SD_V1;
    }
    else {
        DBG("SDV2 "); /* Read rest of response */
        uint8_t r7[4]; /* CMD8 response has 4 more bytes */
        spi_read_blocking(sd->spi, FILL, r7, sizeof(r7));

        for (int i = 0; i < (int)sizeof(r7); i++) {
            DBG("%02x ", r7[i]);
        }

        if (r7[3] != 0xaa) {
            DBG_ERR("FAIL: Wrong echo-back pattern");
            goto fail;
        }

        if ((r7[2] & 0xf) != 0x1) {
            DBG_ERR("FAIL: Voltage not supported");
            goto fail;
        }

        sd->version = SD_V2SC;
        DBG("OK");
    }

    DBG("\r\nCheck Voltage... ");
    status = send_cmd(sd, CMD(58), 0, 0xff);
    DBG("%02x ", status);
    if (status & R1_ILLEGAL_COMMAND) {
        DBG_ERR("FAIL: Illegal command");
        goto fail;
    } else {
        uint8_t r3[4]; /* CMD58 response has 4 more bytes */
        spi_read_blocking(sd->spi, FILL, r3, sizeof(r3));
        for (int i = 0; i < (int)sizeof(r3); i++) {
            DBG("%02x ", r3[i]);
        }
        if (!(r3[1] & 0x10)) {
            DBG_ERR("FAIL: 3.2~3.3V Not supported");
        }
    }
    DBG("OK");

    DBG("\r\nActivate card... ");
    uint32_t arg = 0;
    int iters = 0;
    if (sd->version == 2) {
        arg |= 1 << 30; /* HCS bit */
    }
    do {
        if (iters++ > 100) { /* Seams to be a reasonable timeout (?) */
            DBG_ERR("FAIL: Activate card timeout ");
            goto fail;
        }
        send_cmd(sd, CMD(55), 0, 0xff);
        status = send_cmd(sd, CMD(41), arg, 0xff);
    } while (status & R1_IDLE_STATE);
    DBG("OK");

    if (sd->version == SD_V2SC) {
        DBG("\r\nCheck HC support... ");
        status = send_cmd(sd, CMD(58), 0, 0xff);
        DBG("%02x ", status);
        uint8_t r3[4]; /* CMD58 response has 4 more bytes */
        spi_read_blocking(sd->spi, FILL, r3, sizeof(r3));
        for (size_t i = 0; i < sizeof(r3); i++) {
            DBG("%02x ", r3[i]);
        }
        if (r3[0] & 0x40) {
            sd->version = SD_V2HC;
            DBG("HC=1 ");
        } else {
            DBG("HC=0 ");
        }
        DBG("OK");
    }

    DBG("\r\nQuery size... ");
    status = send_cmd(sd, CMD(9), 0, 0xff);
    DBG("%02x ", status);
    if (status != 0x00) {
        DBG("FAIL: CMD9 ");
        goto fail;
    }
    uint8_t csd[16];
    if (!wait_token(sd, TOKEN_START_BLOCK)) {
        DBG("FAIL: CSD Wait timeout ");
        goto fail;
    }
    spi_read_blocking(sd->spi, FILL, csd, sizeof(csd));
    for (size_t i = 0; i < sizeof(csd); i++) {
        DBG("%02x ", csd[i]);
    }
    int csd_structure = csd[0] >> 6; /* 2bits [127:126] */
    DBG("CSD_STRUCTURE=%d ", csd_structure);
    switch (csd_structure) {
        case 0: { /* As discribed in page 229 of the SD specs */
            uint32_t c_size = (uint32_t)(csd[6] & 0x03) << 10 | (uint32_t)csd[7] << 2 | csd[8] >> 6; /* 12bits [73:62] */
            uint32_t c_size_mult = (uint32_t)(csd[9] & 0x03) << 1 | csd[10] >> 7; /* 3bits [49:47] */
            uint32_t read_bl_len = csd[5] & 0x0f; /* 4bits [83:80] */
            uint32_t mult = 1 << (c_size_mult + 2);
            uint32_t blocknr = (c_size + 1) * mult;
            uint32_t block_len = 1 << read_bl_len;
            DBG("C_SIZE=%lu C_SIZE_MULT=%lu READ_BL_LEN=%lu ", c_size, c_size_mult, mult);
            sd->num_sectors = (uint64_t)blocknr * block_len / SD_SECTOR_SIZE;
        } break;

        case 1: {
            uint32_t c_size = (uint32_t)(csd[7] & 0x3f) << 16 | (uint32_t)csd[8] << 8 | csd[9]; /* 22bits [69:48] */
            DBG("C_SIZE=%lu ", c_size);
            sd->num_sectors = (c_size + 1) * 512 * 1024 / SD_SECTOR_SIZE;
        } break;

        default:
            DBG_ERR("FAIL: Unsupported CSD structure ");
            goto fail;
        break;
    }
    fill(sd, 2); /* Ignore CRC */
    DBG("OK");

    DBG("\r\nSet block size... ");
    status = send_cmd(sd, CMD(16), SD_SECTOR_SIZE, 0xff);
    DBG("%02x ", status);
    if (status != 0x00) {
        DBG_ERR("FAIL: CMD16 ");
        goto fail;
    }
    DBG("OK");

    disable(sd);
    sd->init = true;
    return true;

fail:
    disable(sd);
    return false;
}


bool sd_read(sd_t *sd, uint8_t *data, uint32_t start_sector, uint32_t num_sectors)
{
    if (num_sectors == 0) {
        return true; // ??
    }

    if (start_sector + num_sectors >= sd->num_sectors) {
        return false;
    }

    uint32_t address = start_sector;
    if (sd->version != SD_V2HC) {
        address *= SD_SECTOR_SIZE;
    }

    enable(sd);

    if (num_sectors == 1) {
        uint8_t status = send_cmd(sd, CMD(17), address, 0xff);
        DBG("%02x ", status);
        if (status != 0x00) {
            DBG_ERR("FAIL: CMD17 ");
            goto fail;
        }

        if (!wait_token(sd, TOKEN_START_BLOCK)) {
            DBG_ERR("FAIL: CMD17 timeout ");
            goto fail;
        }

        spi_read_blocking(sd->spi, FILL, data, SD_SECTOR_SIZE);
        fill(sd, 2); /* Ignore CRC */
    }
    else { /* Read multiple blocks */
        uint8_t status = send_cmd(sd, CMD(18), address, 0xff);
        DBG("%02x ", status);
        if (status != 0x00) {
            DBG_ERR("FAIL: CMD18 ");
            goto fail;
        }

        for (uint32_t i = 0; i < num_sectors; i++) {
            if (!wait_token(sd, TOKEN_START_BLOCK)) { /* This wasn't shown like that in the specs */
                DBG_ERR("FAIL: CMD18 timeout ");
                goto fail;
            }

            spi_read_blocking(sd->spi, FILL, data, SD_SECTOR_SIZE);
            fill(sd, 2); /* Ignore CRC */
            data += SD_SECTOR_SIZE;
        }

        status = send_cmd(sd, CMD(12), 0, 0xff);
        DBG("%02x ", status);
        if (status != 0x00) {
            DBG_ERR("FAIL: CMD12 ");
            goto fail;
        }

        // DBG("WAIT BUSY ");
        // for (int iters = 0; iters < 100; iters++) {
        //     spi_read_blocking(sd->spi, FILL, &status, 1);
        //     DBG("%02x ", status);
        //     if (status != 0x00) {
        //         DBG("ENDWAIT ");
        //         break;
        //     }
        // }
        // if (status == 0) {
        //     DBG("STILL BUSY ");
        //     goto fail;
        // }
    }

    disable(sd);
    return true;

fail:
    disable(sd);
    return false;
}

bool sd_write(sd_t *sd, const uint8_t *data, uint32_t start_sector, uint32_t num_sectors)
{
    if (num_sectors == 0) {
        return true; // ??
    }

    if (start_sector + num_sectors >= sd->num_sectors) {
        return false;
    }

    uint32_t address = start_sector;
    if (sd->version != SD_V2HC) {
        address *= SD_SECTOR_SIZE;
    }

    enable(sd);

    if (num_sectors == 1) {
        uint8_t status = send_cmd(sd, CMD(24), address, 0xff);
        DBG("%02x ", status);
        if (status != 0x00) {
            DBG_ERR("FAIL: CMD24 ");
            goto fail;
        }

        uint8_t token = TOKEN_START_BLOCK;
        spi_write_blocking(sd->spi, &token, 1);
        spi_write_blocking(sd->spi, data, SD_SECTOR_SIZE);
        fill(sd, 2); /* Ignore CRC */

        spi_read_blocking(sd->spi, FILL, &status, 1);
        if ((status & 0x1f) != 0x05) { /* Data accepted */
            DBG("FAIL: Writing block ");
            goto fail;
        }

        DBG("WAIT BUSY ");
        for (int iters = 0; iters < 1000; iters++) {
            spi_read_blocking(sd->spi, FILL, &status, 1);
            DBG("%02x ", status);
            if (status != 0x00) {
                DBG("ENDWAIT ");
                break;
            }
        }
        if (status == 0) {
            DBG_ERR("FAIL: Write still busy ");
            goto fail;
        }
    }
    else {
        uint8_t status = send_cmd(sd, CMD(25), address, 0xff);
        DBG("%02x ", status);
        if (status != 0x00) {
            DBG_ERR("FAIL: CMD25 ");
            goto fail;
        }

        for (uint32_t i = 0; i < num_sectors; i++) {
            uint8_t token = TOKEN_START_MULTIBLOCK;
            spi_write_blocking(sd->spi, &token, 1);
            spi_write_blocking(sd->spi, data, SD_SECTOR_SIZE);
            fill(sd, 2); /* Ignore CRC */

            spi_read_blocking(sd->spi, FILL, &status, 1);
            if ((status & 0x1f) != 0x05) { /* Data accepted */
                DBG("FAIL: Writing block ");
                goto fail;
            }

            DBG("WAIT BUSY ");
            for (int iters = 0; iters < 1000; iters++) {
                spi_read_blocking(sd->spi, FILL, &status, 1);
                DBG("%02x ", status);
                if (status != 0x00) {
                    DBG("ENDWAIT ");
                    break;
                }
            }
            if (status == 0) {
                DBG_ERR("FAIL: Write still busy ");
                goto fail;
            }
        }

        uint8_t token = TOKEN_STOP_TRAN;
        spi_write_blocking(sd->spi, &token, 1);

        DBG("WAIT BUSY ");
        for (int iters = 0; iters < 1000; iters++) {
            spi_read_blocking(sd->spi, FILL, &status, 1);
            DBG("%02x ", status);
            if (status != 0x00) {
                DBG("ENDWAIT ");
                break;
            }
        }
        if (status == 0) {
            DBG_ERR("FAIL: Write still busy ");
            goto fail;
        }
    }

    disable(sd);
    return true;

fail:
    disable(sd);
    return false;
}

