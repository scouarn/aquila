#ifndef PANEL_H
#define PANEL_H

/* Front panel IO */

#include "i8080.h"


/* Bits of the control word (from MSB to LSB)
   This is also the register priority order (most to least) */
enum panel_ctrl {
    CTRL_STOP,
    CTRL_RUN,
    CTRL_SLOW,
    CTRL_STEP,
    CTRL_EX,
    CTRL_EXNEXT,
    CTRL_DEPNEXT,
    CTRL_DEP,
    CTRL_RESET,
    CTRL_CLEAR,
    CTRL_UNPROT,
    CTRL_PROT,
    CTRL_AUX1_UP,
    CTRL_AUX1_DN,
    CTRL_AUX2_DN,
    CTRL_AUX2_UP,

    CTRL_NONE,
};

extern const char * const panel_ctrlstr[];

typedef struct {
    uint32_t data_in; /* Internal */

    addr_t   led_addr;
    data_t   led_data;
    data_t   led_stat;
    bool     led_hlda;
    bool     led_inte;
    bool     led_prot;
    bool     led_wait;

    addr_t   sw_addr;
    data_t   sw_sense;
    data_t   sw_data;
    bool     sw_power;
    uint16_t sw_ctrl;
    enum panel_ctrl event;
} panel_t;

void panel_init(void);
void panel_io(panel_t *panel);
void panel_update(panel_t *panel);

#endif
