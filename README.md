# Aquila

## Usage

Unit testing (not everything covered):
```console
make test
_build/test
```

Main emulator:
```console
make emul
_build/emul
```

To open the menu, press `Ctrl+B` and you will be prompted for a command.


## Arduino first prototype

Compiling the prototype, the ROM is loaded at address `0x0000` :
```
cp <my_boot_rom> proto1/bootrom.bin
make proto1
```

Upload to the Arduino:
```
cd proto1
make upload
```

Open serial communication with the Arduino:
```
proto1/tty.sh
```


## RPi Pico second prototype



## 8K BASIC Sense switche settings

From BASIC manual (1977).

| Device                | Sense Switch | Terminal        | Load     | Channels       |
|                       | Setting      | Switches        | Switches |                |
|-----------------------|--------------|-----------------|----------|----------------|
| 2SI0 (2 stop bits)    | 0            | none     (0x00) | none     | 20, 21         |
| 2SIO 1 (1 stop bit)   | 1            | A12      (0x10) | A8       | 20, 21         |
| SIO                   | 2            | A13      (0x20) | A9       | 0, 1           |
| ACR                   | 3            | A13, A12 (0x30) | A9, A8   | 6, 7           |
| 4PI0                  | 4            | A14      (0x40) | A10      | 40, 41, 42, 43 |
| PIO                   | 5            | A14, A12 (0x50) | A10, A8  | 4, 5           |
| HSR                   | 6            | A14, A13 (0x60) | A10, A9  | 46, 47         |
| non-standard terminal | 14           |                 |          |                |
| no terminal           | 15           |                 |          |                |


## Links

### Altair 8800
- Theory of operation : http://vtda.org/docs/computing/MITS/MITS_Altair8800TheoryOperation_1975.pdf
- Some doc :            http://dunfield.classiccmp.org/altair/altair6.htm
- More manuals :        https://altairclone.com/downloads/manuals/

### Intel 8080
- Instruction set table:    https://www.pastraiser.com/cpu/i8080/i8080_opcodes.html
- i8080 datasheet:          http://kazojc.com/elementy_czynne/IC/INTEL-8080A.pdf
- Programmer's manual:      https://altairclone.com/downloads/manuals/8080%20Programmers%20Manual.pdf

### Arduino Mega 2560
- ATmega datasheet:         https://ww1.microchip.com/downloads/en/devicedoc/atmel-2549-8-bit-avr-microcontroller-atmega640-1280-1281-2560-2561_datasheet.pdf
- Arduino mega schematics:  https://www.arduino.cc/en/uploads/Main/arduino-mega2560-schematic.pdf

### Minitel Serial Communication
- https://pojntfx.github.io/minitel/main.html
- Furrtek:  http://furrtek.free.fr/index.php?a=telinux
- x0r:      https://x0r.fr/blog/5
- https://blog.nicelab.eu/minitels-en-serie/
- https://web.archive.org/web/20230220212557/https://pila.fr/wordpress/?p=361
- http://sta6502.blogspot.com/2016/02/utiliser-un-minitel-comme-terminal-sur.html
- https://forums.raspberrypi.com//viewtopic.php?t=44932
- https://arduiblog.com/2019/04/29/ressuscitez-le-minitel/
- https://electronics.stackexchange.com/questions/186168/how-to-convert-uart-voltage-from-5v-to-3-3v
- https://www.instructables.com/DIY-5v-to-33v-Logic-Level-Shifter/

### Other
- Great resource on raw mode: https://viewsourcecode.org/snaptoken/kilo/02.enteringRawMode.html

