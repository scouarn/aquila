#!/usr/bin/sh
CHAR_DELAY=30
LINE_DELAY=200
PORT=/dev/ttyACM0

picocom $PORT \
    --send-cmd "ascii-xfr -s -c $CHAR_DELAY -l $LINE_DELAY" \
    --receive-cmd "ascii-xfr -r"
