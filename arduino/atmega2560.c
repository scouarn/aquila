/* Prototype board:

Layout:

    [RUN/STOP]  [STEP]             (HOLD) (READY)  (DATA LEDs 0:8)
                                   (       ADDR LEDs 0:11        )
    [EXAM]      [DEP]   [SHIFT]    [bits 8:11][bits 4:7][bits 0:3]


Input buttons:
    Arduino pin:    21       20      19        18      2          [3   ]
    Interrupt:      INT0     INT1    [INT2]    INT3    INT4       [INT5]
    Port number:    D0       D1      D2        D3      E4         [E5  ]
    Emulator fn:    EXAM     DEP     SHIFT     STEP    RUN/STOP   Unused
    Fn with shift:  EXAMN    DEPN                      RESET

    Note: The shift button is read directly (no interrupt).
    Note: E5/INT5 is not used for now.


Address / data input switches:
    port A (pins 22:29) : low byte (data + sense)
    port C (pins 37:30) : high byte

    Note: There are no switches for the high 4bits of the high byte (always 0)
        The prototype doesn't address the full 64k


LEDs outputs (multiplexed) :
    port B: 4 bit binary value
    port L: 6 bit select, one bit for group of 4 LEDs (see enum led_nibbles)

*/


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#undef SP /* Name collision with cpu.SP */
#include "i8080.h"


#define SIO_STATUS_PORT    0x00
#define SIO_DATA_PORT      0x01
#define SIO_INPUT_MASK     0x01
#define SIO_OUTPUT_MASK    0x80
#define USB_STRIP_MASK     0x7f // Strip 8th bit, can't do it from Picocom...

#define SENSE_PORT    255
#define RAM_SIZE     4096

static data_t ram[RAM_SIZE] = {
#include "bootrom.hex"
};

static data_t io_data_bus = 0x00;
static addr_t io_addr_bus = 0x0000;

static cpu_t cpu = {0};



/* Read from inputs ports */
#define INPUT_DATA  ((data_t)~PINA)
#define INPUT_ADDR  ((addr_t)~((PINC << 8) | PINA))
#define INPUT_SENSE ((data_t)~PINA) /* On the real Altair the sense switches are the upper 8bits */
#define INPUT_SHIFT (!(PIND & _BV(PIND2)))

enum event {
    EVENT_NONE,
    EVENT_EXAM,
    EVENT_DEP,
    EVENT_STEP,
    EVENT_RUN,
};

static volatile enum event last_event = EVENT_NONE;



/* Serial rates */
#define BAUD_USB 9600U
#define BAUD_MINITEL 4800U
#define BAUD_PRESCALE(BAUD_RATE) ((F_CPU / (16UL*BAUD_RATE))-1)


/* Global timer for debounce and updating the LEDs */
/* Delay of 2ms */
#define TIMER_DELAY (0xFFFFUL - F_CPU/(64UL * 500UL))
static uint16_t timer_ticks = 0;


/* Multiplexing: Bits of port L used to address groups of 4 LEDs */
enum led_nibbles {
    DATA_NIB_1,
    DATA_NIB_0,
    ADDR_NIB_0,
    STAT_NIB_0,
    ADDR_NIB_1,
    ADDR_NIB_2,
    ADDR_NIB_3,
    NIB_COUNT
};


/* Init external interrupts, switch inputs, timer and LED pins */
static void device_init(void) {

    /*
        Push buttons
    */

    /* Enable input with pullup on pins */
    DDRD  &= ~(_BV(PORTD0) | _BV(PORTD1) | _BV(PORTD2) | _BV(PORTD3));
    DDRE  &= ~(_BV(PORTE4) | _BV(PORTE5));
    PORTD |=   _BV(PORTD0) | _BV(PORTD1) | _BV(PORTD2) | _BV(PORTD3);
    PORTE |=   _BV(PORTE4) /* | _BV(PORTE5) */;

    /* Trigger on rising edge (button release) */
    EICRA |= _BV(ISC01) | _BV(ISC01);
    EICRA |= _BV(ISC11) | _BV(ISC11);
    /* EICRA |= _BV(ISC21) | _BV(ISC21); */ // INT2 unused
    EICRA |= _BV(ISC31) | _BV(ISC31);
    EICRB |= _BV(ISC41) | _BV(ISC41);
    /* EICRB |= _BV(ISC51) | _BV(ISC51); */ // INT5 unused

    /* Enable INT0-5 */
    EIMSK |= _BV(INT0) | _BV(INT1) /* | _BV(INT2) */ // unused
          |  _BV(INT3) | _BV(INT4) /* | _BV(INT5) */; // unused


    /*
        Timer for LED multiplexing and software debounce
    */

    /* 16bit timer, divide by 64 */
    TCCR1A = 0x00;
    TCNT1  = 0x00;
    TCCR1B |= _BV(CS11) | _BV(CS10);
    TIMSK1 |= _BV(TOIE1);


    /*
        Addr/data input and display
    */

    /* Address/data switches: Enable input with pullup on port A port C */
    DDRA = 0x00; PORTA = 0xff;
    DDRC = 0x00; PORTC = 0xff;

    /* LED multiplexer 4 "value" pins
        and 6 "select" pins as output */
    DDRB = 0xff; PORTB = 0x00;
    DDRL = 0xff; PORTL = 0xff;


    /*
        Init serial for USB (port 0)
    */

    /* Enable Rx/Tx */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);

    /* 7bit char, 1 stop bit, even parity */
    UCSR0C = _BV(UCSZ01) | _BV(UPM01);

    /* Baud rate */
    UBRR0L = BAUD_PRESCALE(BAUD_USB) & 0xff;
    UBRR0H = BAUD_PRESCALE(BAUD_USB) >> 8;


    /*
        Init serial for Minitel
        TODO: send init codes to the minitel at 1200 baud then switch to 4800
    */

    /* Enable Rx/Tx */
    UCSR2B = _BV(RXEN2) | _BV(TXEN2);

    /* 7bit char, 1 stop bit, even parity */
    UCSR2C = _BV(UCSZ21) | _BV(UPM21);

    /* Baud rate */
    UBRR2L = BAUD_PRESCALE(BAUD_MINITEL) & 0xff;
    UBRR2H = BAUD_PRESCALE(BAUD_MINITEL) >> 8;

}



/* Timer interrupt: update ticks counter and LED multiplexer */
ISR(TIMER1_OVF_vect) {
    TCNT1 = TIMER_DELAY;
    timer_ticks++;

    static int curr_nib = 0;
    static uint8_t count = 0;
    if (timer_ticks % 100 == 0) count ++;

    uint8_t val;

    /* Read the state of the CPU and buses */
    switch (curr_nib) {
        case STAT_NIB_0: val = (cpu.hold << 3) | (!cpu.ready << 2); break; // TODO: Replace ready with something useful
        case DATA_NIB_0: val = io_data_bus >>  0; break;
        case DATA_NIB_1: val = io_data_bus >>  4; break;
        case ADDR_NIB_0: val = io_addr_bus >>  0; break;
        case ADDR_NIB_1: val = io_addr_bus >>  4; break;
        case ADDR_NIB_2: val = io_addr_bus >>  8; break;
        case ADDR_NIB_3: val = io_addr_bus >> 12; break;
        default: val = 0x00; break;
    }

    /* Output value on PORTB and select bit on PORTL */
    PORTB = (~val) & 0x0f;
    PORTL = 1U << curr_nib;

    if (++curr_nib > NIB_COUNT) curr_nib = 0;
}


/* Abort ISR if a tick hasn't passed since last press or if a button press
    is still beeing processed */
#define DEBOUNCE \
    static uint16_t debounce = 0; \
    if (debounce == timer_ticks) return; \
    debounce = timer_ticks; \
    if (last_event != EVENT_NONE) return;

/* Examine (next) button */
ISR (INT0_vect) {
    DEBOUNCE;
    if (!cpu.hold) return;
    last_event = EVENT_EXAM;
}

/* Deposit (next) button */
ISR (INT1_vect) {
    DEBOUNCE;
    if (!cpu.hold) return;
    last_event = EVENT_DEP;
}

/* Shift button */
// ISR (INT2_vect) {}

/* Step button */
ISR (INT3_vect) {
    DEBOUNCE;
    if (!cpu.hold) return;
    last_event = EVENT_STEP;
}

/* Run/stop/reset button */
ISR (INT4_vect) {
    DEBOUNCE;
    last_event = EVENT_RUN;
}

/* Unused */
// ISR (INT5_vect) {}


/*
    CPU IO functions
*/

static data_t load(addr_t addr) {
    if (addr >= RAM_SIZE) return 0x00;

    io_addr_bus = addr;
    io_data_bus = ram[addr];
    return io_data_bus;
}

static void store(addr_t addr, data_t data) {
    if (addr >= RAM_SIZE) return;

    io_addr_bus  = addr;
    io_data_bus  = data;
    ram[addr] = data;
}


/* NOTE:
    We receive from ONE of the two serial ports (USB/Minitel),
    But we send to BOTH serial ports.
*/
static data_t input(port_t port) {
    io_addr_bus = (port << 8) | port; // According to datasheet

    switch (port) {

        /* Translate the USART status byte into a 88-2 SIO status byte */
        case SIO_STATUS_PORT:
            io_data_bus = 0x00;

            /* Rx busy: If both of them are busy then we can't receive */
            if ((UCSR0A & _BV(RXC0)) == 0
             && (UCSR2A & _BV(RXC2)) == 0) {
                io_data_bus |= SIO_INPUT_MASK;
            }

            /* Tx busy: If one of them is busy then we can't send */
            if ((UCSR0A & _BV(UDRE0)) == 0
             || (UCSR2A & _BV(UDRE2)) == 0) {
                io_data_bus |= SIO_OUTPUT_MASK;
            }

            /* TODO: Error bits */

        break;

        case SIO_DATA_PORT:

            /* Receive from USB if there is data */
            if (UCSR0A & _BV(RXC0)) {
                io_data_bus = UDR0;
            }

            /* Receive from Minitel */
            else if (UCSR2A & _BV(RXC2)) {
                io_data_bus = UDR2;
            }

            else {
                io_data_bus = 0x00;
            }

        break;

        case SENSE_PORT:
            io_data_bus = INPUT_SENSE;
        break;

        default: break;
    }

    return io_data_bus;
}


static void output(port_t port, data_t data) {
    io_addr_bus = (port << 8) | port; // According to datasheet
    io_data_bus = data;

    switch (port) {

        /* Control register of the SIO card */
        case SIO_STATUS_PORT:
            /* TODO: control baud rate, stop bits, ... */
        break;

        case SIO_DATA_PORT:
            /* Write to USB if ready */
            if (UCSR0A & _BV(UDRE0)) {
                UDR0 = data & USB_STRIP_MASK;
            }

            /* Write to Minitel if ready */
            if (UCSR2A & _BV(UDRE2)) {
                UDR2 = data;
            }
        break;

        case SENSE_PORT:
            /* TODO: Nothing to output ? */
        break;

        default: break;
    }

}


int main(void) {

    /* Init microcontroller */
    device_init();

    /* Init emulator */
    cpu.cb_load   = load;
    cpu.cb_store  = store;
    cpu.cb_input  = input;
    cpu.cb_output = output;

    cpu.reset = true;
    cpu_step(&cpu);
    cpu.hold = true; // Press the reset button to start

    sei();

    while(1) {

        cli();
        switch (last_event) {
            case EVENT_EXAM:
                /* Next */
                if (INPUT_SHIFT) {
                    io_addr_bus++;
                }
                else {
                    io_addr_bus = INPUT_ADDR;
                }

                /* Update state of the buses */
                cpu.PC = io_addr_bus;
                io_data_bus = ram[io_addr_bus];
            break;

            case EVENT_DEP:
                /* Next */
                if (INPUT_SHIFT) {
                    io_addr_bus++;
                }

                /* Update state of the buses */
                io_data_bus = INPUT_DATA;

                /* Update RAM content */
                ram[io_addr_bus] = io_data_bus;
            break;

            case EVENT_STEP:
                cpu.hold = false;
                cpu_step(&cpu);
                cpu.hold = true;
                io_addr_bus = cpu.PC;
            break;

            case EVENT_RUN:
                /* Reset */
                if (INPUT_SHIFT) {
                    cpu.reset = true;
                    cpu.hold = true; // TODO: does reset on the real 8080 bypasses hold ? On real Altair reset does not set hold but wait...
                    io_data_bus = 0x00;
                    io_addr_bus = 0x0000;
                    cpu_step(&cpu);
                    cpu.reset = true;
                }

                /* Run / Stop */
                else {
                    cpu.hold = !cpu.hold;
                }
            break;

            default:
            case EVENT_NONE:
            break;
        }
        last_event = EVENT_NONE;
        sei();

        cpu_step(&cpu);
    }

}
