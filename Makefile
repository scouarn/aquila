# Make the Altair emulator and the test for the CPU

BUILD_DIR := $(PWD)/_build
SRC_DIR := $(PWD)/src

CC = gcc
CFLAGS = -Wall -Wextra -pedantic -std=c11 -I$(SRC_DIR)
CFLAGS += -O2
CFLAGS += -ggdb

# For native applications, no cross compiling for other platforms
export BUILD_DIR SRC_DIR CC CFLAGS

TARGETS = test emul arduino pico/build
.PHONY: all clean $(TARGETS)
all: $(TARGETS) test

$(TARGETS): % :
	@$(MAKE) -C $@

clean:
	rm -rf $(BUILD_DIR)
