#ifndef DEVICES_H
#define DEVICES_H

/* Emulation for IO cards and devices
   Physical devices are platform-specific and have to be user-defined
*/

#include "i8080.h"
#define SENSE_PORT 0xff

/* REV1 88-SIO Card
    STATUS:
    7: Output not ready
    6: Unused
    5: Data available in buffer
    4: Overflow
    3: Framing error (no valid stop bit)
    2: Parity error
    1: X-mitter buffer empty ?
    0: Input not ready

    TODO: Control, interupts, clock rate
*/

#define SIO_DEFAULT_PORT 000
#define SIO_INPUT_BUSY_MASK  0x01
#define SIO_OUTPUT_BUSY_MASK 0x80

typedef struct {
    port_t base_port; /* Status port = base, data port = base + 1 */
} sio_t;

data_t sio_update_read(sio_t *sio, port_t port);
void   sio_update_write(sio_t *sio, port_t port, data_t data);

/* 88-2SIO Card
    STATUS:
    7: IRQ
    6: Parity error
    5: Overrun error
    4: Frame error
    3: Not clear to send (modem ?)
    2: Carrier not present (modem ?)
    1: Transmit Data Register Empty (output ready)
    0: Receive Data Register Full   (input ready)

    TODO: Control, interupts, clock rate
*/

#define SIO2_DEFAULT_PORT 020
#define SIO2_INPUT_READY_MASK  0x01
#define SIO2_OUTPUT_READY_MASK 0x02

typedef struct {
    port_t base_port; /* status1=base, data1=base+1, status2=base+2, data2=base+3 */
} sio2_t;

data_t sio2_update_read(sio2_t *sio, port_t port);
void   sio2_update_write(sio2_t *sio, port_t port, data_t data);


/* TTY */
// TODO: tty struct?

void tty_init(void);
void tty_uninit(void);

bool   tty_input_ready(port_t port);
bool   tty_output_ready(port_t port);
data_t tty_input(port_t port);
void   tty_output(port_t port, data_t data);

void tty_update(void);
data_t tty_peek(void);

/* Pass NULL to stop tape reader/punch */
bool tty_tape_in(const char *fname);
bool tty_tape_out(const char *fname);


/* 88-DCDD Disk card

    SELECTION: (010 OUT) True is 0 and false is 1
    0-3: Drive number
    4-6: Unused
    7:   0 -> Enable drive

    STATUS: (010 IN) True is 0 and false is 1
    0: 0 -> Ready to write data
    1: 0 -> Head movement is allowed
    2: 0 -> Head is loaded, ready for read/write, enable sector position status
    3-4: Unused, always 0
    5: 0 -> Interrupt enabled (not implemented)
    6: 0 -> Head is on track 0 (outermost)
    7: 0 -> Ready to read data

    CONTROL: (011 OUT) True is 1 and false is 0
    0: 1 -> Step in
    1: 1 -> Step out
    2: 1 -> Load head on disk, enable sector position status
    3: 1 -> Unload head from disk
    4: 1 -> Interupt enable (not implemented)
    5: 1 -> Interupt disable (not implemented)
    6: 1 -> Head current switch (not implemented)
    7: 1 -> Write enable

    SECTOR POSITION: (011 IN)
    ...

    DATA: (012 IN/OUT)
    ...

*/

#define DCDD_STATUS_PORT  010
#define DCDD_CONTROL_PORT 011
#define DCDD_DATA_PORT    012

#define DCDD_NUM_DRIVES  16
#define DCDD_NUM_TRACKS  77
#define DCDD_NUM_SECTORS 32

#define DCDD_SECTOR_SIZE 137
#define DCDD_TRACK_SIZE  (DCDD_NUM_SECTORS * DCDD_SECTOR_SIZE)
#define DCDD_DISK_SIZE   (DCDD_NUM_TRACKS * DCDD_TRACK_SIZE)

#define DCDD_SELECT_MASK        0x0f
#define DCDD_ENABLE_MASK        0x80

#define DCDD_WRITE_READY_MASK   0x01
#define DCDD_HEAD_MOVE_MASK     0x02
#define DCDD_STATUS_UNUSED_MASK 0x18
#define DCDD_LOADED_MASK        0x04
#define DCDD_TRACK0_MASK        0x40
#define DCDD_READ_READY_MASK    0x80

#define DCDD_STEP_IN_MASK       0x01
#define DCDD_STEP_OUT_MASK      0x02
#define DCDD_LOAD_MASK          0x04
#define DCDD_UNLOAD_MASK        0x08
#define DCDD_WRITE_MASK         0x80


#define DCDD_STEP_IN

typedef struct {
    uint8_t curr_drive;
    uint16_t present_drives; /* Bit mask: bit n set means n is a valid drive number */
    bool buff_inval;  /* buff contains modified data that should be written before switching sector */
    bool buff_loaded; /* buff contains data from current sector ready to read */

    struct {
        uint8_t status, curr_byte, curr_sector;
        int8_t curr_track;
    } drives[DCDD_NUM_DRIVES];

    data_t buff[DCDD_SECTOR_SIZE];
} dcdd_t;

data_t dcdd_update_read(dcdd_t *dcdd, port_t port);
void   dcdd_update_write(dcdd_t *dcdd, port_t port, data_t data);


/* Set of physical disk drives */
// TODO: disk drive struct?

void disk_init(void);
void disk_uninit(void);

void disk_write_sector(uint8_t driveno, uint8_t track, uint8_t sector, const data_t data[DCDD_SECTOR_SIZE]);
void disk_read_sector(uint8_t driveno, uint8_t track, uint8_t sector, data_t data[DCDD_SECTOR_SIZE]);
bool disk_attach(uint8_t driveno, const char *path); /* Use path=NULL to eject */

#endif
