
#include "i8080.h"

/* Links:
    https://www.pastraiser.com/cpu/i8080/i8080_opcodes.html
    http://dunfield.classiccmp.org/r/8080.txt
    https://www.autometer.de/unix4fun/z80pack/ftp/manuals/Intel/8080_8085_asm_Nov78.pdf
    https://retroprogramming.it/2021/02/8080-z80-instruction-set/
*/


static const uint8_t cycle_table[256] = {
    4, 10,  7,  5,  5,  5,  7,  4,  4, 10,  7,  5,  5,  5,  7,  4,
    4, 10,  7,  5,  5,  5,  7,  4,  4, 10,  7,  5,  5,  5,  7,  4,
    4, 10, 16,  5,  5,  5,  7,  4,  4, 10, 16,  5,  5,  5,  7,  4,
    4, 10, 13,  5, 10, 10, 10,  4,  4, 10, 13,  5,  5,  5,  7,  4,

    5,  5,  5,  5,  5,  5,  7,  5,  5,  5,  5,  5,  5,  5,  7,  5,
    5,  5,  5,  5,  5,  5,  7,  5,  5,  5,  5,  5,  5,  5,  7,  5,
    5,  5,  5,  5,  5,  5,  7,  5,  5,  5,  5,  5,  5,  5,  7,  5,
    7,  7,  7,  7,  7,  7,  7,  7,  5,  5,  5,  5,  5,  5,  7,  5,

    4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
    4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
    4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
    4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,

    5, 10, 10, 10, 11, 11,  7, 11,  5, 10, 10, 10, 11, 17,  7, 11,
    5, 10, 10, 10, 11, 11,  7, 11,  5, 10, 10, 10, 11, 17,  7, 11,
    5, 10, 10, 18, 11, 11,  7, 11,  5,  5, 10,  5, 11, 17,  7, 11,
    5, 10, 10,  4, 11, 11,  7, 11,  5,  5, 10,  4, 11, 17,  7, 11,
};


static data_t fetch(cpu_t *cpu) {

    /* Fetch from interrupt input */
    if (cpu->irq_data != NULL) {
        data_t data = *cpu->irq_data;
        cpu->irq_data++;
        return data;
    }

    /* Fetch from memory */
    data_t data = cpu_user_load(cpu, cpu->PC);
    cpu->PC++;
    return data;
}

static addr_t fetch_addr(cpu_t *cpu) {
    data_t lo = fetch(cpu);
    data_t hi = fetch(cpu);
    return (hi << 8) | lo;
}

static void push(cpu_t *cpu, addr_t word) {
    cpu_user_store(cpu, cpu->SP-1, word >> 8);
    cpu_user_store(cpu, cpu->SP-2, word & 0xff);
    cpu->SP -= 2;
}

static addr_t pop(cpu_t *cpu) {
    data_t hi = cpu_user_load(cpu, cpu->SP+1);
    data_t lo = cpu_user_load(cpu, cpu->SP);
    cpu->SP += 2;
    return (hi << 8) | lo;
}

static inline void set_flag(cpu_t *cpu, unsigned int f, bool b) {
    if (b) {
        cpu->F |=  f; // Set the bit to 1
    }
    else {
        cpu->F &= ~f; // Set the bit to 0
    }
}

/* Update the value of zero, sign and parity flag
    based on the value reg (usually the accumulator) */
static void update_ZSP(cpu_t *cpu, data_t reg) {
    set_flag(cpu, FLAG_Z, reg == 0x00); // Zero
    set_flag(cpu, FLAG_S, reg  & 0x80); // Sign

    /* Parity */
    int p = 0;
    for (int mask = (1<<7); mask > 0; mask >>= 1) {
        if (reg & mask) p++;
    }

    set_flag(cpu, FLAG_P, p % 2 == 0);
}

/* ADD ADI ADC ACI */
static void adc(cpu_t *cpu, data_t data, uint8_t cmask) {
    uint16_t res = cpu->A + data + (cpu->F & cmask);
    uint16_t cr = res ^ data ^ cpu->A;
    cpu->A = res;
    update_ZSP(cpu, res);
    set_flag(cpu, FLAG_A, cr & 0x010);
    set_flag(cpu, FLAG_C, cr & 0x100);
}

/* SUB SBI SBB SBI */
static void sbb(cpu_t *cpu, data_t data, uint8_t cmask) {
    uint16_t res = cpu->A + ~data + !(cpu->F & cmask);
    uint16_t cr = res ^ ~data ^ cpu->A;
    cpu->A = res;
    update_ZSP(cpu, res);
    set_flag(cpu, FLAG_A,  cr & 0x010);
    set_flag(cpu, FLAG_C, ~cr & 0x100);
}

/* CMP CPI */
static void cmp(cpu_t *cpu, data_t data) {
    uint16_t res = cpu->A + ~data + 1;
    uint16_t cr = res ^ ~data ^ cpu->A;
    update_ZSP(cpu, res);
    set_flag(cpu, FLAG_A,  cr & 0x010);
    set_flag(cpu, FLAG_C, ~cr & 0x100);
}

/* ANA ANI */
static void ana(cpu_t *cpu, data_t data) {
    data_t res = cpu->A & data;
    update_ZSP(cpu, res);
    set_flag(cpu, FLAG_A, (cpu->A | data) & 0x08);\
    set_flag(cpu, FLAG_C, 0);
    cpu->A = res;
}

/* ORA ORI */
static void ora(cpu_t *cpu, data_t data) {
    cpu->A |= data;
    update_ZSP(cpu, cpu->A);
    set_flag(cpu, FLAG_A, 0);
    set_flag(cpu, FLAG_C, 0);
}

/* XRA XRI */
static void xra(cpu_t *cpu, data_t data) {
    cpu->A ^= data;
    update_ZSP(cpu, cpu->A);
    set_flag(cpu, FLAG_A, 0);
    set_flag(cpu, FLAG_C, 0);
}

/* result of INR and DCR */
static data_t inc8(cpu_t *cpu, data_t data, int inc) {
    data += inc;
    update_ZSP(cpu, data);

    if (inc > 0) {
        set_flag(cpu, FLAG_A, (data & 0x0f) == 0x00);
    }
    else {
        set_flag(cpu, FLAG_A, (data & 0x0f) != 0x0f);
    }

    return data;
}

/* DAD */
static void dad(cpu_t *cpu, addr_t data) {
    uint32_t res = (uint32_t)data + cpu->HL;
    set_flag(cpu, FLAG_C, res > 0xffff);
    cpu->HL = res;
}

/* JMP JNZ JNC JPO JP JZ JC JPE JM */
static void jmp(cpu_t *cpu, bool cond) {
    addr_t addr = fetch_addr(cpu);
    if (!cond) return;
    cpu->PC = addr;
}

/* RET RNZ RNC RPO RP RZ RC RPE RM */
static void ret(cpu_t *cpu, bool cond) {
    if (!cond) return;
    cpu->PC = pop(cpu);
    cpu->cycles += 6;
}

/* CALL CNZ CNC CPO CP CZ CC CPE CM */
static void call(cpu_t *cpu, bool cond) {
    addr_t addr = fetch_addr(cpu);
    if (!cond) return;
    push(cpu, cpu->PC);
    cpu->PC = addr;
    cpu->cycles += 6;
}


int cpu_step(cpu_t *cpu) {

#define COND_Z   (cpu->F & FLAG_Z)
#define COND_C   (cpu->F & FLAG_C)
#define COND_PE  (cpu->F & FLAG_P)
#define COND_M   (cpu->F & FLAG_S)
#define COND_NZ  (!COND_Z)
#define COND_NC  (!COND_C)
#define COND_PO  (!COND_PE)
#define COND_P   (!COND_M)

    if (cpu->wait) {
        return 1; /* ??? */
    }

    /* Handle reset request */
    else if (cpu->reset) {
        cpu->F = ENFORCE_FLAGS(cpu->F);
        cpu->PC = 0x0000;

        cpu->inte = false;
        cpu->hold = false;
        cpu->reset = false;
        cpu->irq_ack = false;

        cpu->cycles = 0;
        return 1; /* ??? */
    }

    /* Handle interrupt request */
    else if (cpu->irq_data != NULL) {

        if (cpu->inte) {
            cpu->inte = false;
            cpu->irq_ack = true;
        }
        else {
            cpu->irq_ack = false;
            cpu->irq_data = NULL; /* Reset so it's not executed */
        }

        /* CONTINUE EXECUTION */
    }

    /* Do nothing if halted TODO: check what hold means */
    else if (cpu->hold) {
        //cpu->cycles++;
        return 1; /* ??? */
    }


    /* Fetch */
    uint8_t opc = fetch(cpu);

    /* Decode-Excecute */
    switch (opc) {
        default:
        case 0x76: cpu->hold = true; break; // HLT

        case 0x00:
        case 0x10:
        case 0x20:
        case 0x30:
        case 0x08:
        case 0x18:
        case 0x28:
        case 0x38: break; // NOP

        case 0x40: cpu->B = cpu->B; break; // MOV B, B
        case 0x41: cpu->B = cpu->C; break; // MOV B, C
        case 0x42: cpu->B = cpu->D; break; // MOV B, D
        case 0x43: cpu->B = cpu->E; break; // MOV B, E
        case 0x44: cpu->B = cpu->H; break; // MOV B, H
        case 0x45: cpu->B = cpu->L; break; // MOV B, L
        case 0x47: cpu->B = cpu->A; break; // MOV B, A
        case 0x48: cpu->C = cpu->B; break; // MOV C, B
        case 0x49: cpu->C = cpu->C; break; // MOV C, C
        case 0x4a: cpu->C = cpu->D; break; // MOV C, D
        case 0x4b: cpu->C = cpu->E; break; // MOV C, E
        case 0x4c: cpu->C = cpu->H; break; // MOV C, H
        case 0x4d: cpu->C = cpu->L; break; // MOV C, L
        case 0x4f: cpu->C = cpu->A; break; // MOV C, A
        case 0x50: cpu->D = cpu->B; break; // MOV D, B
        case 0x51: cpu->D = cpu->C; break; // MOV D, C
        case 0x52: cpu->D = cpu->D; break; // MOV D, D
        case 0x53: cpu->D = cpu->E; break; // MOV D, E
        case 0x54: cpu->D = cpu->H; break; // MOV D, H
        case 0x55: cpu->D = cpu->L; break; // MOV D, L
        case 0x57: cpu->D = cpu->A; break; // MOV D, A
        case 0x58: cpu->E = cpu->B; break; // MOV E, B
        case 0x59: cpu->E = cpu->C; break; // MOV E, C
        case 0x5a: cpu->E = cpu->D; break; // MOV E, D
        case 0x5b: cpu->E = cpu->E; break; // MOV E, E
        case 0x5c: cpu->E = cpu->H; break; // MOV E, H
        case 0x5d: cpu->E = cpu->L; break; // MOV E, L
        case 0x5f: cpu->E = cpu->A; break; // MOV E, A
        case 0x60: cpu->H = cpu->B; break; // MOV H, B
        case 0x61: cpu->H = cpu->C; break; // MOV H, C
        case 0x62: cpu->H = cpu->D; break; // MOV H, D
        case 0x63: cpu->H = cpu->E; break; // MOV H, E
        case 0x64: cpu->H = cpu->H; break; // MOV H, H
        case 0x65: cpu->H = cpu->L; break; // MOV H, L
        case 0x67: cpu->H = cpu->A; break; // MOV H, A
        case 0x68: cpu->L = cpu->B; break; // MOV L, B
        case 0x69: cpu->L = cpu->C; break; // MOV L, C
        case 0x6a: cpu->L = cpu->D; break; // MOV L, D
        case 0x6b: cpu->L = cpu->E; break; // MOV L, E
        case 0x6c: cpu->L = cpu->H; break; // MOV L, H
        case 0x6d: cpu->L = cpu->L; break; // MOV L, L
        case 0x6f: cpu->L = cpu->A; break; // MOV L, A
        case 0x78: cpu->A = cpu->B; break; // MOV A, B
        case 0x79: cpu->A = cpu->C; break; // MOV A, C
        case 0x7a: cpu->A = cpu->D; break; // MOV A, D
        case 0x7b: cpu->A = cpu->E; break; // MOV A, E
        case 0x7c: cpu->A = cpu->H; break; // MOV A, H
        case 0x7d: cpu->A = cpu->L; break; // MOV A, L
        case 0x7f: cpu->A = cpu->A; break; // MOV A, A

        case 0x46: cpu->B = cpu_user_load(cpu, cpu->HL); break; // MOV B, M
        case 0x4e: cpu->C = cpu_user_load(cpu, cpu->HL); break; // MOV C, M
        case 0x56: cpu->D = cpu_user_load(cpu, cpu->HL); break; // MOV D, M
        case 0x5e: cpu->E = cpu_user_load(cpu, cpu->HL); break; // MOV E, M
        case 0x66: cpu->H = cpu_user_load(cpu, cpu->HL); break; // MOV H, M
        case 0x6e: cpu->L = cpu_user_load(cpu, cpu->HL); break; // MOV L, M
        case 0x7e: cpu->A = cpu_user_load(cpu, cpu->HL); break; // MOV A, M

        case 0x70: cpu_user_store(cpu, cpu->HL, cpu->B); break; // MOV M, B
        case 0x71: cpu_user_store(cpu, cpu->HL, cpu->C); break; // MOV M, C
        case 0x72: cpu_user_store(cpu, cpu->HL, cpu->D); break; // MOV M, D
        case 0x73: cpu_user_store(cpu, cpu->HL, cpu->E); break; // MOV M, E
        case 0x74: cpu_user_store(cpu, cpu->HL, cpu->H); break; // MOV M, H
        case 0x75: cpu_user_store(cpu, cpu->HL, cpu->L); break; // MOV M, L
        case 0x77: cpu_user_store(cpu, cpu->HL, cpu->A); break; // MOV M, A

        case 0x06: cpu->B = fetch(cpu); break; // MVI B, d8
        case 0x16: cpu->D = fetch(cpu); break; // MVI D, d8
        case 0x26: cpu->H = fetch(cpu); break; // MVI H, d8
        case 0x0e: cpu->C = fetch(cpu); break; // MVI C, d8
        case 0x1e: cpu->E = fetch(cpu); break; // MVI E, d8
        case 0x2e: cpu->L = fetch(cpu); break; // MVI L, d8
        case 0x3e: cpu->A = fetch(cpu); break; // MVI A, d8
        case 0x36: cpu_user_store(cpu, cpu->HL, fetch(cpu)); break; // MVI M, d8

        case 0x02: cpu_user_store(cpu, cpu->BC, cpu->A); break; // STAX B
        case 0x12: cpu_user_store(cpu, cpu->DE, cpu->A); break; // STAX D

        case 0x0a: cpu->A = cpu_user_load(cpu, cpu->BC); break; // LDAX B
        case 0x1a: cpu->A = cpu_user_load(cpu, cpu->DE); break; // LDAX D

        case 0x32: cpu_user_store(cpu, fetch_addr(cpu), cpu->A); break; // STA a16
        case 0x3a: cpu->A = cpu_user_load(cpu, fetch_addr(cpu)); break; // LDA a16

        case 0x22: {
            addr_t addr = fetch_addr(cpu);
            cpu_user_store(cpu, addr,   cpu->L);
            cpu_user_store(cpu, addr+1, cpu->H);
        } break; // SHLD a16

        case 0x2a: {
            addr_t addr = fetch_addr(cpu);
            cpu->L = cpu_user_load(cpu, addr);
            cpu->H = cpu_user_load(cpu, addr+1);
        } break; // LHLD a16

        case 0x01: cpu->BC = fetch_addr(cpu); break; // LXI B, d16
        case 0x11: cpu->DE = fetch_addr(cpu); break; // LXI D, d16
        case 0x21: cpu->HL = fetch_addr(cpu); break; // LXI H, d16
        case 0x31: cpu->SP = fetch_addr(cpu); break; // LXI SP, d16

        case 0xe9: cpu->PC = cpu->HL; break; // PCHL
        case 0xf9: cpu->SP = cpu->HL; break; // SPHL

        case 0xeb: {
            addr_t tmp = cpu->HL;
            cpu->HL = cpu->DE;
            cpu->DE = tmp;
        } break; // XCHG

        case 0xe3: {
            data_t tmp_h = cpu->H;
            data_t tmp_l = cpu->L;

            data_t hi = cpu_user_load(cpu, cpu->SP+1);
            data_t lo = cpu_user_load(cpu, cpu->SP);
            cpu->HL = (hi << 8) | lo;

            cpu_user_store(cpu, cpu->SP+1, tmp_h);
            cpu_user_store(cpu, cpu->SP,   tmp_l);
        } break; // XTHL

        case 0xc5: push(cpu, cpu->BC);  break; // PUSH B
        case 0xd5: push(cpu, cpu->DE);  break; // PUSH D
        case 0xe5: push(cpu, cpu->HL);  break; // PUSH H
        case 0xf5: push(cpu, cpu->PSW); break; // PUSH PSW

        case 0xc1: cpu->BC  = pop(cpu); break; // POP B
        case 0xd1: cpu->DE  = pop(cpu); break; // POP D
        case 0xe1: cpu->HL  = pop(cpu); break; // POP H
        case 0xf1:
            cpu->PSW = pop(cpu);
            cpu->F = ENFORCE_FLAGS(cpu->F);
        break; // POP PSW

        case 0xfb: cpu->inte = true;  break; // EI
        case 0xf3: cpu->inte = false; break; // DI
        case 0x37: cpu->F |= FLAG_C;  break; // STC
        case 0x3f: cpu->F ^= FLAG_C;  break; // CMC

        case 0x2f: cpu->A = ~cpu->A; break; // CMA

        case 0x80: adc(cpu, cpu->B, 0); break; // ADD B
        case 0x81: adc(cpu, cpu->C, 0); break; // ADD C
        case 0x82: adc(cpu, cpu->D, 0); break; // ADD D
        case 0x83: adc(cpu, cpu->E, 0); break; // ADD E
        case 0x84: adc(cpu, cpu->H, 0); break; // ADD H
        case 0x85: adc(cpu, cpu->L, 0); break; // ADD L
        case 0x87: adc(cpu, cpu->A, 0); break; // ADD A

        case 0x88: adc(cpu, cpu->B, 1); break; // ADC B
        case 0x89: adc(cpu, cpu->C, 1); break; // ADC C
        case 0x8a: adc(cpu, cpu->D, 1); break; // ADC D
        case 0x8b: adc(cpu, cpu->E, 1); break; // ADC E
        case 0x8c: adc(cpu, cpu->H, 1); break; // ADC H
        case 0x8d: adc(cpu, cpu->L, 1); break; // ADC L
        case 0x8f: adc(cpu, cpu->A, 1); break; // ADC A

        case 0x86: adc(cpu, cpu_user_load(cpu, cpu->HL), 0); break; // ADD M
        case 0x8e: adc(cpu, cpu_user_load(cpu, cpu->HL), 1); break; // ADC M

        case 0xc6: adc(cpu, fetch(cpu), 0); break; // ADI d8
        case 0xce: adc(cpu, fetch(cpu), 1); break; // ACI d8

        case 0x27: {
            /* I still don't understand how DAA is supposed to work with the carries */
            data_t lo = (cpu->A & 0x0f);
            data_t hi = (cpu->A & 0xf0);

            /* Low nibble and half carry */
            if (lo > 0x09 || cpu->F & FLAG_A) {
                cpu->A += 0x06;
                set_flag(cpu, FLAG_A, lo + 0x06 > 0x0f);
            }

            /* High nibble and normal carry */
            if (hi > 0x90 || cpu->F & FLAG_C || (hi >= 0x90 && lo > 0x09)) {
                cpu->A += 0x60;
                cpu->F |= FLAG_C;
            }

            update_ZSP(cpu, cpu->A);

        } break; // DAA


        case 0x90: sbb(cpu, cpu->B, 0); break; // SUB B
        case 0x91: sbb(cpu, cpu->C, 0); break; // SUB C
        case 0x92: sbb(cpu, cpu->D, 0); break; // SUB D
        case 0x93: sbb(cpu, cpu->E, 0); break; // SUB E
        case 0x94: sbb(cpu, cpu->H, 0); break; // SUB H
        case 0x95: sbb(cpu, cpu->L, 0); break; // SUB L
        case 0x97: sbb(cpu, cpu->A, 0); break; // SUB A

        case 0x98: sbb(cpu, cpu->B, 1); break; // SBB B
        case 0x99: sbb(cpu, cpu->C, 1); break; // SBB C
        case 0x9a: sbb(cpu, cpu->D, 1); break; // SBB D
        case 0x9b: sbb(cpu, cpu->E, 1); break; // SBB E
        case 0x9c: sbb(cpu, cpu->H, 1); break; // SBB H
        case 0x9d: sbb(cpu, cpu->L, 1); break; // SBB L
        case 0x9f: sbb(cpu, cpu->A, 1); break; // SBB A

        case 0x96: sbb(cpu, cpu_user_load(cpu, cpu->HL), 0); break; // SUB M
        case 0x9e: sbb(cpu, cpu_user_load(cpu, cpu->HL), 1); break; // SBB M

        case 0xd6: sbb(cpu, fetch(cpu), 0); break; // SUI d8
        case 0xde: sbb(cpu, fetch(cpu), 1); break; // SBI d8

        case 0xb8: cmp(cpu, cpu->B); break; // CMP B
        case 0xb9: cmp(cpu, cpu->C); break; // CMP C
        case 0xba: cmp(cpu, cpu->D); break; // CMP D
        case 0xbb: cmp(cpu, cpu->E); break; // CMP E
        case 0xbc: cmp(cpu, cpu->H); break; // CMP H
        case 0xbd: cmp(cpu, cpu->L); break; // CMP L
        case 0xbf: cmp(cpu, cpu->A); break; // CMP A
        case 0xbe: cmp(cpu, cpu_user_load(cpu, cpu->HL)); break; // CMP M
        case 0xfe: cmp(cpu, fetch(cpu)); break; // CPI d8

        case 0xa0: ana(cpu, cpu->B); break; // ANA B
        case 0xa1: ana(cpu, cpu->C); break; // ANA C
        case 0xa2: ana(cpu, cpu->D); break; // ANA D
        case 0xa3: ana(cpu, cpu->E); break; // ANA E
        case 0xa4: ana(cpu, cpu->H); break; // ANA H
        case 0xa5: ana(cpu, cpu->L); break; // ANA L
        case 0xa7: ana(cpu, cpu->A); break; // ANA A
        case 0xa6: ana(cpu, cpu_user_load(cpu, cpu->HL)); break; // ANA M
        case 0xe6: ana(cpu, fetch(cpu)); break; // ANI d8

        case 0xb0: ora(cpu, cpu->B); break; // ORA B
        case 0xb1: ora(cpu, cpu->C); break; // ORA C
        case 0xb2: ora(cpu, cpu->D); break; // ORA D
        case 0xb3: ora(cpu, cpu->E); break; // ORA E
        case 0xb4: ora(cpu, cpu->H); break; // ORA H
        case 0xb5: ora(cpu, cpu->L); break; // ORA L
        case 0xb7: ora(cpu, cpu->A); break; // ORA A
        case 0xb6: ora(cpu, cpu_user_load(cpu, cpu->HL)); break; // ORA M
        case 0xf6: ora(cpu, fetch(cpu)); break; // ORI d8

        case 0xa8: xra(cpu, cpu->B); break; // XRA B
        case 0xa9: xra(cpu, cpu->C); break; // XRA C
        case 0xaa: xra(cpu, cpu->D); break; // XRA D
        case 0xab: xra(cpu, cpu->E); break; // XRA E
        case 0xac: xra(cpu, cpu->H); break; // XRA H
        case 0xad: xra(cpu, cpu->L); break; // XRA L
        case 0xaf: xra(cpu, cpu->A); break; // XRA A
        case 0xae: xra(cpu, cpu_user_load(cpu, cpu->HL)); break; // XRA M
        case 0xee: xra(cpu, fetch(cpu)); break; // XRI d8

        case 0x07: {
            int bit = (cpu->A >> 7) & 0x01;
            cpu->A = (cpu->A << 1) | bit;
            set_flag(cpu, FLAG_C, bit);
        } break; // RLC

        case 0x17: {
            int bit = (cpu->A >> 7) & 0x01;
            cpu->A = (cpu->A << 1) | (cpu->F & FLAG_C); // FLAG_C is bit 0
            set_flag(cpu, FLAG_C, bit);
        } break; // RAL

        case 0x0f: {
            int bit = cpu->A & 0x01;
            cpu->A = (cpu->A >> 1) | (bit << 7);
            set_flag(cpu, FLAG_C, bit);
        } break; // RRC

        case 0x1f: {
            int bit = cpu->A & 0x01;
            cpu->A = (cpu->A >> 1) | ((cpu->F & FLAG_C) << 7); // FLAG_C is bit 0
            set_flag(cpu, FLAG_C, bit);
        } break; // RAR

        case 0x04: cpu->B = inc8(cpu, cpu->B, +1); break; // INR B
        case 0x0c: cpu->C = inc8(cpu, cpu->C, +1); break; // INR C
        case 0x14: cpu->D = inc8(cpu, cpu->D, +1); break; // INR D
        case 0x1c: cpu->E = inc8(cpu, cpu->E, +1); break; // INR E
        case 0x24: cpu->H = inc8(cpu, cpu->H, +1); break; // INR H
        case 0x2c: cpu->L = inc8(cpu, cpu->L, +1); break; // INR L
        case 0x3c: cpu->A = inc8(cpu, cpu->A, +1); break; // INR A
        case 0x34: cpu_user_store(cpu, cpu->HL, inc8(cpu, cpu_user_load(cpu, cpu->HL), +1)); break; // INR M

        case 0x05: cpu->B = inc8(cpu, cpu->B, -1); break; // DCR B
        case 0x0d: cpu->C = inc8(cpu, cpu->C, -1); break; // DCR C
        case 0x15: cpu->D = inc8(cpu, cpu->D, -1); break; // DCR D
        case 0x1d: cpu->E = inc8(cpu, cpu->E, -1); break; // DCR E
        case 0x25: cpu->H = inc8(cpu, cpu->H, -1); break; // DCR H
        case 0x2d: cpu->L = inc8(cpu, cpu->L, -1); break; // DCR L
        case 0x3d: cpu->A = inc8(cpu, cpu->A, -1); break; // DCR A
        case 0x35: cpu_user_store(cpu, cpu->HL, inc8(cpu, cpu_user_load(cpu, cpu->HL), -1)); break; // DCR M

        case 0x03: cpu->BC++; break; // INX B
        case 0x13: cpu->DE++; break; // INX D
        case 0x23: cpu->HL++; break; // INX H
        case 0x33: cpu->SP++; break; // INX SP

        case 0x0b: cpu->BC--; break; // DCX B
        case 0x1b: cpu->DE--; break; // DCX D
        case 0x2b: cpu->HL--; break; // DCX H
        case 0x3b: cpu->SP--; break; // DCX SP

        case 0x09: dad(cpu, cpu->BC); break; // DAD B
        case 0x19: dad(cpu, cpu->DE); break; // DAD D
        case 0x29: dad(cpu, cpu->HL); break; // DAD H
        case 0x39: dad(cpu, cpu->SP); break; // DAD SP

        case 0xdb: cpu->A = cpu_user_input(cpu, fetch(cpu)); break; // IN d8
        case 0xd3: cpu_user_output(cpu, fetch(cpu), cpu->A); break; // OUT d8

        case 0xc3:
        case 0xcb: jmp(cpu, true);    break; // JMP
        case 0xc2: jmp(cpu, COND_NZ); break; // JNZ
        case 0xd2: jmp(cpu, COND_NC); break; // JNC
        case 0xe2: jmp(cpu, COND_PO); break; // JPO
        case 0xf2: jmp(cpu, COND_P ); break; // JP
        case 0xca: jmp(cpu, COND_Z ); break; // JZ
        case 0xda: jmp(cpu, COND_C ); break; // JC
        case 0xea: jmp(cpu, COND_PE); break; // JPE
        case 0xfa: jmp(cpu, COND_M ); break; // JM

        case 0xc9:
        case 0xd9: ret(cpu, true);    break; // RET
        case 0xc0: ret(cpu, COND_NZ); break; // RNZ
        case 0xd0: ret(cpu, COND_NC); break; // RNC
        case 0xe0: ret(cpu, COND_PO); break; // RPO
        case 0xf0: ret(cpu, COND_P ); break; // RP
        case 0xc8: ret(cpu, COND_Z ); break; // RZ
        case 0xd8: ret(cpu, COND_C ); break; // RC
        case 0xe8: ret(cpu, COND_PE); break; // RPE
        case 0xf8: ret(cpu, COND_M ); break; // RM

        case 0xcd:
        case 0xdd:
        case 0xed:
        case 0xfd: call(cpu, true);    break; // CALL
        case 0xc4: call(cpu, COND_NZ); break; // CNZ
        case 0xd4: call(cpu, COND_NC); break; // CNC
        case 0xe4: call(cpu, COND_PO); break; // CPO
        case 0xf4: call(cpu, COND_P ); break; // CP
        case 0xcc: call(cpu, COND_Z ); break; // CZ
        case 0xdc: call(cpu, COND_C ); break; // CC
        case 0xec: call(cpu, COND_PE); break; // CPE
        case 0xfc: call(cpu, COND_M ); break; // CM

        case 0xc7:
        case 0xcf:
        case 0xd7:
        case 0xdf:
        case 0xe7:
        case 0xef:
        case 0xf7:
        case 0xff: push(cpu, cpu->PC); cpu->PC = opc & 0x38; break; // RST x
    }

    int cycles = cycle_table[opc];
    cpu->cycles += cycles;
    return cycles;
}
