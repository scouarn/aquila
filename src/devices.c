#include <stdio.h>
#include <unistd.h>

#include "devices.h"

data_t sio_update_read(sio_t *sio, port_t port)
{
    if (port == sio->base_port) { /* Status */
        data_t data = 0x00;

        if (!tty_input_ready(port)) {
            data |= SIO_INPUT_BUSY_MASK;
        }

        if (!tty_output_ready(port)) {
            data |= SIO_OUTPUT_BUSY_MASK;
        }

        return data;
    }
    else if (port == sio->base_port + 1) { /* Data */
        return tty_input(port);
    }

    return 0x00;
}

void sio_update_write(sio_t *sio, port_t port, data_t data)
{
    if (port == sio->base_port) { /* Control */
        /* TODO: Control, requires set_baudrate, error handling, ... */
    }
    else if (port == sio->base_port + 1) { /* Data */
        tty_output(port, data);
    }
}

data_t sio2_update_read(sio2_t *sio2, port_t port)
{
    data_t data = 0x00;

    switch (port - sio2->base_port) {
        case 0: /* Status 1 */
            if (tty_input_ready(port)) {
                data |= SIO2_INPUT_READY_MASK;
            }
            if (tty_output_ready(port)) {
                data |= SIO2_OUTPUT_READY_MASK;
            }
            return data;
        break;

        case 1: /* Data 1 */
            return tty_input(port);
        break;

        case 2: /* Status 2 */
            if (tty_input_ready(port)) {
                data |= SIO2_INPUT_READY_MASK;
            }
            if (tty_output_ready(port)) {
                data |= SIO2_OUTPUT_READY_MASK;
            }
            return data;
        break;

        case 3: /* Data 2 */
            return tty_input(port);
        break;

        default: return 0x00; break;
    }

    return 0x00;
}

void sio2_update_write(sio2_t *sio2, port_t port, data_t data)
{
    switch (port - sio2->base_port) {
        case 0: /* Control 1 */
            /* TODO: Control, requires set_baudrate, error handling, ... */
        break;

        case 1: /* Data 1 */
            tty_output(port, data);
        break;

        case 2: /* Control 2 */
            /* TODO: Control, requires set_baudrate, error handling, ... */
        break;

        case 3: /* Data 2 */
            tty_output(port, data);
        break;

        default: break;
    }
}


#define CURR_DRIVE (dcdd->drives[dcdd->curr_drive])

static void write_sector(dcdd_t *dcdd)
{
    uint8_t track  = CURR_DRIVE.curr_track;
    uint8_t sector = CURR_DRIVE.curr_sector;
    for (int i = CURR_DRIVE.curr_byte; i < DCDD_SECTOR_SIZE; i++) {
        dcdd->buff[i] = 0;
    }
    disk_write_sector(dcdd->curr_drive, track, sector, dcdd->buff);
    dcdd->buff_inval = false;
}

data_t dcdd_update_read(dcdd_t *dcdd, port_t port)
{
    switch (port) {
        case DCDD_STATUS_PORT:
            // TODO: REMOVE DEBUG
            //fprintf(stderr, "READ STATUS DRIVE:%d TRACK:%d SECTOR:%d\r\n", dcdd->curr_drive, CURR_DRIVE.curr_track, CURR_DRIVE.curr_sector);
            return CURR_DRIVE.status;
        break;

        case DCDD_CONTROL_PORT: { /* Sector status */
            if (dcdd->buff_inval) {
                write_sector(dcdd);
            }

            /* Increment sector to simulate disk spinning (inaccurate) */
            CURR_DRIVE.curr_sector++;
            CURR_DRIVE.curr_sector %= DCDD_NUM_SECTORS;
            dcdd->buff_loaded = false;

            // TODO: REMOVE DEBUG
            //fprintf(stderr, "READ SECTOR NUM DRIVE:%d TRACK:%d SECTOR:%d\r\n", dcdd->curr_drive, CURR_DRIVE.curr_track, CURR_DRIVE.curr_sector);

            return CURR_DRIVE.curr_sector << 1;
        } break;

        case DCDD_DATA_PORT: {
            if (!dcdd->buff_loaded) {
                disk_read_sector(
                    dcdd->curr_drive,
                    CURR_DRIVE.curr_track,
                    CURR_DRIVE.curr_sector,
                    dcdd->buff
                );
                dcdd->buff_loaded = true;
                CURR_DRIVE.curr_byte = 0;

                // TODO: REMOVE DEBUG
                //fprintf(stderr, "LOADED DRIVE:%d TRACK:%d SECTOR:%d\r\n", dcdd->curr_drive, CURR_DRIVE.curr_track, CURR_DRIVE.curr_sector);
            }

            data_t data = dcdd->buff[CURR_DRIVE.curr_byte++];
            CURR_DRIVE.curr_byte %= DCDD_SECTOR_SIZE;

            // TODO: RESET READY FLAG ?

            // TODO: REMOVE DEBUG
            // fprintf(stderr, "CURR_BYTE %d DATA %d\r\n", CURR_DRIVE.curr_byte, data);

            return data;
        } break;

        default: return 0x00; break;
    }

    return 0x00;
}

// TODO: Head move bit is always 0 (true)
void dcdd_update_write(dcdd_t *dcdd, port_t port, data_t data)
{
    switch (port) {
        case DCDD_STATUS_PORT: /* SELECT */
            if (dcdd->buff_inval) {
                write_sector(dcdd);
            }

            if (data & DCDD_ENABLE_MASK) { /* Disable drive */
                CURR_DRIVE.status = ~DCDD_STATUS_UNUSED_MASK; /* TODO: init the drive before that */

                // TODO: REMOVE DEBUG
                //fprintf(stderr, "** DISABLE DRIVE %d\r\n", dcdd->curr_drive);
            }
            else { /* Enable drive */

                // FIXME: Don't know how invalid drives should be treated
                // They are 2 BDOS errors related to nonexistent drives: http://www.gaby.de/cpm/manuals/archive/cpm22htm/axi.htm
                //      Bdos Err On d: Bad Sector
                //      Bdos Err on d: Select
                // When setting status ff on nonexistant drive it disables and reenabled in a loop (C^C or return not working)
                // When setting status to ~DCDD_UNUSED_MASK it hangs but don't know what it does (C^C or return not working)
                // (CURRENT BEHAVIOUR) When enableing head move it says bad sector (simh does the same)
                //
                // When swapping to drive E,F,... it says "Error on d: select" without sending a select command to the drives
                //          -> Maybe CP/M only knows A->D but some programs (like format.com) allow A->P ?

                dcdd->curr_drive  = data & DCDD_SELECT_MASK;
                dcdd->buff_loaded = false;
                CURR_DRIVE.status = ~DCDD_STATUS_UNUSED_MASK
                                  & ~DCDD_HEAD_MOVE_MASK;
                if (CURR_DRIVE.curr_track == 0) {
                    CURR_DRIVE.status &= ~DCDD_TRACK0_MASK;
                }

                // TODO: REMOVE DEBUG
                //fprintf(stderr, "** SELECT DRIVE %d\r\n", dcdd->curr_drive);
            }
        break;

        case DCDD_CONTROL_PORT:
            if (data & DCDD_STEP_IN_MASK) {
                if (dcdd->buff_inval) {
                    write_sector(dcdd);
                }

                dcdd->buff_loaded = false;

                if (CURR_DRIVE.curr_track < DCDD_NUM_TRACKS) {
                    CURR_DRIVE.curr_track++;
                    CURR_DRIVE.status |= DCDD_TRACK0_MASK;
                }

                // TODO: REMOVE DEBUG
                //fprintf(stderr, "** STEP IN TO TRACK %d\r\n", CURR_DRIVE.curr_track);
            }

            if (data & DCDD_STEP_OUT_MASK) {
                if (dcdd->buff_inval) {
                    write_sector(dcdd);
                }

                dcdd->buff_loaded = false;

                CURR_DRIVE.curr_track--;
                if (CURR_DRIVE.curr_track < 0) {
                    CURR_DRIVE.curr_track = 0;
                    CURR_DRIVE.status &= ~DCDD_TRACK0_MASK;
                }

                // TODO: REMOVE DEBUG
               //fprintf(stderr, "** STEP OUT TO TRACK %d\r\n", CURR_DRIVE.curr_track);
            }

            if (data & DCDD_LOAD_MASK) {
                CURR_DRIVE.status &= ~DCDD_LOADED_MASK
                                  &  ~DCDD_READ_READY_MASK;

                // TODO: REMOVE DEBUG
                // fprintf(stderr, "** LOAD HEAD\r\n");
            }

            if (data & DCDD_UNLOAD_MASK) {
                CURR_DRIVE.status |= DCDD_LOADED_MASK
                                  |  DCDD_READ_READY_MASK;

                // TODO: REMOVE DEBUG
                // fprintf(stderr, "** UNLOAD HEAD\r\n");
            }

            if (data & DCDD_WRITE_MASK) {
                CURR_DRIVE.status &= ~DCDD_WRITE_READY_MASK;
                CURR_DRIVE.curr_byte = 0;

                // TODO: REMOVE DEBUG
                //fprintf(stderr, "** START WRITE\r\n");
                //exit(1);
            }
        break;

        case DCDD_DATA_PORT:
            if (CURR_DRIVE.curr_byte >= DCDD_SECTOR_SIZE) { /* The controller expects one more byte at the end */
                write_sector(dcdd);
                CURR_DRIVE.status |= DCDD_WRITE_READY_MASK;
            }
            else {
                dcdd->buff_inval = true;
                dcdd->buff[CURR_DRIVE.curr_byte++] = data;
            }
        break;

        default: break;
    }
}
