#ifndef I8080_H
#define I8080_H

#ifndef __BYTE_ORDER__
#error "__BYTE_ORDER__ not defined"

/* PDP endianness is the same as little endian at the 16bit word level */
#elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__ \
   || __BYTE_ORDER__ == __ORDER_PDP_ENDIAN__
#define ORDER(A, B) struct { B; A; }

#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define ORDER(A, B) struct { A; B; }

#endif


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

typedef uint16_t addr_t;
typedef uint8_t  data_t;
typedef uint8_t  port_t;

/* Bits of the status register */
#define FLAG_C (1<<0)
#define FLAG_P (1<<2)
#define FLAG_A (1<<4)
#define FLAG_Z (1<<6)
#define FLAG_S (1<<7)

/* The bit which is always 1 and the bits which are always 0 */
#define ENFORCE_FLAGS(F) ((F | 0x02) & 0xd7)

typedef struct cpu_t {

    /* Register bank assuming little endian */
    union {
        addr_t PSW;
        ORDER(data_t A, data_t F);
    };

    union {
        addr_t BC;
        ORDER(data_t B, data_t C);
    };

    union {
        addr_t DE;
        ORDER(data_t D, data_t E);
    };

    union {
        addr_t HL;
        ORDER(data_t H, data_t L);
    };

    addr_t SP, PC;

    /* Output pins: hold/halt, interrupt enabled */
    bool hold, inte, wait; // TODO: hold is wrong (?) see implementation

    /* Input pins:
       Setting reset and stepping resets PC, inte, cycles and
       reset is set back to false

       ready does nothing and is juste an indicator
       set by io which does the waiting */
    bool ready, reset;

    /* Number of cycles since last reset */
    uint64_t cycles;

    /* Set to request an interrupt, the instruction
       will be fetched from this pointer */
    // TODO: Not shure it's worth it to support multi byte interrupt request
    data_t *irq_data;
    bool irq_ack;

} cpu_t;

/* Step and return number of cycles */
int cpu_step(cpu_t* cpu);

/* Application defined callbacks */
extern data_t cpu_user_load  (cpu_t *cpu, addr_t addr);
extern void   cpu_user_store (cpu_t *cpu, addr_t addr, data_t data);
extern data_t cpu_user_input (cpu_t *cpu, port_t port);
extern void   cpu_user_output(cpu_t *cpu, port_t port, data_t data);


#undef ORDER
#endif
