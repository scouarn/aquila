import random
import numpy as np
import matplotlib.pyplot as plt

# Parameters
sample_rate = 22050
baud_rate = 300
f1 = 1850
f2 = 2400


# Input data
bits_in = [1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1] * 1
#random.shuffle(bits_in)
samples_per_bit = sample_rate / baud_rate
binary_signal = np.repeat(bits_in, 0.9*samples_per_bit)
t = np.arange(0, len(binary_signal) / sample_rate, 1.0 / sample_rate)

def modulate(binary_signal, f1, f2):
    # Convert bits to tones
    tones = np.array([0.8*f1 if b == 0 else f2 for b in binary_signal])

    # Continuous phase FSK
    delta_phase = tones * 2.0 * np.pi / sample_rate
    phase = np.cumsum(delta_phase)
    signal = np.sin(phase)

    # Add some noise
    noise = np.random.random(len(t))*2 - 1
    signal_to_noise_ratio = 1
    signal = signal_to_noise_ratio*signal + (1-signal_to_noise_ratio)*noise

    return signal


# Demodulate
def squared_average_of_product(signal, osc):
    product = signal * osc
    squared_average = []
    # Low pass filter of freq baud_rate
    y = 0
    for x in product:
        y += (x - y) * baud_rate * 2 * np.pi / sample_rate
        squared_average.append(y*y)

    return np.array(squared_average)

def dft_energy(signal, f):
    osc_sin = np.sin(2.0 * np.pi * f * t)
    osc_cos = np.cos(2.0 * np.pi * f * t)
    e_sin = squared_average_of_product(signal, osc_sin)
    e_cos = squared_average_of_product(signal, osc_cos)
    e_total = e_sin + e_cos
    return e_total

def demodulate(signal, f1, f2):
    e1 = dft_energy(signal, f1)
    e2 = dft_energy(signal, f2)
    diff = e2 - e1
    return diff

def lpf(X, f):
    Y = []
    y = 0
    for x in X:
        y += (x - y) * f * 2 * np.pi / sample_rate
        Y.append(y)
    return np.array(Y)

signal = modulate(binary_signal, 1.0*f1, 1.0*f2)
diff = demodulate(signal, f1, f2)
#diff = lpf(diff, 1)


def demodulate(signal, f1, f2, baud_rate):
    debug_signals = []
    output = []

    t = 0
    dt = 1 / sample_rate

    lpf_1_cos = 0
    lpf_1_sin = 0
    lpf_2_cos = 0
    lpf_2_sin = 0

    threshold = 0.1
    demodulated_binary = 0

    sample_phase = 0
    sample_phase_error = 0

    for x in signal:
        z_1_cos = x * np.cos(2.0 * np.pi * f1  * t);
        z_1_sin = x * np.sin(2.0 * np.pi * f1  * t);
        z_2_cos = x * np.cos(2.0 * np.pi * f2  * t);
        z_2_sin = x * np.sin(2.0 * np.pi * f2  * t);

        lpf_factor = baud_rate * 2 * np.pi / sample_rate
        lpf_1_cos += lpf_factor * (z_1_cos - lpf_1_cos)
        lpf_1_sin += lpf_factor * (z_1_sin - lpf_1_sin)
        lpf_2_cos += lpf_factor * (z_2_cos - lpf_2_cos)
        lpf_2_sin += lpf_factor * (z_2_sin - lpf_2_sin)

        power_1 = lpf_1_cos * lpf_1_cos + lpf_1_sin * lpf_1_sin;
        power_2 = lpf_2_cos * lpf_2_cos + lpf_2_sin * lpf_2_sin;

        # TODO: adjust f1 and f2
        diff = power_2 - power_1

        old_demodulated_binary = demodulated_binary
        if diff > threshold:
            demodulated_binary = 1
        elif diff < -threshold:
            demodulated_binary = 0

        edge = old_demodulated_binary != demodulated_binary

        if edge: # We want sample_phase to be 0.5 here so we need to correct baud_rate
            sample_phase_error = 0.5 - sample_phase
            sample_phase = 0.5
            baud_rate += 100*sample_phase_error # Just a proportional controller

        sample_phase += baud_rate / sample_rate
        if sample_phase >= 1:
            sample_phase = 0
            bit = demodulated_binary - 0.5
            output.append(demodulated_binary)
        else:
            bit = 0

        debug_signals.append((diff, bit))
        t += dt

    return np.array(output), np.array(debug_signals)

demodulated, debug_signals = demodulate(signal, f1, f2, baud_rate)

print(demodulated)
print(all(demodulated == bits_in))

#e1 = goertzel(signal, f1, N)
#e2 = goertzel(signal, f2, N)
#diff_g = e2 - e1

baud = np.cos(t * baud_rate * 2 * np.pi)

plt.plot(t, binary_signal);
#plt.plot(t, signal);
#plt.plot(t, diff);
plt.plot(t, debug_signals);
#plt.plot(t, baud);
#plt.plot(t, e2);
plt.show()
