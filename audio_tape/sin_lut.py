#!/usr/bin/env python3

import math
import matplotlib.pyplot as plt

sin_lut = []

for i in range(256):
    x = 2 * math.pi * i / 256
    y = round(127 * math.sin(x))
    sin_lut.append(y)

cos_lut = [ sin_lut[(i + 64) % 256] for i in range(256) ]

print("int8_t sin_lut[256] = {", ", ".join([f"{x:-4}" for x in sin_lut]), "};")


plt.plot(sin_lut)
plt.plot(cos_lut)
plt.show()
