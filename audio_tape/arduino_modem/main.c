#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>

#define BAUD 300UL
#define LED_PIN PORTB7 /* pin 13 */
#define AUD_TX  PORTB6 /* pin 12 */
#define SER_RX  PORTB4 /* pin 10 */
#define SER_TX  PORTB5 /* pin 11 */
// AUDIO RX on AIN1 pin5

#define MARK  2400UL
#define SPACE 1850UL

/* FIXME: The device is intended to work at 300 baud but I can't make the USB
work at that speed, it will be anoying to debug */
#define BAUD_USB 9600UL
#define BAUD_PRESCALE(BAUD) ((F_CPU / (16UL*(unsigned long)BAUD))-1UL)

/*
    RX0/TX0: 9600Baud USB Debug
    RX1: 300Baud Bit stream reconstruction of demodulator output (for debug)

    PB??/PB??: 300Baud Serial device IO (no hardware UART)

    AUDIO IN --> Demodulator --> TX1
                   |
                   v
                UART1 RX
                   |
                   v
          +--- Debugger logic --> UART0 TX (USB)
          |
    RX1 --+--> Tone select --> AUDIO OUT

    SER_RX --> Tone select --> AUD_TX
    AUD_RX --> Demodulator --> SER_TX
*/

/* Utility functions */

void print_char(char c)
{
    while ((UCSR0A & _BV(UDRE0)) == 0) {} /* Wait until ready */
    UDR0 = c;
}

void print_str(const char *s)
{
    while (*s != '\0') {
        print_char(*s);
        s++;
    }
}

void print_u4(uint8_t n)
{
    n &= 0x0f;
    if (n >= 10) {
        n += 'A' - 10;
    } else {
        n += '0';
    }
    print_char(n);
}

void print_u8(uint16_t n)
{
    print_u4(n >> 4);
    print_u4(n);
}

void print_u16(uint16_t n)
{
    print_u4(n >> 12);
    print_u4(n >>  8);
    print_u4(n >>  4);
    print_u4(n);
}

/* Modulator - Generate one of two tones based on the value of SER_X */

/* Tone generation */
#define TONE_VAL(FREQ) ((1UL<<16) - (F_CPU / (1UL*FREQ)) / 2UL) /* 1UL is clock prescaler */
#define SET_TONE(FREQ) do { tone_reset_val = TONE_VAL(FREQ); } while (0)
uint16_t tone_reset_val = TONE_VAL(MARK);
ISR(TIMER1_OVF_vect)
{
    TCNT1 = tone_reset_val;
    PORTB ^= _BV(AUD_TX);
}

/* Modulate SER_RX */
ISR(PCINT0_vect)
{
    if(PINB & _BV(SER_RX)) {
        SET_TONE(MARK);
    } else {
        SET_TONE(SPACE);
    }
}

void modulator_init(void)
{
    /* Timer 1 for tone generator */
    TCCR1B = _BV(CS10); /* Clock divide 1 */
    TIMSK1 = _BV(TOIE1); /* Enable interrupt */
    DDRB  |= _BV(AUD_TX); /* Audio out */

    /* Pin change interupt on SER_RX */
    DDRB   &= ~_BV(SER_RX); /* Serial in */
    PORTB  |=  _BV(SER_RX); /* Enable pullup (useful ?) */
    PCICR  |=  _BV(PCIE0); /* Enable pin change interrupts */
    PCMSK0 |=  _BV(PCINT4); /* Enable interupt on PCINT4 (PB4, pin10) */
}


/* Demodulator - Extract binary signal from audio input */
#define P1 135UL /* 1850Hz in TIMER0 ticks */
#define P2 104UL /* 2400Hz in TIMER0 ticks */
#define PTRESH 4

bool detect = false;
uint8_t period = 0;
uint8_t demodulated = 0;

ISR(ANALOG_COMP_vect)
{
    period = TCNT0;
    TCNT0 = 0;

    if (period < P1+PTRESH && period > P1-PTRESH) {
        demodulated = 0;
        PORTB &= ~_BV(SER_TX);
        PORTB |=  _BV(LED_PIN);
        detect = true;
    } else if (period < P2+PTRESH && period > P2-PTRESH) {
        demodulated = 1;
        PORTB |= _BV(SER_TX);
        PORTB |= _BV(LED_PIN);
        detect = true;
    } else {
        detect = false;
        PORTB &= ~_BV(LED_PIN);
    }

}

void demodulator_init(void)
{
    /* Setup analog comparator */
    ACSR = _BV(ACBG) | _BV(ACIE) | _BV(ACIS1) | _BV(ACIS0); /* Enable interrupt on rising edge and use internal reference (1.1V) for + side of the comparator since AIN0 is not connected on the arduino mega... */
    DIDR1 = _BV(AIN1D); /* Disable digital input for AIN1 */

    /* Enable outputs */
    DDRB |= _BV(LED_PIN);
    DDRB |= _BV(SER_TX);

    /* Timer 0 for period measure */
    TCCR0B = _BV(CS01) | _BV(CS00); /* Clock divide 64 */
    //TIMSK0 = _BV(TOIE0); /* Enable interrupt */
}


/* Serial scope output for testing */
#define WAVEFORM_SIZE 128
uint16_t waveform_idx = 0;
uint8_t waveform[WAVEFORM_SIZE];

ISR(TIMER3_OVF_vect)
{
    TCNT3 = 1<<15;
    /* Read ADC */
    while (ADCSRA & _BV(ADSC)) {} /* Wait until it has been read */
    ADCSRA |= _BV(ADSC); /* Start reading next value */
    uint16_t full_sample = ADC;

    // TODO: add some gain, maybe just multiply with bit shifts,
    // Or add a reference voltage and use it in the audio coupler instead of +5
    uint8_t sample = full_sample >> 2; /* sample is between 0 and 1023 */


    /* Record */
    waveform[waveform_idx++] = sample;
    if (waveform_idx >= WAVEFORM_SIZE) {
        waveform_idx = 0;

        for (int i = 0; i < WAVEFORM_SIZE; i++) {
            print_u8(waveform[i]);
            print_char(' ');
        }
        print_str("\r\n");
    }
}

void scope_init(void)
{
    /* Setup analog pin for audio input */
    ADMUX  = 0; /* Enable ADC0 with no left-align */
    ADMUX  |= _BV(REFS0); /* Use 5V reference */
    ADCSRA = _BV(ADEN) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0); /* Enable ADC with prescaler 128: 125KHz */
    ADCSRA |= _BV(ADSC); /* Read first value */

    /* Timer 3 for sampling */
    TCCR3B = _BV(CS30); /* Clock divide 1 */
    TIMSK3 = _BV(TOIE3); /* Enable interrupt */
}

int main(void)
{
    /* Debbuging serial USB (port 0) */
    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8N1 */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0); /* Enable rx & tx */
    UBRR0  = BAUD_PRESCALE(BAUD_USB);

    /* Debbuging serial (port 1) */
    UCSR1C = _BV(UCSZ01) | _BV(UCSZ10); /* 8N1 */
    UCSR1B = _BV(RXEN0) | _BV(TXEN1); /* Enable rx & tx */
    UBRR1  = BAUD_PRESCALE(BAUD);

    //modulator_init();
    demodulator_init();
    //scope_init();

    sei();

    while (1) {
        /* Relay USB to TX1, modulator debug */
        if (UCSR0A & _BV(RXC0)) { /* If UART0 available */
            char c = UDR0;
            while ((UCSR1A & _BV(UDRE1)) == 0) {} /* Wait until ready to send */
            UDR1 = c;
        }

        /* Relay RX1 to USB, demodulator debug */
        if (UCSR1A & _BV(RXC1)) { /* If UART1 available */
            char c = UDR1;
            while ((UCSR0A & _BV(UDRE0)) == 0) {} /* Wait until ready to send */
            UDR0 = c;
        }

        //print_str("Hello, \r\n");
        //if (detect) {
        //    print_u8(period);
        //    print_str("    ");
        //    print_u8(demodulated);
        //    print_str("\r\n");
        //}
        //_delay_ms(10);
    }
}
