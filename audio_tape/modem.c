#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>
#include <limits.h>

typedef struct {
    char      chunk_id[4];     /* "RIFF" */
    uint32_t  chunk_size;      /* Size of the rest of the file */
    char      format[4];       /* "WAVE" */

    char      fmt_chunk_id[4];  /* "fmt " */
    uint32_t  fmt_chunk_size;   /*  Should be 16 for PCM */
    uint16_t  audio_format;    /*  1 for PCM */
    uint16_t  num_channels;
    uint32_t  sample_rate;
    uint32_t  byte_rate;       /*  SampleRate * NumChannels * sizeof(sample) */
    uint16_t  block_align;     /*  NumChannels * sizeof(sample) -> bytes per alsa "frame" */
    uint16_t  bits_per_sample; /*  8 * sizeof(sample) */

    char      data_chunk_id[4]; /* "data" */
    uint32_t  data_chunk_size;  /* Number of bytes in data.  */
} __attribute__((packed)) wav_header_t;

typedef struct {
    int sample_rate;
    int baud_rate;
    int space_freq;
    int mark_freq;
} tape_fmt_t;


/* CPFSK, take current phase and return new phase */
float send_bit_sine(int bit, float phase, const tape_fmt_t *format, FILE *ofile)
//float send_bit(int bit, float phase, const tape_fmt_t *format, FILE *ofile)
{
    float freq = bit ? format->mark_freq : format->space_freq;
    size_t samples_per_bit = format->sample_rate / format->baud_rate;
    float delta_phase = freq * M_PI / (0.5f * format->sample_rate);

    for (size_t i = 0; i < samples_per_bit; i++) {
        float y = sinf(phase);
        phase += delta_phase;
        if (phase >= 2.0f*M_PI) {
            phase -= 2.0f*M_PI;
        }
        uint8_t sample = y * 127 + 127;
        fputc(sample, ofile);
    }

    return phase;
}

/* Period is between 0 and 255 instead of 0 and 2PI */
//int send_bit_triangle(int bit, int phase, const tape_fmt_t *format, FILE *ofile)
int send_bit(int bit, int phase, const tape_fmt_t *format, FILE *ofile)
{
    int freq = bit ? format->mark_freq : format->space_freq;
    int samples_per_bit = format->sample_rate / format->baud_rate;
    int delta_phase = freq * 255 / format->sample_rate;

    for (int i = 0; i < samples_per_bit; i++) {
        uint8_t sample = phase > 127 ? 255 : 0; // For square wave
        //uint8_t sample = phase; // For triangle wave
        phase += delta_phase;
        if (phase > 255) {
            phase -= 255;
        }
        fputc(sample, ofile);
    }

    return phase;
}


/* 8N1 FSK Modulator */
int encode(const tape_fmt_t *format, FILE *ifile, FILE *ofile)
{
    if (fseek(ifile, 0L, SEEK_END) != 0) {
        perror("Could't seek file");
        return -1;
    }

    size_t input_size_bytes = ftell(ifile);
    fseek(ifile, 0L, SEEK_SET);

    size_t frame_size_bits = 10; /* 8N1 */
    size_t samples_per_bit = format->sample_rate / format->baud_rate;
    size_t num_samples = (input_size_bytes*frame_size_bits + 2 /* Idle bits */) * samples_per_bit;

    wav_header_t header = {
        .chunk_id = "RIFF",
        .chunk_size = num_samples + sizeof(header) - offsetof(wav_header_t, chunk_size),
        .format = "WAVE",
        .fmt_chunk_id = "fmt ",
        .fmt_chunk_size = 16,
        .audio_format = 1,
        .num_channels = 1,
        .sample_rate = format->sample_rate,
        .byte_rate = format->sample_rate,
        .block_align = 1,
        .bits_per_sample = 8,
        .data_chunk_id = "data",
        .data_chunk_size = num_samples,
    };

    if (fwrite(&header, sizeof(header), 1, ofile) != 1) {
        perror("Couldn't write header");
        return -1;
    }

    float phase = 0.0f;
    phase = send_bit(1, phase, format, ofile); /* Idle */

    int byte;
    ssize_t size;
    while ((size = fread(&byte, 1, 1, ifile)) > 0) {
        /* 8N1: Start (0), data (byte), Stop (1) */
        uint16_t frame = 1 << 9 | byte << 1 | 0;
        for (size_t i = 0; i < frame_size_bits; i++) {
            int bit = frame & 1;
            frame >>= 1;
            phase = send_bit(bit, phase, format, ofile);
        }
    }

    phase = send_bit(1, phase, format, ofile); /* Idle */

    if (size < 0) {
        perror("Reading byte");
        return -1;
    }

    return 0;
}


/* FSK demodulator
   We want to known at each time how much the signal resemble these two
   frequencies using a DFT to reconstruct the binary signal, then we sample
   this binary signal at the correct moments to extract bits (we need to sync
   our clock with the signal), finally the 8N1 signal is decoded from the bits
*/
int decode_float(const tape_fmt_t *format, FILE *ifile, FILE *ofile)
{
    wav_header_t header;
    if (fread(&header, sizeof(header), 1, ifile) != 1) {
        fprintf(stderr, "Couldn't load header\n");
        return -1;
    }

    fprintf(stderr, "Loaded header:\n");
    fprintf(stderr, "  chans = %u\n", header.num_channels);
    fprintf(stderr, "  srate = %u\n", header.sample_rate);
    fprintf(stderr, "  brate = %u\n", header.byte_rate);
    fprintf(stderr, "  align = %u\n", header.block_align);
    fprintf(stderr, "  bits  = %u\n", header.bits_per_sample);
    fprintf(stderr, "  size  = %u\n", header.data_chunk_size);

    if (header.num_channels != 1) {
        fprintf(stderr, "Only supports 1 channel\n");
        return -1;
    }

    if (header.bits_per_sample != 8) {
        fprintf(stderr, "Only supports 8bits unsigned\n");
        return -1;
    }

    const float sample_rate = header.sample_rate;

    float t = 0.0f;
    float baud_rate = format->baud_rate;
    float f1 = format->space_freq; // TODO: Ajust these frequencies
    float f2 = format->mark_freq;

    float lpf_1_cos = 0.0f;
    float lpf_1_sin = 0.0f;
    float lpf_2_cos = 0.0f;
    float lpf_2_sin = 0.0f;

    const float threshold = 0.01f;
    int demodulated_binary = 0;

#define SAMPLE_CLOCK_TICKS (1L<<16)
    uint16_t sample_phase = 0;
    uint16_t sample_phase_delta = SAMPLE_CLOCK_TICKS * baud_rate / sample_rate;
    int16_t  sample_phase_error = 0;

    int frame_size = 10; /* 8N1 */
    int current_frame_bits = 0;
    uint8_t current_frame_data = 0;

    uint8_t sample;
    ssize_t size;
    while ((size = fread(&sample, 1, 1, ifile)) > 0) {
        const float signal = sample/127.0f - 1.0f;

        /* DFT at f1 and f2 */
        // TODO: work on the signal as u8 sample, use LUT for cos/sin, multipling give a u16...
        // TODO: in terms of phase instead of time
        const float prod_1_cos = signal * cosf(2.0f * M_PI * f1 * t);
        const float prod_1_sin = signal * sinf(2.0f * M_PI * f1 * t);
        const float prod_2_cos = signal * cosf(2.0f * M_PI * f2 * t);
        const float prod_2_sin = signal * sinf(2.0f * M_PI * f2 * t);

        /* Low pass filter */
        const float lpf_factor = baud_rate * 2.0f * M_PI / sample_rate;
        lpf_1_cos += lpf_factor * (prod_1_cos - lpf_1_cos);
        lpf_1_sin += lpf_factor * (prod_1_sin - lpf_1_sin);
        lpf_2_cos += lpf_factor * (prod_2_cos - lpf_2_cos);
        lpf_2_sin += lpf_factor * (prod_2_sin - lpf_2_sin);

        /* Energy is the squared magnitude of the average
          -> How much the signal resemble the frequency
          -> Difference of energies:
               < 0 when signal is close to space_freq
               > 0 when signal is close to mark_freq */
        const float power_1 = lpf_1_cos * lpf_1_cos + lpf_1_sin * lpf_1_sin;
        const float power_2 = lpf_2_cos * lpf_2_cos + lpf_2_sin * lpf_2_sin;
        const float diff = power_2 - power_1;

        /* Threshold the diff signal to extract binary signal,
            we now have to sample it at the right places */
        const int old_demodulated_binary = demodulated_binary;
        if (diff > threshold) {
            demodulated_binary = 1;
        }
        else if (diff < -threshold) {
            demodulated_binary = 0;
        }

        /* Edge detected, we want sample_phase to be at half here so we sample
        in the middle of the baud period when the signal is stable. We need to
        synchronize our sample clock accordingly, if the phase is too late, go
        faster, and if too early slow down */
        // TODO: Add logic to prevent triggering too fast if diff signal is noisy
        if (demodulated_binary != old_demodulated_binary) {
            sample_phase_error = SAMPLE_CLOCK_TICKS/2 - sample_phase;
            sample_phase = SAMPLE_CLOCK_TICKS/2;
            // TODO: what is the correct correction factor ?  Do a proper PI controller ?
            sample_phase_delta += sample_phase_error / 1024;
        }

        /* Sampler */
        uint16_t old_sample_phase = sample_phase;
        sample_phase += sample_phase_delta;
        if (sample_phase < old_sample_phase) { /* If overflow */
            /* Here we sample demodulated_binary and decode 8N1 */
            if (current_frame_bits == 0 && demodulated_binary == 0) { /* Start bit */
                current_frame_bits++;
            } else if (current_frame_bits == frame_size-1 /* && demodulated_binary == 1 */) { /* Stop bit */
                fputc(current_frame_data, ofile);
                current_frame_bits = 0;
                current_frame_data = 0;
            } else if (current_frame_bits > 0) { /* Data bits */
                current_frame_data |= demodulated_binary << (current_frame_bits - 1);
                current_frame_bits++;
            }
        }

        if (t > 9.0f && t < 10.0) {
            printf("%f\t%f\t%f\t%f\n", t, diff*8000, (float)sample_phase_error, (float)sample_phase);
        }

        t += 1.0f / sample_rate;
    }

    if (size < 0) {
        perror("Reading sample");
        return -1;
    }

    return 0;
}


/* Fully digital decode function, without float/trig functions */

int8_t sin_lut[256] = {
    0,    3,    6,    9,   12,   16,   19,   22,
   25,   28,   31,   34,   37,   40,   43,   46,
   49,   51,   54,   57,   60,   63,   65,   68,
   71,   73,   76,   78,   81,   83,   85,   88,
   90,   92,   94,   96,   98,  100,  102,  104,
  106,  107,  109,  111,  112,  113,  115,  116,
  117,  118,  120,  121,  122,  122,  123,  124,
  125,  125,  126,  126,  126,  127,  127,  127,
  127,  127,  127,  127,  126,  126,  126,  125,
  125,  124,  123,  122,  122,  121,  120,  118,
  117,  116,  115,  113,  112,  111,  109,  107,
  106,  104,  102,  100,   98,   96,   94,   92,
   90,   88,   85,   83,   81,   78,   76,   73,
   71,   68,   65,   63,   60,   57,   54,   51,
   49,   46,   43,   40,   37,   34,   31,   28,
   25,   22,   19,   16,   12,    9,    6,    3,
    0,   -3,   -6,   -9,  -12,  -16,  -19,  -22,
  -25,  -28,  -31,  -34,  -37,  -40,  -43,  -46,
  -49,  -51,  -54,  -57,  -60,  -63,  -65,  -68,
  -71,  -73,  -76,  -78,  -81,  -83,  -85,  -88,
  -90,  -92,  -94,  -96,  -98, -100, -102, -104,
 -106, -107, -109, -111, -112, -113, -115, -116,
 -117, -118, -120, -121, -122, -122, -123, -124,
 -125, -125, -126, -126, -126, -127, -127, -127,
 -127, -127, -127, -127, -126, -126, -126, -125,
 -125, -124, -123, -122, -122, -121, -120, -118,
 -117, -116, -115, -113, -112, -111, -109, -107,
 -106, -104, -102, -100,  -98,  -96,  -94,  -92,
  -90,  -88,  -85,  -83,  -81,  -78,  -76,  -73,
  -71,  -68,  -65,  -63,  -60,  -57,  -54,  -51,
  -49,  -46,  -43,  -40,  -37,  -34,  -31,  -28,
  -25,  -22,  -19,  -16,  -12,   -9,   -6,   -3
};

int decode_digital(const tape_fmt_t *format, FILE *ifile, FILE *ofile)
{
    wav_header_t header;
    if (fread(&header, sizeof(header), 1, ifile) != 1) {
        fprintf(stderr, "Couldn't load header\n");
        return -1;
    }

    fprintf(stderr, "Loaded header:\n");
    fprintf(stderr, "  chans = %u\n", header.num_channels);
    fprintf(stderr, "  srate = %u\n", header.sample_rate);
    fprintf(stderr, "  brate = %u\n", header.byte_rate);
    fprintf(stderr, "  align = %u\n", header.block_align);
    fprintf(stderr, "  bits  = %u\n", header.bits_per_sample);
    fprintf(stderr, "  size  = %u\n", header.data_chunk_size);

    if (header.num_channels != 1) {
        fprintf(stderr, "Only supports 1 channel\n");
        return -1;
    }

    if (header.bits_per_sample != 8) {
        fprintf(stderr, "Unsupported format\n");
        return -1;
    }

    const uint16_t sample_rate = header.sample_rate;

    float t = 0.0f; // For debug

    int16_t baud_rate = format->baud_rate;
    int16_t f1 = format->space_freq; // TODO: Ajust these frequencies to match signal
    int16_t f2 = format->mark_freq;

    /* Oscillators at f1 and f2 freq (8bits seems to be too low resolution) */
    uint16_t f1_phase = 0;
    uint16_t f2_phase = 0;
    uint16_t f1_phase_delta = (f1 * 65535) / sample_rate;
    uint16_t f2_phase_delta = (f2 * 65535) / sample_rate;

    int8_t lpf_1_cos = 0;
    int8_t lpf_1_sin = 0;
    int8_t lpf_2_cos = 0;
    int8_t lpf_2_sin = 0;

    const int8_t threshold = 32;
    uint8_t demodulated_binary = 0;

    uint16_t sample_phase = 0;
    uint16_t sample_phase_delta = 65535 * baud_rate / sample_rate;
    int16_t  sample_phase_error = 0;

    int frame_size = 10; /* 8N1 */
    int current_frame_bits = 0;
    uint8_t current_frame_data = 0;

    uint8_t sample;
    ssize_t size;
    while ((size = fread(&sample, 1, 1, ifile)) > 0) {
        int8_t sample_signed = (int16_t)sample - 128;

        /* DFT at f1 and f2 */
        const int8_t prod_1_cos = sample_signed * sin_lut[(uint8_t)((f1_phase >> 8) + 64)] >> 8;
        const int8_t prod_1_sin = sample_signed * sin_lut[(uint8_t)((f1_phase >> 8)     )] >> 8;
        const int8_t prod_2_cos = sample_signed * sin_lut[(uint8_t)((f2_phase >> 8) + 64)] >> 8;
        const int8_t prod_2_sin = sample_signed * sin_lut[(uint8_t)((f2_phase >> 8)     )] >> 8;

        f1_phase += f1_phase_delta;
        f2_phase += f2_phase_delta;

        /* Low pass filter */
        lpf_1_cos += (prod_1_cos - lpf_1_cos) * baud_rate * 2 * M_PI / sample_rate;
        lpf_1_sin += (prod_1_sin - lpf_1_sin) * baud_rate * 2 * M_PI / sample_rate;
        lpf_2_cos += (prod_2_cos - lpf_2_cos) * baud_rate * 2 * M_PI / sample_rate;
        lpf_2_sin += (prod_2_sin - lpf_2_sin) * baud_rate * 2 * M_PI / sample_rate;

        /* Energy is the squared magnitude of the average
          -> How much the signal resemble the frequency
          -> Difference of energies:
               < 0 when signal is close to space_freq
               > 0 when signal is close to mark_freq */
        const int16_t power_1 = lpf_1_cos * lpf_1_cos + lpf_1_sin * lpf_1_sin;
        const int16_t power_2 = lpf_2_cos * lpf_2_cos + lpf_2_sin * lpf_2_sin;
        const int16_t diff = power_2 - power_1;

        /* Threshold the diff signal to extract binary signal,
            we now have to sample it at the right places */
        const uint8_t old_demodulated_binary = demodulated_binary;
        if (diff > threshold) {
            demodulated_binary = 1;
        }
        else if (diff < -threshold) {
            demodulated_binary = 0;
        }

        /* Edge detected, we want sample_phase to be at half here so we sample
        in the middle of the baud period when the signal is stable. We need to
        synchronize our sample clock accordingly by adjusting baud rate,
        if the phase is too late, go faster, and if too early slow down */
        // TODO: Add logic to prevent triggering too fast if diff signal is noisy
        if (demodulated_binary != old_demodulated_binary) {
            sample_phase_error = 32768 - sample_phase;
            sample_phase = 32768;
            // TODO: what is the correct correction factor ?  Do a proper PI controller ? Watch for phase error, should tend toward 0 without oscillating
            baud_rate += sample_phase_error >> 12;
            sample_phase_delta = 65535 * baud_rate / sample_rate;
        }

        /* Sampler */
        uint16_t old_sample_phase = sample_phase;
        sample_phase += sample_phase_delta;
        if (sample_phase < old_sample_phase) { /* If overflow */
            /* Here we sample demodulated_binary and decode 8N1 */
            if (current_frame_bits == 0 && demodulated_binary == 0) { /* Start bit */
                current_frame_bits++;
            } else if (current_frame_bits == frame_size-1 /* && demodulated_binary == 1 */) { /* Stop bit */
                fputc(current_frame_data, ofile);
                current_frame_bits = 0;
                current_frame_data = 0;
            } else if (current_frame_bits > 0) { /* Data bits */
                current_frame_data |= demodulated_binary << (current_frame_bits - 1);
                current_frame_bits++;
            }
        }

        //if (t > 9.0f && t < 10.0) {
        //if (t > 20.0f && t < 22.0) {
        //    printf("%f\t%f\t%f\t%f\n", t, (float)diff, (float)256.0f*demodulated_binary, (float)sample_phase_error);
        //}

        t += 1.0f / sample_rate;
    }

    if (size < 0) {
        perror("Reading sample");
        return -1;
    }

    return 0;
}

/* Stupid algorithm that looks at sample sign to measure measure frequency */
/* Warning, this won't likely work for other than the default baud/frequency settings */
int decode(const tape_fmt_t *format, FILE *ifile, FILE *ofile)
{
    wav_header_t header;
    if (fread(&header, sizeof(header), 1, ifile) != 1) {
        fprintf(stderr, "Couldn't load header\n");
        return -1;
    }

    fprintf(stderr, "Loaded header:\n");
    fprintf(stderr, "  chans = %u\n", header.num_channels);
    fprintf(stderr, "  srate = %u\n", header.sample_rate);
    fprintf(stderr, "  brate = %u\n", header.byte_rate);
    fprintf(stderr, "  align = %u\n", header.block_align);
    fprintf(stderr, "  bits  = %u\n", header.bits_per_sample);
    fprintf(stderr, "  size  = %u\n", header.data_chunk_size);

    if (header.num_channels != 1) {
        fprintf(stderr, "Only supports 1 channel\n");
        return -1;
    }

    if (header.bits_per_sample != 8) {
        fprintf(stderr, "Unsupported format\n");
        return -1;
    }

    const uint16_t sample_rate = header.sample_rate;
    int16_t baud_rate = format->baud_rate;

    float t = 0.0f; // For debug
    uint64_t nsample64 = 0; // For debug

    uint16_t nsample = 0; /* Time value */

    const int8_t sign_threshold = 32;
    int8_t sign = 0;
    uint16_t last_edge = 0; /* In number of samples */

    /* << 8 for more precision */
    uint16_t period1 = (sample_rate << 8) / format->mark_freq;
    uint16_t period2 = (sample_rate << 8) / format->space_freq;
    uint16_t middle_period = (period1 + period2) / 2;
    uint16_t period = 0;

    uint8_t demodulated_binary = 0;

    uint16_t sample_phase = 0;
    uint16_t sample_phase_delta = 65535 * baud_rate / sample_rate;
    int16_t  sample_phase_error = 0;

    int frame_size = 10; /* 8N1 */
    int current_frame_bits = 0;
    uint8_t current_frame_data = 0;

    uint8_t sample;
    ssize_t size;
    while ((size = fread(&sample, 1, 1, ifile)) > 0) {

        int8_t sample_signed = (int16_t)sample - 128;
        const int8_t old_sign = sign;
        if (sample_signed > sign_threshold) {
            sign = 1;
        } else if (sample_signed < -sign_threshold) {
            sign = -1;
        }

        /* Positive edge, LPF period */
        if (old_sign != sign && sign == 1) {
            uint16_t dt;
            if (nsample < last_edge) {
                dt = 65536UL - last_edge + nsample;
            } else {
                dt = nsample - last_edge;
            }
            dt <<= 8; /* We want more precision */

            // This isn't even needed ???
            period += (dt - period) * 8 * baud_rate * 2.0f * M_PI / sample_rate;
            period = dt;
            last_edge = nsample;
        }

        const uint8_t old_demodulated_binary = demodulated_binary;
        demodulated_binary = period < middle_period; /* WARNING! We assume space_freq < mark_freq */

        /* Edge detected, we want sample_phase to be at half here so we sample
        in the middle of the baud period when the signal is stable. We need to
        synchronize our sample clock accordingly by adjusting baud rate,
        if the phase is too late, go faster, and if too early slow down */
        // TODO: Add logic to prevent triggering too fast if diff signal is noisy
        if (demodulated_binary != old_demodulated_binary) {
            sample_phase_error = 32768 - sample_phase;
            sample_phase = 32768;
            // TODO: what is the correct correction factor ?  Do a proper PI controller ? Watch for phase error, should tend toward 0 without oscillating
            baud_rate += sample_phase_error >> 12;
            sample_phase_delta = 65535 * baud_rate / sample_rate;
        }

        /* Sampler */
        uint16_t old_sample_phase = sample_phase;
        sample_phase += sample_phase_delta;
        if (sample_phase < old_sample_phase) { /* If overflow */
            /* Here we sample demodulated_binary and decode 8N1 */
            if (current_frame_bits == 0 && demodulated_binary == 0) { /* Start bit */
                current_frame_bits++;
            } else if (current_frame_bits == frame_size-1 /* && demodulated_binary == 1 */) { /* Stop bit */
                fputc(current_frame_data, ofile);
                current_frame_bits = 0;
                current_frame_data = 0;
            } else if (current_frame_bits > 0) { /* Data bits */
                current_frame_data |= demodulated_binary << (current_frame_bits - 1);
                current_frame_bits++;
            }
        }

        //if (t > 9.0f && t < 10.0) {
        ////if (t > 20.0f && t < 22.0) {
        ////if (nsample64 > 65000 && nsample64 < 66000) {
        //    printf("%ld\t%f\t%f\t%f\t%f\n", nsample64, (float)sample_signed/127.0f, (float)period, (float)sample_phase/65535.0f, (float)demodulated_binary);
        //}

        t += 1.0f / sample_rate;
        nsample++;
        nsample64++;
    }

    if (size < 0) {
        perror("Reading sample");
        return -1;
    }

    return 0;
}



#define MODE_RX 0
#define MODE_TX 1

int main(int argc, char **argv)
{
    int mode = MODE_TX;
    const char *ifname = NULL;
    const char *ofname = NULL;

    tape_fmt_t format = {
        .sample_rate = 22050,
        .baud_rate = 300,
        .space_freq = 1850.0,
        .mark_freq = 2400.0,
    };

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-rx") == 0) {
            mode = MODE_RX;
        } else if (strcmp(argv[i], "-tx") == 0) {
            mode = MODE_TX;
        } else if (i < argc-1 && strcmp(argv[i], "-i") == 0) {
            ifname = argv[++i];
        } else if (i < argc-1 && strcmp(argv[i], "-o") == 0) {
            ofname = argv[++i];
        } else if (i < argc-1 && strcmp(argv[i], "-B") == 0) {
            format.baud_rate = atoi(argv[++i]);
        } else if (i < argc-1 && strcmp(argv[i], "-R") == 0) {
            format.sample_rate = atoi(argv[++i]);
        } else if (i < argc-1 && strcmp(argv[i], "-M") == 0) {
            format.mark_freq = atoi(argv[++i]);
        } else if (i < argc-1 && strcmp(argv[i], "-S") == 0) {
            format.space_freq = atoi(argv[++i]);
        } else {
            goto usage;
        }
    }

    if (ifname == NULL && mode == MODE_TX) {
        fprintf(stdout, "Cannot use stdin in TX mode because input size needs to be known at start\n");
        return 1;
    }

    FILE *ifile = stdin;
    FILE *ofile = stdout;

    if (ifname != NULL) {
        ifile = fopen(ifname, "rb");
        if (ifile == NULL) {
            perror("Couldn't open file");
            return 1;
        }
    } else {
        ifname = "stdin";
    }

    if (ofname != NULL) {
        ofile = fopen(ofname, "wb");
        if (ofile == NULL) {
            perror("Couldn't open file");
            return 1;
        }
    } else {
        ofname = "stdout";
    }

    fprintf(stderr, "mode        = %s\n", mode == MODE_RX ? "RX" : "TX");
    fprintf(stderr, "sample rate = %d\n", format.sample_rate);
    fprintf(stderr, "baud rate   = %d\n", format.baud_rate);
    fprintf(stderr, "mark freq   = %d\n", format.mark_freq);
    fprintf(stderr, "space freq  = %d\n", format.space_freq);
    fprintf(stderr, "input       = %s\n", ifname);
    fprintf(stderr, "output      = %s\n", ofname);

    if (mode == MODE_RX) {
        decode(&format, ifile, ofile);
    } else if (mode == MODE_TX) {
        encode(&format, ifile, ofile);
    }

    fclose(ofile);
    fclose(ifile);
    return 0;

usage:
    fprintf(stderr, "FSK modem for unsigned 8bit WAV audio files\n");
    fprintf(stderr, "Usage: %s [-h] [-rx|-tx] [-i <input-file>] [-o <output-file>]\n", argv[0]);
    fprintf(stderr, "    [-B <baud-rate>] [-R <sample-rate>] [-M <mark-freq>] [-S <space-freq>]\n");
    return 1;
}
